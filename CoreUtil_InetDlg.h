#if !defined(AFX_COREUTIL_INETDLG_H__15AF0B3F_E460_4334_9697_52252C4A9A5A__INCLUDED_)
#define AFX_COREUTIL_INETDLG_H__15AF0B3F_E460_4334_9697_52252C4A9A5A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// CoreUtil_InetDlg.h : header file
//

#include "..\Common\MFC\Inet\Inet.h"

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_InetDlg dialog

class CCoreUtil_InetDlg : public CDialog
{
// Construction
public:
	CCoreUtil_InetDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCoreUtil_InetDlg)
	enum { IDD = IDD_COREUTIL_INET_DIALOG };
	BOOL	m_bFile;
	CString	m_szURL;
	int		m_nTimerMS;
	BOOL	m_bTimes;
	//}}AFX_DATA


	CInet m_inet;
	bool m_bTimerSet;
	bool m_bDownloading;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCoreUtil_InetDlg)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCoreUtil_InetDlg)
	afx_msg void OnButtonStart();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnButtonTimer();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COREUTIL_INETDLG_H__15AF0B3F_E460_4334_9697_52252C4A9A5A__INCLUDED_)
