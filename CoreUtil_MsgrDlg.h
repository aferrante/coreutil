#if !defined(AFX_COREUTIL_MSGRDLG_H__917C8A64_1CE8_40D7_8503_B0E9813493A2__INCLUDED_)
#define AFX_COREUTIL_MSGRDLG_H__917C8A64_1CE8_40D7_8503_B0E9813493A2__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// CoreUtil_MsgrDlg.h : header file
//

#include "..\Common\MSG\Messager.h"

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_MsgrDlg dialog

class CCoreUtil_MsgrDlg : public CDialog
{
// Construction
public:
	CCoreUtil_MsgrDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCoreUtil_MsgrDlg)
	enum { IDD = IDD_COREUTIL_MSGR_DIALOG };
	CListCtrl	m_list;
	CString	m_szCaller;
	CString	m_szDest;
	CString	m_szFlags;
	CString	m_szMsg;
	CString	m_szName;
	CString	m_szParams;
	int		m_nType;
	int		m_nTimerMS;
	BOOL	m_bClock;
	//}}AFX_DATA


	bool m_bTimer;
	int m_nCount;
	CMessager m_msgr;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCoreUtil_MsgrDlg)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	void Display();
// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCoreUtil_MsgrDlg)
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonAdd();
	afx_msg void OnButtonDispatch();
	afx_msg void OnButtonModify();
	afx_msg void OnButtonRemove();
	afx_msg void OnButtonDispenc();
	afx_msg void OnButtonTimer();
	afx_msg void OnButtonQuickemail();
	afx_msg void OnButtonQuicklog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COREUTIL_MSGRDLG_H__917C8A64_1CE8_40D7_8503_B0E9813493A2__INCLUDED_)
