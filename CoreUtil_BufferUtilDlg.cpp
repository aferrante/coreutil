// CoreUtil_BufferUtilDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CoreUtil.h"
#include "CoreUtil_BufferUtilDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_BufferUtilDlg dialog


CCoreUtil_BufferUtilDlg::CCoreUtil_BufferUtilDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCoreUtil_BufferUtilDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCoreUtil_BufferUtilDlg)
	m_nTokenMode = 0;
	m_szString = _T("here||is|,a|set,of<|<,delimiters");
	m_szDelims = _T("|,<");
	m_szB64Alpha = _T("");
	m_szB64Padch = _T("");
	//}}AFX_DATA_INIT
}


void CCoreUtil_BufferUtilDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCoreUtil_BufferUtilDlg)
	DDX_Radio(pDX, IDC_RADIO1, m_nTokenMode);
	DDX_Text(pDX, IDC_EDIT_STRING, m_szString);
	DDX_Text(pDX, IDC_EDIT_DELIMS, m_szDelims);
	DDX_Text(pDX, IDC_EDITALPHA, m_szB64Alpha);
	DDX_Text(pDX, IDC_EDITPADCH, m_szB64Padch);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCoreUtil_BufferUtilDlg, CDialog)
	//{{AFX_MSG_MAP(CCoreUtil_BufferUtilDlg)
	ON_BN_CLICKED(IDC_BUTTON_BASE64DEC, OnButtonBase64dec)
	ON_BN_CLICKED(IDC_BUTTON_BASE64ENC, OnButtonBase64enc)
	ON_BN_CLICKED(IDC_BUTTON_INITTOKEN, OnButtonInittoken)
	ON_BN_CLICKED(IDC_BUTTON_NEXTTOKEN, OnButtonNexttoken)
	ON_BN_CLICKED(IDC_BUTTON_CSV, OnButtonCsv)
	ON_BN_CLICKED(IDC_BUTTON_B64NUM, OnButtonB64num)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_BufferUtilDlg message handlers

BOOL CCoreUtil_BufferUtilDlg::Create(CWnd* pParentWnd) 
{
	return CDialog::Create(CCoreUtil_BufferUtilDlg::IDD, pParentWnd);
}

void CCoreUtil_BufferUtilDlg::OnCancel() 
{
//	CDialog::OnCancel();
}

void CCoreUtil_BufferUtilDlg::OnOK() 
{
//	CDialog::OnOK();
}

void CCoreUtil_BufferUtilDlg::OnButtonBase64dec() 
{
	UpdateData(TRUE);
	CString szText;
	GetDlgItem(IDC_EDIT_INBUFFER)->GetWindowText(szText);	
	unsigned long ulBufLen = szText.GetLength();
	char* pszText = (char*) malloc(ulBufLen+1);

	if(pszText)
	{
		sprintf(pszText, "%s", szText);

		int n = m_szB64Alpha.GetLength();
		if(n>=64) m_szB64Alpha = m_szB64Alpha.Left(64);
		else
		if(n<64) m_szB64Alpha = "";
		n = m_szB64Padch.GetLength();
		if(n>=1) m_szB64Padch = m_szB64Padch.Left(1);
		else
		if(n<1) m_szB64Padch = "";
		UpdateData(FALSE);

		CBufferUtil bu;
		if(m_szB64Alpha.GetLength())
		{
			if(m_szB64Padch.GetLength())
			{
				bu.Base64Decode(&pszText, &ulBufLen, true, m_szB64Alpha.GetBuffer(64), m_szB64Padch.GetAt(0));
			}
			else
			{
				bu.Base64Decode(&pszText, &ulBufLen, true, m_szB64Alpha.GetBuffer(64));
			}
		}
		else
		{
			bu.Base64Decode(&pszText, &ulBufLen, true);
		}
		GetDlgItem(IDC_EDIT_OUTPUT)->SetWindowText(pszText);	
		free(pszText);
	}
}

void CCoreUtil_BufferUtilDlg::OnButtonBase64enc() 
{
	UpdateData(TRUE);
	CString szText;
	GetDlgItem(IDC_EDIT_INBUFFER)->GetWindowText(szText);	
	unsigned long ulBufLen = szText.GetLength();
	char* pszText = (char*) malloc(ulBufLen+1);
	if(pszText)
	{
		sprintf(pszText, "%s", szText);
		int n = m_szB64Alpha.GetLength();
		if(n>=64) m_szB64Alpha = m_szB64Alpha.Left(64);
		else
		if(n<64) m_szB64Alpha = "";
		n = m_szB64Padch.GetLength();
		if(n>=1) m_szB64Padch = m_szB64Padch.Left(1);
		else
		if(n<1) m_szB64Padch = "";
		UpdateData(FALSE);

		CBufferUtil bu;
		if(m_szB64Alpha.GetLength())
		{
			if(m_szB64Padch.GetLength())
			{
				bu.Base64Encode(&pszText, &ulBufLen, true, m_szB64Alpha.GetBuffer(64), m_szB64Padch.GetAt(0));
			}
			else
			{
				bu.Base64Encode(&pszText, &ulBufLen, true, m_szB64Alpha.GetBuffer(64));
			}
		}
		else
		{
			bu.Base64Encode(&pszText, &ulBufLen, true);
		}

		GetDlgItem(IDC_EDIT_OUTPUT)->SetWindowText(pszText);	
		free(pszText);
	}
}

void CCoreUtil_BufferUtilDlg::OnButtonInittoken() 
{
	UpdateData(TRUE);	

	char string[1024];
	sprintf(string, "%s",m_szString);
	char delims[1024];
	sprintf(delims, "%s",m_szDelims);

	char* pch = m_sbu.Token(string, strlen(string), delims, m_nTokenMode);
	if(pch) AfxMessageBox(pch);
	else AfxMessageBox("null");	
}

void CCoreUtil_BufferUtilDlg::OnButtonNexttoken() 
{
	UpdateData(TRUE);	
	char delims[1024];
	sprintf(delims, "%s",m_szDelims);
	char* pch = m_sbu.Token(NULL, 0, delims, m_nTokenMode);
	if(pch) AfxMessageBox(pch);
	else AfxMessageBox("null");	
}

void CCoreUtil_BufferUtilDlg::OnButtonCsv() 
{
	CFileDialog d(TRUE, NULL, NULL, OFN_FILEMUSTEXIST|OFN_NOCHANGEDIR|OFN_LONGNAMES|OFN_PATHMUSTEXIST,
		_T("csv files (*.csv)|*.csv|All Files (*.*)|*.*||"),
		this);

	if(d.DoModal()==IDOK)
	{
		char* pch = NULL;
		FILE* fp = fopen(d.GetPathName( ), "rb");
		if(fp)
		{
			fseek(fp, 0, SEEK_END);
			int len = ftell(fp);

			pch = (char*)malloc(len+1);
			if(pch)
			{
				fseek(fp, 0, SEEK_SET);
				fread(pch, sizeof(char), len, fp);
				*(pch+len) = 0;
			}
			fclose(fp);
		}
		if(pch)
		{
			char*** pppchBuffer=NULL;
			unsigned long ulRows;
			unsigned long ulColumns;
			if(m_bu.ParseCSV(pch, &pppchBuffer, &ulRows, &ulColumns)<0)
			{
				AfxMessageBox("ParseCSV returned an error");
			}
			else
			{
				CString szText;

				szText.Format("ParseCSV returned %d columns and %d rows:\r\n", ulColumns, ulRows);
				if(pppchBuffer)
				{
					unsigned long ulR=0;

					while(ulR<ulRows)
					{
						unsigned long ulC=0;
						while(ulC<ulColumns)
						{
							szText+= "[";
							if(pppchBuffer[ulR][ulC])
								szText+= pppchBuffer[ulR][ulC];
							else
								szText+= "(null)";
							szText+= "] ";

							if(pppchBuffer[ulR][ulC]) free(pppchBuffer[ulR][ulC]);
							ulC++;
						}

						szText+= char(13);
						szText+= char(10);

						delete [] pppchBuffer[ulR];
						ulR++;
					}
					delete [] pppchBuffer;
				}
				else szText+= "The records buffer was NULL.";

				GetDlgItem(IDC_EDIT_OUTPUT)->SetWindowText(szText);	

			}
		}
	}
	
}

void CCoreUtil_BufferUtilDlg::OnButtonB64num() 
{
	UpdateData(TRUE);
	CString szText;
	GetDlgItem(IDC_EDIT_INBUFFER)->GetWindowText(szText);	
	__int64  i64Value = _atoi64(szText);

	int n = m_szB64Alpha.GetLength();
	if(n>=64) m_szB64Alpha = m_szB64Alpha.Left(64);
	else
	if(n<64) m_szB64Alpha = "";

	UpdateData(FALSE);

	CBufferUtil bu;
	CString foo; foo.Format("input: %I64d", i64Value ); AfxMessageBox(foo);


	char* pszText = NULL;
	if(m_szB64Alpha.GetLength())
	{
//AfxMessageBox("now Here");
		pszText = bu.ReturnBase64Number(i64Value, m_szB64Alpha.GetBuffer(64));
	}
	else
	{
//AfxMessageBox("no alpha Here");
		pszText = bu.ReturnBase64Number(i64Value);
	}

	if(pszText)
	{
		GetDlgItem(IDC_EDIT_OUTPUT)->SetWindowText(pszText);
		free(pszText);
	}
	else
		GetDlgItem(IDC_EDIT_OUTPUT)->SetWindowText("error");
	

}
