#if !defined(AFX_COREUTIL_LOGUTILDLG_H__F3F76BB5_4759_4AAD_B65C_CD5F63F114EB__INCLUDED_)
#define AFX_COREUTIL_LOGUTILDLG_H__F3F76BB5_4759_4AAD_B65C_CD5F63F114EB__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// CoreUtil_LogUtilDlg.h : header file
//

#include "..\Common\TXT\LogUtil.h"


/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_LogUtilDlg dialog

class CCoreUtil_LogUtilDlg : public CDialog
{
// Construction
public:
	CCoreUtil_LogUtilDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCoreUtil_LogUtilDlg)
	enum { IDD = IDD_COREUTIL_LOGUTIL_DIALOG };
	CString	m_szCallerFunc;
	CString	m_szDestination;
	CString	m_szFileBaseName;
	CString	m_szRawData;
	CString	m_szTranslatedData;
	int		m_nIcon;
	CString	m_szMsg;
	CString	m_szRotSpec;
	int		m_nSeverity;
	int		m_nTimerMS;
	CString	m_szActualFilename;
	//}}AFX_DATA

	CLogUtil* m_plu;
	CLogUtil m_lu;

	BOOL	m_bFileOpen;
	BOOL	m_bTimerInUse;
	BOOL	m_bFileInterrupted;

	unsigned long m_ulCount;

public:

	void UpdateDisplay();


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCoreUtil_LogUtilDlg)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCoreUtil_LogUtilDlg)
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnButtonOpen();
	afx_msg void OnButtonSend();
	afx_msg void OnButtonTimer();
	afx_msg void OnButtonInterrupt();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COREUTIL_LOGUTILDLG_H__F3F76BB5_4759_4AAD_B65C_CD5F63F114EB__INCLUDED_)
