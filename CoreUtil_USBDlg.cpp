// CoreUtil_USBDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CoreUtil.h"
#include "CoreUtil_USBDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_USBDlg dialog


CCoreUtil_USBDlg::CCoreUtil_USBDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCoreUtil_USBDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCoreUtil_USBDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CCoreUtil_USBDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCoreUtil_USBDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCoreUtil_USBDlg, CDialog)
	//{{AFX_MSG_MAP(CCoreUtil_USBDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_USBDlg message handlers

void CCoreUtil_USBDlg::OnCancel() 
{
//	CDialog::OnCancel();
}

void CCoreUtil_USBDlg::OnOK() 
{
//CDialog::OnOK();
}

BOOL CCoreUtil_USBDlg::Create(CWnd* pParentWnd)
{
	// TODO: Add your specialized code here and/or call the base class
	return CDialog::Create(CCoreUtil_USBDlg::IDD, pParentWnd);
}
