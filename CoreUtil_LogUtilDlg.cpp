// CoreUtil_LogUtilDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CoreUtil.h"
#include "CoreUtil_LogUtilDlg.h"
#include "..\Common\TXT\BufferUtil.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_LogUtilDlg dialog


CCoreUtil_LogUtilDlg::CCoreUtil_LogUtilDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCoreUtil_LogUtilDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCoreUtil_LogUtilDlg)
	m_szCallerFunc = _T("Function");
	m_szDestination = _T("Dest1");
	m_szFileBaseName = _T("filename");
	m_szRawData = _T("");
	m_szTranslatedData = _T("");
	m_nIcon = 0;
	m_szMsg = _T("message text");
	m_szRotSpec = _T("YM||0|0|0|0");
	m_nSeverity = 0;
	m_nTimerMS = 250;
	m_szActualFilename = _T("");
	//}}AFX_DATA_INIT

	m_bFileOpen=FALSE;
	m_bTimerInUse=FALSE;
	m_bFileInterrupted = FALSE;
	m_plu = &m_lu;
}


void CCoreUtil_LogUtilDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCoreUtil_LogUtilDlg)
	DDX_Text(pDX, IDC_EDIT_CALLER, m_szCallerFunc);
	DDX_Text(pDX, IDC_EDIT_DEST, m_szDestination);
	DDX_Text(pDX, IDC_EDIT_FILEBASENAME, m_szFileBaseName);
	DDX_Text(pDX, IDC_EDIT_FILERAW, m_szRawData);
	DDX_Text(pDX, IDC_EDIT_FILETRANS, m_szTranslatedData);
	DDX_Text(pDX, IDC_EDIT_ICON, m_nIcon);
	DDX_Text(pDX, IDC_EDIT_MSG, m_szMsg);
	DDX_Text(pDX, IDC_EDIT_ROTSPEC, m_szRotSpec);
	DDX_Text(pDX, IDC_EDIT_SEV, m_nSeverity);
	DDX_Text(pDX, IDC_EDIT_TIMERMS, m_nTimerMS);
	DDX_Text(pDX, IDC_STATIC_NAME, m_szActualFilename);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCoreUtil_LogUtilDlg, CDialog)
	//{{AFX_MSG_MAP(CCoreUtil_LogUtilDlg)
	ON_BN_CLICKED(IDC_BUTTON_OPEN, OnButtonOpen)
	ON_BN_CLICKED(IDC_BUTTON_SEND, OnButtonSend)
	ON_BN_CLICKED(IDC_BUTTON_TIMER, OnButtonTimer)
	ON_BN_CLICKED(IDC_BUTTON_INTERRUPT, OnButtonInterrupt)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_LogUtilDlg message handlers

void CCoreUtil_LogUtilDlg::OnCancel() 
{
//  CDialog::OnCancel();
}

void CCoreUtil_LogUtilDlg::OnOK() 
{
//	CDialog::OnOK();
}

BOOL CCoreUtil_LogUtilDlg::Create(CWnd* pParentWnd) 
{
	return CDialog::Create(CCoreUtil_LogUtilDlg::IDD, pParentWnd);
}

void CCoreUtil_LogUtilDlg::OnButtonOpen() 
{
	if(!UpdateData(TRUE)) return;

	if(m_bFileOpen)
	{
		m_plu->CloseLog();

		GetDlgItem(IDC_EDIT_FILEBASENAME)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_ROTSPEC)->EnableWindow(TRUE);

		GetDlgItem(IDC_BUTTON_OPEN)->SetWindowText("Open File");
		m_bFileOpen = FALSE;
	}
	else
	{
		char params[1024];
		sprintf(params, "%s|%s", m_szFileBaseName, m_szRotSpec);

		if(m_plu)
		{
			int nRV = m_plu->OpenLog(params);
			if(nRV==LOG_SUCCESS)
			{
				GetDlgItem(IDC_EDIT_FILEBASENAME)->EnableWindow(FALSE);
				GetDlgItem(IDC_EDIT_ROTSPEC)->EnableWindow(FALSE);

				GetDlgItem(IDC_BUTTON_OPEN)->SetWindowText("Close File");
				m_bFileOpen = TRUE;
			}
			else
			{
				sprintf(params, "Error: OpenLog returned 0x%x", nRV);
				AfxMessageBox(params);
			}
		}
		else
		{
			sprintf(params, "Error: Couldn't create object.");
			AfxMessageBox(params);
		}
	}

	UpdateDisplay();

}

void CCoreUtil_LogUtilDlg::OnButtonSend() 
{
	if(!UpdateData(TRUE)) return;

	_timeb timebuffer;
	_ftime( &timebuffer );

	char pszMessage[1024];
	if(m_bTimerInUse)
	{
		m_ulCount++;
//		sprintf(pszMessage, "%s %ld", m_szMsg, m_ulCount);
		sprintf(pszMessage, "%s", m_szMsg);
	}
	else
		sprintf(pszMessage, "%s", m_szMsg);
	char pszCaller[1024];
	sprintf(pszCaller, "%s", m_szCallerFunc);
	char pszDestinations[1024];
	sprintf(pszDestinations, "%s", m_szDestination);

	m_plu->HandleMessage(timebuffer, m_nIcon|(m_nSeverity<<4), pszMessage, pszCaller, pszDestinations);
	UpdateDisplay();
}

void CCoreUtil_LogUtilDlg::OnButtonTimer() 
{
	if(!UpdateData(TRUE)) return;

	if(m_bTimerInUse)
	{
		m_bTimerInUse = FALSE;
		KillTimer(1);
		GetDlgItem(IDC_BUTTON_TIMER)->SetWindowText("Do Timer");
	}
	else
	{
		m_ulCount = 0;
		m_bTimerInUse = TRUE;
		SetTimer(1, m_nTimerMS, NULL);
		GetDlgItem(IDC_BUTTON_TIMER)->SetWindowText("Kill Timer");
	}
}

void CCoreUtil_LogUtilDlg::OnButtonInterrupt() 
{
	// TODO: Add your control notification handler code here
	if(m_bFileInterrupted)
	{
		m_bFileInterrupted = FALSE;
		m_plu->m_bFileInUse = FALSE;
	}
	else
	{
		m_bFileInterrupted = TRUE;
		m_plu->m_bFileInUse = TRUE;
	}	
}

void CCoreUtil_LogUtilDlg::UpdateDisplay()
{
	GetDlgItem(IDC_EDIT_FILERAW)->SetWindowText("");
	GetDlgItem(IDC_EDIT_FILETRANS)->SetWindowText("");
	if(m_bFileOpen)
	{
		Sleep(30); //wait for file operations to complete.
		if(m_plu->m_pfile)
		{
			// get the whole file.
			unsigned long ulFileSize=0;

			fseek( m_plu->m_pfile, 0, SEEK_END );
			ulFileSize = ftell(m_plu->m_pfile);

			if(ulFileSize)
			{
				char* pch = (char*) malloc(ulFileSize+1);// zero term
				if(pch)
				{
					rewind( m_plu->m_pfile);
					int x = ftell(m_plu->m_pfile);
					int y = fread( pch, 1, ulFileSize, m_plu->m_pfile );
	//				char foo[256]; sprintf(foo, "pos %d, %d chars", x, y);
	//				AfxMessageBox(foo);
					fseek( m_plu->m_pfile, 0, SEEK_END); 
					*(pch+ulFileSize)=0;// zero term

					CBufferUtil bu;
					char* pch2;
					unsigned long ulSize= ulFileSize;

					if(!m_plu->m_bVerboseFile)  pch2 = bu.ReadableHex(pch, &ulSize, MODE_ALLOWLINEFEED|MODE_ALLOWPRINTABLES|MODE_MAKECRLF);
					else pch2=pch;

					if(pch2)
					{
						GetDlgItem(IDC_EDIT_FILERAW)->SetWindowText(pch2);
						char* pch3;

						if(!m_plu->m_bVerboseFile)
							pch3 = m_plu->HumanReadableStream(pch, &ulFileSize );
						else
							pch3 = pch2;
							
						if(pch3)
						{
							GetDlgItem(IDC_EDIT_FILETRANS)->SetWindowText(pch3);
							if(!m_plu->m_bVerboseFile) free(pch3);
						}
						else
						{
							GetDlgItem(IDC_EDIT_FILETRANS)->SetWindowText("");
						}
						free(pch2);

					}
					if(!m_plu->m_bVerboseFile)free(pch);
				}
			}
		}
	}

	((CEdit*)GetDlgItem(IDC_EDIT_FILERAW))->LineScroll( ((CEdit*)GetDlgItem(IDC_EDIT_FILERAW))->GetLineCount( ) );
	((CEdit*)GetDlgItem(IDC_EDIT_FILETRANS))->LineScroll( ((CEdit*)GetDlgItem(IDC_EDIT_FILETRANS))->GetLineCount( ) );

}


void CCoreUtil_LogUtilDlg::OnTimer(UINT nIDEvent) 
{

	if(nIDEvent==1)
	{
		OnButtonSend();
		
	}
	CDialog::OnTimer(nIDEvent);
}
