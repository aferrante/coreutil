// CoreUtil_Http10Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "CoreUtil.h"
#include "CoreUtil_Http10Dlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_Http10Dlg dialog


CCoreUtil_Http10Dlg::CCoreUtil_Http10Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCoreUtil_Http10Dlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCoreUtil_Http10Dlg)
	m_szRoot = _T("C:\\temp\\Temp\\VDI temp\\Harris\\IBC demo\\root");
	m_nPort = 80;
	//}}AFX_DATA_INIT
	m_bServerStarted = false;
}


void CCoreUtil_Http10Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCoreUtil_Http10Dlg)
	DDX_Text(pDX, IDC_EDIT1, m_szRoot);
	DDX_Text(pDX, IDC_EDIT_PORT, m_nPort);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCoreUtil_Http10Dlg, CDialog)
	//{{AFX_MSG_MAP(CCoreUtil_Http10Dlg)
	ON_BN_CLICKED(IDC_BUTTON_START, OnButtonStart)
	ON_BN_CLICKED(IDC_BUTTON_STOP, OnButtonStop)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_Http10Dlg message handlers


void CCoreUtil_Http10Dlg::OnCancel() 
{
//	CDialog::OnCancel();
}

void CCoreUtil_Http10Dlg::OnOK() 
{
//	CDialog::OnOK();
}

BOOL CCoreUtil_Http10Dlg::Create(CWnd* pParentWnd) 
{
	return CDialog::Create(CCoreUtil_Http10Dlg::IDD, pParentWnd);
}

void CCoreUtil_Http10Dlg::OnButtonStart() 
{
	CWaitCursor cw;
				GetDlgItem(IDC_EDIT_STATUS)->SetWindowText("starting server...");
	UpdateData(TRUE);
	char buffer[MAX_PATH];
	sprintf(buffer, "%s", m_szRoot);
	if(m_http.SetRoot(buffer)==HTTP_SUCCESS)
	{
//		AfxMessageBox("set root");

		if(m_http.GetHost()==HTTP_SUCCESS)
		{
				sprintf(buffer, "Host: %s", m_http.m_pszHost);

			GetDlgItem(IDC_STATIC_HOST)->SetWindowText(buffer);

//			AfxMessageBox("got host");
			if(m_http.InitServer(m_nPort)==HTTP_SUCCESS)
			{
				GetDlgItem(IDC_EDIT_STATUS)->SetWindowText("Server started");
			} else AfxMessageBox("couldnt init server");
		} else AfxMessageBox("couldnt get host");
	} else AfxMessageBox("couldnt set root");
}

void CCoreUtil_Http10Dlg::OnButtonStop() 
{
	m_http.EndServer();  //its unconditional success
				GetDlgItem(IDC_EDIT_STATUS)->SetWindowText("Server stopped");

}
