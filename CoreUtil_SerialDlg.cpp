// CoreUtil_SerialDlg.cpp : implementation file
//

#include "stdafx.h"
#include <process.h>
#include "CoreUtil.h"
#include "CoreUtil_SerialDlg.h"
#include "../Common/TXT/BufferUtil.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


CBufferUtil  bu;


void PortMonitorThread(void* pvArgs);

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_SerialDlg dialog


CCoreUtil_SerialDlg::CCoreUtil_SerialDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCoreUtil_SerialDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCoreUtil_SerialDlg)
	m_szBaud = _T("19200");
	m_nCom = 0;
	m_nParity = 0;
	m_nStopBits = 0;
	m_nByteSize = 8;
	m_szCmd = _T("");
	m_szData = _T("");
	m_bCRLF = FALSE;
	m_bListener = FALSE;
	m_bLogTransactions = FALSE;
	//}}AFX_DATA_INIT
}


void CCoreUtil_SerialDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCoreUtil_SerialDlg)
	DDX_CBString(pDX, IDC_COMBO_BAUD, m_szBaud);
	DDX_CBIndex(pDX, IDC_COMBO_COM, m_nCom);
	DDX_CBIndex(pDX, IDC_COMBO_PARITY, m_nParity);
	DDX_CBIndex(pDX, IDC_COMBO_STOPBITS, m_nStopBits);
	DDX_Text(pDX, IDC_EDIT_BYTESIZE, m_nByteSize);
	DDX_Text(pDX, IDC_EDIT_CMD, m_szCmd);
	DDX_Text(pDX, IDC_EDIT_DATA, m_szData);
	DDX_Check(pDX, IDC_CHECK_CRLF, m_bCRLF);
	DDX_Check(pDX, IDC_CHECK_LISTENER, m_bListener);
	DDX_Check(pDX, IDC_CHECK_LOG, m_bLogTransactions);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCoreUtil_SerialDlg, CDialog)
	//{{AFX_MSG_MAP(CCoreUtil_SerialDlg)
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, OnButtonConnect)
	ON_BN_CLICKED(IDC_BUTTON_SEND, OnButtonSend)
	ON_BN_CLICKED(IDC_CHECK_CRLF, OnCheckCrlf)
	ON_BN_CLICKED(IDC_CHECK_LOG, OnCheckLog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_SerialDlg message handlers

void CCoreUtil_SerialDlg::OnButtonConnect() 
{
	if (m_serial.m_bConnected)
	{
		m_serial.CloseConnection();
	}
	else
	{
		// getparams
		UpdateData(TRUE);
		m_serial.OpenConnection(m_nCom+1, atoi(m_szBaud), m_nByteSize, m_nStopBits, m_nParity);
		if(	m_bListener	)
		{

			m_bSocketMonitorStarted=FALSE;
			m_bMonitorSocket=TRUE;

			_beginthread(PortMonitorThread, 0, (void*)this);
		}
		else
		{
		}

	}
	Sleep(250);

	if (m_serial.m_bConnected)
	{
		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Disconnect");
		GetDlgItem(IDC_CHECK_LISTENER)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
		GetDlgItem(IDC_CHECK_LISTENER)->EnableWindow(TRUE);
	}
	
}

void CCoreUtil_SerialDlg::OnCancel() 
{
//	CDialog::OnCancel();
}

void CCoreUtil_SerialDlg::OnOK() 
{
//	CDialog::OnOK();
}

BOOL CCoreUtil_SerialDlg::Create(CWnd* pParentWnd)
{
	return CDialog::Create(CCoreUtil_SerialDlg::IDD, pParentWnd);
}

BOOL CCoreUtil_SerialDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	CString item;
  int i=1;
	while(i<=24)
	{
		item.Format("COM %d", i);
		((CComboBox*)GetDlgItem(IDC_COMBO_COM))->AddString(item);

		i++;
	}


	((CComboBox*)GetDlgItem(IDC_COMBO_BAUD))->SetCurSel(6);
	((CComboBox*)GetDlgItem(IDC_COMBO_COM))->SetCurSel(0);
	((CComboBox*)GetDlgItem(IDC_COMBO_PARITY))->SetCurSel(0);
	((CComboBox*)GetDlgItem(IDC_COMBO_STOPBITS))->SetCurSel(0);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCoreUtil_SerialDlg::OnButtonSend() 
{
	if(!m_serial.m_bConnected) return;
	UpdateData(TRUE);

	char cmd[4];
	char data[4096];

	_snprintf(cmd, 3, "%s", m_szCmd);
	_snprintf(data, 4095, "%s", m_szData);
	CBufferUtil bu;
	unsigned char uch = (unsigned char) bu.xtol(cmd, 2);

	if(strlen(cmd)>0)
	{
		//its a tascam command...

	//	A command is formatted as follows:
	// [LF][ascii device ID][ascii of first hex digit][ascii of second hex digit][data][data][...][CR]

		char* pch = NULL;
		int nLen = 0;
		if((data)&&(strlen(data)))
		{
			nLen = strlen(data);
		}
		pch = (char*)malloc(nLen+6);

		if(pch)
		{
			*pch = 0x0a;  //LF
			*(pch+1) = '1';  // device ID - default to MD for now
	// OK now, if we have a command such as 0x4e, we want first byte to be '4' and the second to be 'E' (they use uppercase for letter hex chars)
			sprintf((pch+2), "%02X", uch);  // this "ought" to work.

			// then the rest of the data.
			if(nLen)
			{
				strncpy((pch+4), data, nLen);
			}

			*(pch+4+nLen) = 0x0d;  // CR
			*(pch+5+nLen) = 0;  // a just in case zero term.


	//		AfxMessageBox(pch);
			nLen = m_serial.SendData(pch, nLen+5);


			if(nLen>0)
			{
				sprintf(data, "sent [%s]", pch);
				AfxMessageBox(data);
				free(pch);
				pch = NULL;
			
				if(m_serial.ReceiveData(&pch, (unsigned long*)&nLen, 0)>0)
				{
					sprintf(data, "[%s]", pch);
					AfxMessageBox(data);
					free(pch);
				}
				else 		AfxMessageBox("Error receiving or nothing to receive");
			}
			else
			{
				free(pch);

				AfxMessageBox("Error sending");
			}
		}

	}
	else
	{
		// generic send
		char* pch = NULL;
		int nLen = 0;
		if((data)&&(strlen(data)))
		{
			nLen = strlen(data);
		}
		pch = (char*)malloc(nLen+6);

		if(pch)
		{
			// then the rest of the data.
			if(nLen)
			{
				strncpy(pch, data, nLen);
			}

			UpdateData(TRUE);
			if(m_bCRLF)
			{
				*(pch+nLen) = 0x0d;  // CR
				nLen++;
				*(pch+nLen) = 0x0a;  // LF
				nLen++;
				*(pch+nLen) = 0x00;  // zero term
			}
			else
			{
				*(pch+nLen) = 0x00;  // zero term
			}

	//		AfxMessageBox(pch);
			nLen = m_serial.SendData(pch, nLen);


			if(nLen>0)
			{
				sprintf(data, "sent [%s]", pch);
				AfxMessageBox(data);
				free(pch);
				pch = NULL;
			
				if(m_serial.ReceiveData(&pch, (unsigned long*)&nLen, 0)>0)
				{
					sprintf(data, "[%s]", pch);
					AfxMessageBox(data);
					free(pch);
				}
				else 		AfxMessageBox("Error receiving or nothing to receive");
			}
			else
			{
				free(pch);

				AfxMessageBox("Error sending");
			}
		}

	}

	
}

void CCoreUtil_SerialDlg::OnCheckCrlf() 
{
	// TODO: Add your control notification handler code here
	
}


#define LOGFILENAME "serial-tx.log"

void PortMonitorThread(void* pvArgs)
{
	CCoreUtil_SerialDlg* p = (CCoreUtil_SerialDlg*) pvArgs;
	if(p)
	{
		p->m_bSocketMonitorStarted = TRUE;
		while(p->m_bMonitorSocket)
		{

		char* pch = NULL;
		int nLen = 0;

			if(p->m_serial.ReceiveData(&pch, (unsigned long*)&nLen, 0)>0)
			{

if(p->m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );


unsigned long ulRecv = nLen;
char* pchRecv = bu.ReadableHex((char*)pch, &ulRecv, MODE_FULLHEX);

				fprintf(logfp, "%s%03d Recd %d bytes: ", logtmbuf, timestamp.millitm, nLen);

				if(pchRecv)
				{
					fwrite((char*)pchRecv, sizeof(char), ulRecv, logfp);
					p->GetDlgItem(IDC_EDIT_STATUS)->SetWindowText(pchRecv);
				}
				else
				{
					fwrite((char*)pch, sizeof(char), nLen, logfp);
					p->GetDlgItem(IDC_EDIT_STATUS)->SetWindowText(pch);
				}
				fprintf(logfp, "%c%c%c%c", 13, 10, 13, 10);

				fclose(logfp);
			}
}



				free(pch);

			}

			if(p->m_bMonitorSocket) Sleep(0);
		}
		p->m_bSocketMonitorStarted = FALSE;
	}
}

void CCoreUtil_SerialDlg::OnCheckLog() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);	
}
