#if !defined(AFX_COREUTIL_DBUTILDLG_H__97773894_409B_49F2_95D5_B2C8A2F59938__INCLUDED_)
#define AFX_COREUTIL_DBUTILDLG_H__97773894_409B_49F2_95D5_B2C8A2F59938__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// CoreUtil_DBUtilDlg.h : header file
//

#include "..\Common\MFC\ODBC\DBUtil.h"


/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_DBUtilDlg dialog

class CCoreUtil_DBUtilDlg : public CDialog
{
// Construction
public:
	CCoreUtil_DBUtilDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCoreUtil_DBUtilDlg)
	enum { IDD = IDD_COREUTIL_DBUTIL_DIALOG };
	CListCtrl	m_lc;
	CString	m_szDSN;
	int		m_nTableIndex;
	CString	m_szPW;
	CString	m_szTableName;
	CString	m_szUser;
	CString	m_szColName;
	int		m_nColSize;
	int		m_nColType;
	//}}AFX_DATA

	CDBUtil m_db;
	CDBconn* m_pdbconn;  // pointer to the "current" conn


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCoreUtil_DBUtilDlg)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


	void Display();

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCoreUtil_DBUtilDlg)
	virtual void OnCancel();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnItemchangedListConns(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButtonConnect();
	afx_msg void OnButtonNew();
	afx_msg void OnButtonExists();
	afx_msg void OnButtonCreatetable();
	afx_msg void OnButtonGettable();
	afx_msg void OnButtonDropcol();
	afx_msg void OnButtonAddcol();
	afx_msg void OnButtonRetbf();
	afx_msg void OnButtonIndex();
	afx_msg void OnButtonDelete();
	afx_msg void OnButtonInsert();
	afx_msg void OnButtonUpdate();
	afx_msg void OnButtonExecsql();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COREUTIL_DBUTILDLG_H__97773894_409B_49F2_95D5_B2C8A2F59938__INCLUDED_)
