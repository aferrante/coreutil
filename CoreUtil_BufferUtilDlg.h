#if !defined(AFX_COREUTIL_BUFFERUTILDLG_H__8259277B_7CAB_478C_993D_F11EC65C70AE__INCLUDED_)
#define AFX_COREUTIL_BUFFERUTILDLG_H__8259277B_7CAB_478C_993D_F11EC65C70AE__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// CoreUtil_BufferUtilDlg.h : header file
//
#include "..\Common\TXT\BufferUtil.h"

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_BufferUtilDlg dialog

class CCoreUtil_BufferUtilDlg : public CDialog
{
// Construction
public:
	CCoreUtil_BufferUtilDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCoreUtil_BufferUtilDlg)
	enum { IDD = IDD_COREUTIL_BUFFERUTIL_DIALOG };
	int		m_nTokenMode;
	CString	m_szString;
	CString	m_szDelims;
	CString	m_szB64Alpha;
	CString	m_szB64Padch;
	//}}AFX_DATA


	CSafeBufferUtil m_sbu;
	CBufferUtil m_bu;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCoreUtil_BufferUtilDlg)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCoreUtil_BufferUtilDlg)
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnButtonBase64dec();
	afx_msg void OnButtonBase64enc();
	afx_msg void OnButtonInittoken();
	afx_msg void OnButtonNexttoken();
	afx_msg void OnButtonCsv();
	afx_msg void OnButtonB64num();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COREUTIL_BUFFERUTILDLG_H__8259277B_7CAB_478C_993D_F11EC65C70AE__INCLUDED_)
