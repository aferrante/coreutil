// CoreUtil_SmtpDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CoreUtil.h"
#include "CoreUtil_SmtpDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

unsigned long g_ulSoFar;
unsigned long g_ulTotal;
/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_SmtpDlg dialog


CCoreUtil_SmtpDlg::CCoreUtil_SmtpDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCoreUtil_SmtpDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCoreUtil_SmtpDlg)
	m_szAttach = _T("C:\\XMLRocketEngine.pdf\r\nC:\\harris demo.xls\r\nC:\\newnorthamerica.jpg");
	m_szFrom = _T("\"test message\"<test@test.com>");
	m_szHost = _T("mail.adelphia.net");
	m_szMsg = _T("This is a test message.\r\nPlease ignore.");
	m_szTo = _T("kenji@videodesigninteractive.com");
	m_bAttach = FALSE;
	m_szReply = _T("kenji@gravitydefiance.com");
	m_szSubject = _T("this is a test message subject line");
	//}}AFX_DATA_INIT
}


void CCoreUtil_SmtpDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCoreUtil_SmtpDlg)
	DDX_Text(pDX, IDC_EDIT_ATTACH, m_szAttach);
	DDX_Text(pDX, IDC_EDIT_FROM, m_szFrom);
	DDX_Text(pDX, IDC_EDIT_HOST, m_szHost);
	DDX_Text(pDX, IDC_EDIT_MSG, m_szMsg);
	DDX_Text(pDX, IDC_EDIT_TO, m_szTo);
	DDX_Check(pDX, IDC_CHECK_ATTACH, m_bAttach);
	DDX_Text(pDX, IDC_EDIT_REPLY, m_szReply);
	DDX_Text(pDX, IDC_EDIT_SUBJECT, m_szSubject);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCoreUtil_SmtpDlg, CDialog)
	//{{AFX_MSG_MAP(CCoreUtil_SmtpDlg)
	ON_BN_CLICKED(IDC_BUTTON_SEND, OnButtonSend)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_SmtpDlg message handlers

void CCoreUtil_SmtpDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
//	CDialog::OnCancel();
}

void CCoreUtil_SmtpDlg::OnOK() 
{
//	CDialog::OnOK();
}

BOOL CCoreUtil_SmtpDlg::Create(CWnd* pParentWnd) 
{
	return CDialog::Create(CCoreUtil_SmtpDlg::IDD, pParentWnd);
}

void CCoreUtil_SmtpDlg::OnButtonSend() 
{
	UpdateData(TRUE);	

	char attach[1024];
	sprintf(attach, "%s",m_szAttach);
	char from[1024];
	sprintf(from, "%s",m_szFrom);
	char to[1024];
	sprintf(to, "%s",m_szTo);
	char host[256];
	sprintf(host, "%s",m_szHost);
	char msg[4096];
	_snprintf(msg, 4095, "%s",m_szMsg);
	char subj[1024];
	sprintf(subj, "%s",m_szSubject);
	char reply[1024];
	sprintf(reply, "%s",m_szReply);

	g_ulSoFar=0;
	g_ulTotal=0;
	SetTimer(3, 1, NULL);
	OnTimer(3);

	CWaitCursor cw;
	m_mail.ChangeLogFileUsage(true);

	if(m_bAttach)
	{
		if(		m_mail.SendMail(host, to, from, reply, subj, msg, "",
								attach, 25, &g_ulSoFar, &g_ulTotal) < SMTP_SUCCESS)
		{
			AfxMessageBox("could not send mail");
		}
//		else
//		{
//		}

	}
	else
	{
//		AfxMessageBox(host);
//		AfxMessageBox(to);
//		AfxMessageBox(from);
//		AfxMessageBox(reply);
//		AfxMessageBox(subj);
//		AfxMessageBox(msg);
		if(		m_mail.SendMail(host, to, from, reply, subj, 
                msg, "", NULL, 25, &g_ulSoFar, &g_ulTotal) < SMTP_SUCCESS)
		{
			AfxMessageBox("could not send mail");
		}
//		else
//		{
//		}
	}

	OnTimer(3);
	KillTimer(3);
}

void CCoreUtil_SmtpDlg::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==3)
	{
		CString szTemp;
		szTemp.Format("Sent %d of %d (%d%%)", g_ulSoFar, g_ulTotal, (int)((float)(g_ulSoFar*100)/(float)g_ulTotal)) ;
		GetDlgItem(IDC_STATIC_INFO)->SetWindowText(szTemp);
	}
	else
	CDialog::OnTimer(nIDEvent);
}
