// CoreUtil_FileUtilDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CoreUtil.h"
#include "CoreUtil_FileUtilDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_FileUtilDlg dialog


CCoreUtil_FileUtilDlg::CCoreUtil_FileUtilDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCoreUtil_FileUtilDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCoreUtil_FileUtilDlg)
	m_szBuffer = _T("No buffer loaded");
	m_szComment = _T("sample comment");
	m_szEntry = _T("Entry");
	m_szFilename = _T("filename.txt");
	m_szSection = _T("Section");
	m_szValue = _T("value");
	m_bEncrypt = FALSE;
	//}}AFX_DATA_INIT
}


void CCoreUtil_FileUtilDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCoreUtil_FileUtilDlg)
	DDX_Text(pDX, IDC_EDIT_BUFFER, m_szBuffer);
	DDX_Text(pDX, IDC_EDIT_COMMENT, m_szComment);
	DDX_Text(pDX, IDC_EDIT_ENTRY, m_szEntry);
	DDX_Text(pDX, IDC_EDIT_FILENAME, m_szFilename);
	DDX_Text(pDX, IDC_EDIT_SECTION, m_szSection);
	DDX_Text(pDX, IDC_EDIT_VALUE, m_szValue);
	DDX_Check(pDX, IDC_CHECK1, m_bEncrypt);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCoreUtil_FileUtilDlg, CDialog)
	//{{AFX_MSG_MAP(CCoreUtil_FileUtilDlg)
	ON_BN_CLICKED(IDC_BUTTON_GETSETTINGS, OnButtonGetsettings)
	ON_BN_CLICKED(IDC_BUTTON_GPI, OnButtonGpi)
	ON_BN_CLICKED(IDC_BUTTON_GPS, OnButtonGps)
	ON_BN_CLICKED(IDC_BUTTON_SETSETTINGS, OnButtonSetsettings)
	ON_BN_CLICKED(IDC_BUTTON_WPI, OnButtonWpi)
	ON_BN_CLICKED(IDC_BUTTON_WPS, OnButtonWps)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_FileUtilDlg message handlers

BOOL CCoreUtil_FileUtilDlg::Create(CWnd* pParentWnd) 
{
	// TODO: Add your specialized code here and/or call the base class
	return CDialog::Create(CCoreUtil_FileUtilDlg::IDD, pParentWnd);
}

void CCoreUtil_FileUtilDlg::OnCancel() 
{
//	CDialog::OnCancel();
}

void CCoreUtil_FileUtilDlg::OnOK() 
{
//	CDialog::OnOK();
}

void CCoreUtil_FileUtilDlg::OnButtonGetsettings() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	char buffer[1024];
	sprintf(buffer, "%s",m_szFilename);
	if(m_fu.GetSettings(buffer, m_bEncrypt?true:false)==	FILEUTIL_MALLOC_OK)
		m_szBuffer.Format("%s",m_fu.m_pchBuffer);
	else
		m_szBuffer = _T("No buffer loaded");

	UpdateData(FALSE);
}

void CCoreUtil_FileUtilDlg::OnButtonSetsettings() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	char buffer[1024];
	sprintf(buffer, "%s",m_szFilename);
	if(m_fu.SetSettings(buffer, m_bEncrypt?true:false)&FILEUTIL_MALLOC_OK)
		m_szBuffer.Format("%s",m_fu.m_pchBuffer);
	else
		m_szBuffer = _T("No buffer loaded");

	UpdateData(FALSE);
	
}


void CCoreUtil_FileUtilDlg::OnButtonGpi() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
//AfxMessageBox("1");
	char section[1024];
	sprintf(section, "%s",m_szSection);
	char entry[1024];
	sprintf(entry, "%s",m_szEntry);
	char value[1024];
	sprintf(value, "%s",m_szValue);
	char comment[1024];
	sprintf(comment, "%s",m_szComment);

//AfxMessageBox("2");
	int nValue = m_fu.GetIniInt(section, entry, atoi(value));
//AfxMessageBox("3");
	if(nValue==atoi(value))
	{
		sprintf(comment, "default value used: [%ld]",nValue);
		AfxMessageBox(comment);
		m_szBuffer.Format("%s",m_fu.m_pchBuffer);
	}
	else
	{
		sprintf(comment, "value retrieved: [%ld]",nValue);
		AfxMessageBox(comment);
		m_szBuffer.Format("%s",m_fu.m_pchBuffer);
	}

	UpdateData(FALSE);
	
}

void CCoreUtil_FileUtilDlg::OnButtonGps() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	char section[1024];
	sprintf(section, "%s",m_szSection);
	char entry[1024];
	sprintf(entry, "%s",m_szEntry);
	char value[1024];
	sprintf(value, "%s",m_szValue);
	char comment[1024];
	sprintf(comment, "%s",m_szComment);

	char* pch = m_fu.GetIniString(section, entry, value);
	if(pch==value)
	{
		sprintf(comment, "default value used: [%s]",pch);
		AfxMessageBox(comment);
		m_szBuffer.Format("%s",m_fu.m_pchBuffer);
	}
	else
	{
		sprintf(comment, "value retrieved: [%s]",pch);
		AfxMessageBox(comment);
		m_szBuffer.Format("%s",m_fu.m_pchBuffer);
	}

	free(pch); 

	UpdateData(FALSE);
	
}

void CCoreUtil_FileUtilDlg::OnButtonWpi() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	char section[1024];
	sprintf(section, "%s",m_szSection);
	char entry[1024];
	sprintf(entry, "%s",m_szEntry);
	char value[1024];
	sprintf(value, "%s",m_szValue);
	char comment[1024];
	sprintf(comment, "%s",m_szComment);

	int nValue;
	
	if(strlen(comment)) nValue= m_fu.SetIniInt(section, entry, atoi(value), comment);
	else  nValue= m_fu.SetIniInt(section, entry, atoi(value), NULL);
	if(nValue != FILEUTIL_WRITE_OK)
	{
		sprintf(comment, "error: %d", nValue);
		AfxMessageBox(comment);
		m_szBuffer.Format("%s",m_fu.m_pchBuffer);
	}
	else
	{
		sprintf(comment, "record written");
		AfxMessageBox(comment);
		m_szBuffer.Format("%s",m_fu.m_pchBuffer);
	}

	UpdateData(FALSE);
	
}

void CCoreUtil_FileUtilDlg::OnButtonWps() 
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);
	char section[1024];
	sprintf(section, "%s",m_szSection);
	char entry[1024];
	sprintf(entry, "%s",m_szEntry);
	char value[1024];
	sprintf(value, "%s",m_szValue);
	char comment[1024];
	sprintf(comment, "%s",m_szComment);

	int nValue;
	
	if(strlen(comment)) nValue= m_fu.SetIniString(section, entry, value, comment);
	else  nValue= m_fu.SetIniString(section, entry, value, NULL);
	
	if(nValue != FILEUTIL_WRITE_OK)
	{
		sprintf(comment, "error: %d", nValue);
		AfxMessageBox(comment);
		m_szBuffer.Format("%s",m_fu.m_pchBuffer);
	}
	else
	{
		sprintf(comment, "record written");
		AfxMessageBox(comment);
		m_szBuffer.Format("%s",m_fu.m_pchBuffer);
	}

	UpdateData(FALSE);
	
}
