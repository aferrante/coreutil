// CoreUtil.h : main header file for the COREUTIL application
//

#if !defined(AFX_COREUTIL_H__AA437A51_8C39_4D73_9A62_ADE8A89C1BE3__INCLUDED_)
#define AFX_COREUTIL_H__AA437A51_8C39_4D73_9A62_ADE8A89C1BE3__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CCoreUtilApp:
// See CoreUtil.cpp for the implementation of this class
//

class CCoreUtilApp : public CWinApp
{
public:
	CCoreUtilApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCoreUtilApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CCoreUtilApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COREUTIL_H__AA437A51_8C39_4D73_9A62_ADE8A89C1BE3__INCLUDED_)
