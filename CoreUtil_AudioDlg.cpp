// CoreUtil_AudioDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CoreUtil.h"
#include "CoreUtil_AudioDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_AudioDlg dialog


CCoreUtil_AudioDlg::CCoreUtil_AudioDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCoreUtil_AudioDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCoreUtil_AudioDlg)
	m_szFile = _T("C:/OHPLEASE.WAV");
	m_nRate = 0;
	//}}AFX_DATA_INIT
}


void CCoreUtil_AudioDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCoreUtil_AudioDlg)
	DDX_Text(pDX, IDC_EDIT1, m_szFile);
	DDX_Text(pDX, IDC_EDIT_RATE, m_nRate);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCoreUtil_AudioDlg, CDialog)
	//{{AFX_MSG_MAP(CCoreUtil_AudioDlg)
	ON_BN_CLICKED(IDC_BUTTON_PLAY, OnButtonPlay)
	ON_BN_CLICKED(IDC_BUTTON_REC, OnButtonRec)
	ON_BN_CLICKED(IDC_BUTTON_STOP, OnButtonStop)
	ON_BN_CLICKED(IDC_BUTTON_TRIM, OnButtonTrim)
	ON_BN_CLICKED(IDC_BUTTON_STOPREC, OnButtonStoprec)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_CHKFMT, OnButtonChkfmt)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_AudioDlg message handlers

void CCoreUtil_AudioDlg::OnCancel() 
{
//	CDialog::OnCancel();
}

void CCoreUtil_AudioDlg::OnOK() 
{
//	CDialog::OnOK();
}

BOOL CCoreUtil_AudioDlg::Create(CWnd* pParentWnd)
{
	// TODO: Add your specialized code here and/or call the base class
	return CDialog::Create(CCoreUtil_AudioDlg::IDD, pParentWnd);
}

void CCoreUtil_AudioDlg::OnButtonPlay() 
{
	UpdateData(TRUE);
	char file[256];
	sprintf(file, "%s", m_szFile);

	if(m_au.PlayWave( file )<AUDIO_SUCCESS) AfxMessageBox("Error playing sound");
	
}

void CCoreUtil_AudioDlg::OnButtonRec() 
{
	UpdateData(TRUE);
	char file[256];
	sprintf(file, "%s", m_szFile);

	

	if(m_au.SetRecordDevice(((CComboBox*)GetDlgItem(IDC_COMBO_DEV))->GetCurSel())<0)
	{
		AfxMessageBox("SetRecordDevice could not set the device ID.");
		return;
	}
	if(m_au.SetRecordFormat(48000, true, true)<0)  // cf mono, cf 8 bit, if device is valid, set it to best supported
	{
		AfxMessageBox("SetRecordFormat could not set the format.");
		return;
	}


	char error[MAXERRORLENGTH];
	if(m_au.BeginRecordWave( file, 0, error )<AUDIO_SUCCESS) AfxMessageBox(error);
	else SetTimer(1, 10, NULL);
}

void CCoreUtil_AudioDlg::OnButtonStop() 
{
	if(m_au.StopWave( )<AUDIO_SUCCESS) AfxMessageBox("Error stopping sound");
}

void CCoreUtil_AudioDlg::OnButtonTrim() 
{
	UpdateData(TRUE);
	char file[256];
	char file2[256];
	sprintf(file, "%s", m_szFile);

	CString szText;
	GetDlgItem(IDC_EDIT_THRESHOLD)->GetWindowText(szText);
	double threshold = atof(szText);
	szText.Format("%.02f",threshold );
	sprintf(file2, "%s%.02f.wav", m_szFile, threshold);
	GetDlgItem(IDC_EDIT_THRESHOLD)->SetWindowText(szText);

	int nrv = m_au.TrimWave( file, threshold, file2 );
	if(nrv<AUDIO_SUCCESS)
	{
		szText.Format("error: %d",nrv );

		AfxMessageBox(szText);
	}
}

BOOL CCoreUtil_AudioDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	UINT nDevs = waveInGetNumDevs();

	for(UINT i=0; i<nDevs; i++)
	{
		LPWAVEINCAPS pwic = new WAVEINCAPS;
		MMRESULT mmrv = waveInGetDevCaps(i, pwic, sizeof(WAVEINCAPS)); 

		if(mmrv == MMSYSERR_NOERROR)
		{
			((CComboBox*)GetDlgItem(IDC_COMBO_DEV))->InsertString(i, pwic->szPname);
		}
		else
		{
			((CComboBox*)GetDlgItem(IDC_COMBO_DEV))->InsertString(i, "unavailable");  // just to keep the indices correct
		}
		delete pwic;
 	}

	if(((CComboBox*)GetDlgItem(IDC_COMBO_DEV))->GetCount()>0)
	{
		((CComboBox*)GetDlgItem(IDC_COMBO_DEV))->SetCurSel(0);
	}
	
		GetDlgItem(IDC_EDIT_THRESHOLD)->SetWindowText("50.0");

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCoreUtil_AudioDlg::OnButtonStoprec() 
{
	if(m_au.StopRecordWave( )<AUDIO_SUCCESS) AfxMessageBox("Error stopping recording");
	KillTimer(1);
}

void CCoreUtil_AudioDlg::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==1)
	{
		CString szText;
		szText.Format("Recording %02d:%02d:%02d.%03d",
			(m_au.m_ulRecMS/3600000),
			(m_au.m_ulRecMS/60000)%60,
			(m_au.m_ulRecMS/1000)%60,
			m_au.m_ulRecMS%1000
			);
		GetDlgItem(IDC_STATIC_REC)->SetWindowText(szText);
	}
	else
	
	CDialog::OnTimer(nIDEvent);
}

void CCoreUtil_AudioDlg::OnButtonChkfmt() 
{
	UpdateData(TRUE);
	int nDevice =-1;
	if(((CComboBox*)GetDlgItem(IDC_COMBO_DEV))->GetCount()>0)
	{
		nDevice = ((CComboBox*)GetDlgItem(IDC_COMBO_DEV))->GetCurSel();
	}
	if(nDevice>=0)
	{
		WAVEINCAPS wic;
		MMRESULT mmrv = waveInGetDevCaps(nDevice, &wic, sizeof(WAVEINCAPS)); 
		if(mmrv == MMSYSERR_NOERROR)
		{
	//		waveInOpen( 
		}
		else AfxMessageBox("waveInGetDevCaps failed.");

	}
}
