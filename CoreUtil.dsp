# Microsoft Developer Studio Project File - Name="CoreUtil" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=CoreUtil - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "CoreUtil.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "CoreUtil.mak" CFG="CoreUtil - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "CoreUtil - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "CoreUtil - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "CoreUtil - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 rpcrt4.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "CoreUtil - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 rpcrt4.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "CoreUtil - Win32 Release"
# Name "CoreUtil - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\CoreUtil.cpp
# End Source File
# Begin Source File

SOURCE=.\CoreUtil.rc
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_AudioDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_BufferUtilDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_CortexUtilDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_DBUtilDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_DirUtilDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_FileUtilDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_Http10Dlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_InetDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_LogUtilDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_MsgrDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_NetUtilDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_SecurityDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_SerialDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_SmtpDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_USBDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CoreUtilDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SelectDirDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\SMTP\SMTPUtil.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\CoreUtil.h
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_AudioDlg.h
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_BufferUtilDlg.h
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_CortexUtilDlg.h
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_DBUtilDlg.h
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_DirUtilDlg.h
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_FileUtilDlg.h
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_Http10Dlg.h
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_InetDlg.h
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_LogUtilDlg.h
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_MsgrDlg.h
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_NetUtilDlg.h
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_SecurityDlg.h
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_SerialDlg.h
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_SmtpDlg.h
# End Source File
# Begin Source File

SOURCE=.\CoreUtil_USBDlg.h
# End Source File
# Begin Source File

SOURCE=.\CoreUtilDlg.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\SelectDirDlg.h
# End Source File
# Begin Source File

SOURCE=..\Common\SMTP\SMTPUtil.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;cnt;rtf;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\CoreUtil.ico
# End Source File
# Begin Source File

SOURCE=.\res\CoreUtil.rc2
# End Source File
# Begin Source File

SOURCE=.\res\diricons.bmp
# End Source File
# End Group
# Begin Group "Core files"

# PROP Default_Filter "cpp h c hpp"
# Begin Group "defines"

# PROP Default_Filter "h"
# Begin Source File

SOURCE=..\Common\MSG\msg.h
# End Source File
# Begin Source File

SOURCE=..\Common\LAN\NetDefines.h
# End Source File
# Begin Source File

SOURCE=..\Common\TXT\SecurityDefines.h
# End Source File
# End Group
# Begin Source File

SOURCE=..\Common\MFC\Audio\AudioUtil.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\MFC\Audio\AudioUtil.h
# End Source File
# Begin Source File

SOURCE=..\Common\TXT\BufferUtil.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=..\Common\TXT\BufferUtil.h
# End Source File
# Begin Source File

SOURCE=..\Common\MFC\ODBC\DBUtil.cpp

!IF  "$(CFG)" == "CoreUtil - Win32 Release"

# ADD CPP /Yu

!ELSEIF  "$(CFG)" == "CoreUtil - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\Common\MFC\ODBC\DBUtil.h
# End Source File
# Begin Source File

SOURCE=..\Common\FILE\DirUtil.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\FILE\DirUtil.h
# End Source File
# Begin Source File

SOURCE=..\Common\TXT\FileUtil.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=..\Common\TXT\FileUtil.h
# End Source File
# Begin Source File

SOURCE=..\Common\HTTP\HTTP10.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=..\Common\HTTP\HTTP10.h
# End Source File
# Begin Source File

SOURCE=..\Common\MFC\Inet\Inet.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\MFC\Inet\Inet.h
# End Source File
# Begin Source File

SOURCE=..\Common\TXT\LogUtil.cpp

!IF  "$(CFG)" == "CoreUtil - Win32 Release"

# ADD CPP /Yu

!ELSEIF  "$(CFG)" == "CoreUtil - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\Common\TXT\LogUtil.h
# End Source File
# Begin Source File

SOURCE=..\Common\MSG\Messager.cpp

!IF  "$(CFG)" == "CoreUtil - Win32 Release"

# ADD CPP /Yu

!ELSEIF  "$(CFG)" == "CoreUtil - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\Common\MSG\Messager.h
# End Source File
# Begin Source File

SOURCE=..\Common\MSG\MessagingObject.cpp

!IF  "$(CFG)" == "CoreUtil - Win32 Release"

# ADD CPP /Yu

!ELSEIF  "$(CFG)" == "CoreUtil - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\Common\MSG\MessagingObject.h
# End Source File
# Begin Source File

SOURCE=..\Common\LAN\NetUtil.cpp

!IF  "$(CFG)" == "CoreUtil - Win32 Release"

# ADD CPP /Yu

!ELSEIF  "$(CFG)" == "CoreUtil - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\Common\LAN\NetUtil.h
# End Source File
# Begin Source File

SOURCE=..\Common\TXT\Security.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=..\Common\TXT\Security.h
# End Source File
# Begin Source File

SOURCE=..\Common\TTY\Serial.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\TTY\Serial.h
# End Source File
# Begin Source File

SOURCE=..\Common\SMTP\smtp.cpp

!IF  "$(CFG)" == "CoreUtil - Win32 Release"

# ADD CPP /Yu

!ELSEIF  "$(CFG)" == "CoreUtil - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\Common\SMTP\smtp.h
# End Source File
# Begin Source File

SOURCE=..\Common\MFC\USB\USBUtil.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\MFC\USB\USBUtil.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
