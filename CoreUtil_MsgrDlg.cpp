// CoreUtil_MsgrDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CoreUtil.h"
#include "CoreUtil_MsgrDlg.h"
#include "..\Common\TXT\BufferUtil.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_MsgrDlg dialog


CCoreUtil_MsgrDlg::CCoreUtil_MsgrDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCoreUtil_MsgrDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCoreUtil_MsgrDlg)
	m_szCaller = _T("Function");
	m_szDest = _T("log,email");
	m_szFlags = _T("00000000");
	m_szMsg = _T("This is a test message");
	m_szName = _T("log");
	m_szParams = _T("testlog|WH||1");
	m_nType = MSG_DESTTYPE_LOG;
	m_nTimerMS = 1;
	m_bClock = FALSE;
	//}}AFX_DATA_INIT
}

// for logfiles, we need params, and they must be in this format:
//File base name | rotatespec | custom filename spec | human readable | allow repeat text | offset unixtime | offset millisecs

void CCoreUtil_MsgrDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCoreUtil_MsgrDlg)
	DDX_Control(pDX, IDC_LIST1, m_list);
	DDX_Text(pDX, IDC_EDIT_CALLER, m_szCaller);
	DDX_Text(pDX, IDC_EDIT_DEST, m_szDest);
	DDX_Text(pDX, IDC_EDIT_FLAGS, m_szFlags);
	DDX_Text(pDX, IDC_EDIT_MSG, m_szMsg);
	DDX_Text(pDX, IDC_EDIT_NAME, m_szName);
	DDX_Text(pDX, IDC_EDIT_PARAMS, m_szParams);
	DDX_Text(pDX, IDC_EDIT_TYPE, m_nType);
	DDX_Text(pDX, IDC_EDIT_TIMERMS, m_nTimerMS);
	DDX_Check(pDX, IDC_CHECK_CLOCK, m_bClock);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCoreUtil_MsgrDlg, CDialog)
	//{{AFX_MSG_MAP(CCoreUtil_MsgrDlg)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_ADD, OnButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_DISPATCH, OnButtonDispatch)
	ON_BN_CLICKED(IDC_BUTTON_MODIFY, OnButtonModify)
	ON_BN_CLICKED(IDC_BUTTON_REMOVE, OnButtonRemove)
	ON_BN_CLICKED(IDC_BUTTON_DISPENC, OnButtonDispenc)
	ON_BN_CLICKED(IDC_BUTTON_TIMER, OnButtonTimer)
	ON_BN_CLICKED(IDC_BUTTON_QUICKEMAIL, OnButtonQuickemail)
	ON_BN_CLICKED(IDC_BUTTON_QUICKLOG, OnButtonQuicklog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_MsgrDlg message handlers

BOOL CCoreUtil_MsgrDlg::Create(CWnd* pParentWnd) 
{
	return CDialog::Create(CCoreUtil_MsgrDlg::IDD, pParentWnd);
}


void CCoreUtil_MsgrDlg::OnCancel() 
{
//CDialog::OnCancel();
}

void CCoreUtil_MsgrDlg::OnOK() 
{
//CDialog::OnOK();
}

void CCoreUtil_MsgrDlg::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==6)
	{
		OnButtonDispatch();
//		OnButtonDispenc();

		m_nCount++;
		CString foo; foo.Format("%d", m_nCount);
		GetDlgItem(IDC_STATIC_COUNT)->SetWindowText(foo);

	}
	else
	CDialog::OnTimer(nIDEvent);
}

BOOL CCoreUtil_MsgrDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
//	InitMessager(&m_msgr);
//	m_msgr.InitializeMessaging(::DispatchEncodedMessage);
	CRect rc;
	m_list.GetClientRect(&rc);
	m_list.InsertColumn(0, "Destination", LVCFMT_LEFT, rc.Width()*3/6 );
	m_list.InsertColumn(1, "Params", LVCFMT_LEFT, rc.Width()*2/6 );
	m_list.InsertColumn(2, "type", LVCFMT_LEFT, rc.Width()/6 );
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCoreUtil_MsgrDlg::OnButtonAdd() 
{
	UpdateData(TRUE);	

	char name[MAX_MESSAGE_LENGTH];
	sprintf(name, "%s",m_szName);
	char param[MAX_MESSAGE_LENGTH];
	sprintf(param, "%s",m_szParams);
//AfxMessageBox(param);
	char errorstring[MAX_MESSAGE_LENGTH];
	char outputstring[MAX_MESSAGE_LENGTH];

	int nRegisterCode = m_msgr.AddDestination(m_nType, name, param, errorstring);
	if (nRegisterCode != MSG_SUCCESS) 
	{
		_snprintf(outputstring, MAX_MESSAGE_LENGTH-1, "Failed to register destination!\n code: %d\n%s", nRegisterCode, errorstring ); 
		AfxMessageBox(outputstring);
	}
	else
	{
		Display();

		/*
		nRegisterCode = m_list.GetItemCount();

		m_list.InsertItem(nRegisterCode, name);
		m_list.SetItemText(nRegisterCode, 1, param);
		sprintf(errorstring, "%d" , m_nType);
		m_list.SetItemText(nRegisterCode, 2, errorstring);
		*/

//		nRegisterCode = m_msgr.GetDestIndex(name);
//		if(nRegisterCode>=0) AfxMessageBox(m_msgr.m_ppDest[nRegisterCode]->m_pszParams);
	}
	
}

void CCoreUtil_MsgrDlg::OnButtonModify() 
{
	UpdateData(TRUE);	

	char name[MAX_MESSAGE_LENGTH];
	sprintf(name, "%s",m_szName);
	char param[MAX_MESSAGE_LENGTH];
	sprintf(param, "%s",m_szParams);
	char errorstring[MAX_MESSAGE_LENGTH];
	char outputstring[MAX_MESSAGE_LENGTH];

	int nRegisterCode = m_msgr.ModifyDestination( name, param, m_nType, errorstring);
	if (nRegisterCode != MSG_SUCCESS) 
	{
		_snprintf(outputstring, MAX_MESSAGE_LENGTH-1, "Failed to modify destination!\n code: %d\n%s", nRegisterCode, errorstring ); 
		AfxMessageBox(outputstring);
	}
	else
	{
		_snprintf(outputstring, MAX_MESSAGE_LENGTH-1, "Modified destination!\n code: %d\n%s", nRegisterCode, errorstring ); 
		AfxMessageBox(outputstring);
		Display();
		/*
		LV_FINDINFO FindInfo;
		FindInfo.flags = LVFI_STRING; 
    FindInfo.psz = name; 
    FindInfo.lParam = 0; 

		nRegisterCode = m_list.FindItem( &FindInfo );
		if(nRegisterCode>=0)
		{
			m_list.SetItemText(nRegisterCode, 1, param);
			sprintf(errorstring, "%d" , m_nType);
			m_list.SetItemText(nRegisterCode, 2, errorstring);
		}
		*/
	}
	
}

void CCoreUtil_MsgrDlg::OnButtonRemove() 
{
	UpdateData(TRUE);	

	char errorstring[MAX_MESSAGE_LENGTH];
	char outputstring[MAX_MESSAGE_LENGTH];

	int nRegisterCode = m_list.GetNextItem(-1, LVNI_SELECTED|LVNI_FOCUSED);

	if(nRegisterCode<0){ AfxMessageBox("no selection"); return;}

	char name[1024];
	sprintf(name, "%s", m_list.GetItemText(nRegisterCode, 0));
	nRegisterCode = m_msgr.RemoveDestination( name, errorstring);
	if (nRegisterCode != MSG_SUCCESS) 
	{
		_snprintf(outputstring, MAX_MESSAGE_LENGTH-1, "Failed to remove destination!\n code: %d\n%s", nRegisterCode, errorstring ); 
		AfxMessageBox(outputstring);
	}
	else
	{
		sprintf(outputstring, "%d objects left", m_msgr.m_ucNumDest);
		AfxMessageBox(outputstring);

		Display();
		/*
		LV_FINDINFO FindInfo;
		FindInfo.flags = LVFI_STRING; 
    FindInfo.psz = name; 
    FindInfo.lParam = 0; 

		nRegisterCode = m_list.FindItem( &FindInfo );
		if(nRegisterCode>=0)
		{
			m_list.DeleteItem(nRegisterCode);
		}
		*/
	}
}

void CCoreUtil_MsgrDlg::OnButtonDispatch() 
{
	UpdateData(TRUE);	

	char caller[MAX_MESSAGE_LENGTH];
	sprintf(caller, "%s",m_szCaller);
	char dest[MAX_MESSAGE_LENGTH];
	sprintf(dest, "%s",m_szDest);
	char msg[MAX_MESSAGE_LENGTH];
//	sprintf(msg, "%s",m_szMsg);  // makes it same each time
	if(m_bClock)
	{
		sprintf(msg, "%s %d",m_szMsg, clock());  // makes it different each time
	}
	else
	{
		sprintf(msg, "%s",m_szMsg);  // makes it same each time
	}
	char flags[MAX_MESSAGE_LENGTH];
	sprintf(flags, "%s",m_szFlags);
//	AfxMessageBox(flags);

	CBufferUtil bu;
	unsigned long ulFlags=bu.xtol(flags, strlen(flags));
	
//	sprintf(flags, "0x%08x",ulFlags);
//	AfxMessageBox(flags);

	m_msgr.DM(ulFlags, dest, caller, msg);
}

void CCoreUtil_MsgrDlg::OnButtonDispenc() 
{
	UpdateData(TRUE);	

	char caller[MAX_MESSAGE_LENGTH];
	sprintf(caller, "%s",m_szCaller);
	char dest[MAX_MESSAGE_LENGTH];
	sprintf(dest, "%s",m_szDest);
	char msg[MAX_MESSAGE_LENGTH];
	sprintf(msg, "%s",m_szMsg);
	char flags[MAX_MESSAGE_LENGTH];
	sprintf(flags, "%s",m_szFlags);
//	AfxMessageBox(flags);

	CBufferUtil bu;
	unsigned long ulFlags=bu.xtol(flags, strlen(flags));
	
//	sprintf(flags, "0x%08x",ulFlags);
//	AfxMessageBox(flags);

	m_msgr.m_mobj.Message(ulFlags, msg, caller, dest);
	
}

void CCoreUtil_MsgrDlg::Display()
{
	m_list.DeleteAllItems();
	unsigned char i=0;
	while(i<m_msgr.m_ucNumDest)
	{
		if((m_msgr.m_ppDest)&&(m_msgr.m_ppDest[i]))
		{
			int nRegisterCode = m_list.GetItemCount();

			m_list.InsertItem(nRegisterCode, m_msgr.m_ppDest[i]->m_pszDestName);
//			AfxMessageBox(m_msgr.m_ppDest[i]->m_pszParams);
			m_list.SetItemText(nRegisterCode, 1, m_msgr.m_ppDest[i]->m_pszParams);
			char errorstring[MAX_MESSAGE_LENGTH];
			sprintf(errorstring, "%d" , m_msgr.m_ppDest[i]->m_ucType);
			m_list.SetItemText(nRegisterCode, 2, errorstring);
		}
		i++;

	}

}

void CCoreUtil_MsgrDlg::OnButtonTimer() 
{
	if(m_bTimer)
	{
		KillTimer(6);
		GetDlgItem(IDC_BUTTON_TIMER)->SetWindowText("Do Timer");
		m_bTimer = false;
	}
	else
	{
		UpdateData(TRUE);
		SetTimer(6, m_nTimerMS, NULL);
		GetDlgItem(IDC_BUTTON_TIMER)->SetWindowText("Kill Timer");
		GetDlgItem(IDC_STATIC_COUNT)->SetWindowText("0");
		m_nCount = 0;
		m_bTimer = true;
	}
	
}

void CCoreUtil_MsgrDlg::OnButtonQuickemail() 
{
	GetDlgItem(IDC_STATIC_PARAMFMT)->SetWindowText("SMTP address[: override port (optional)] | distribution lists | from email | subject spec | local host name | log filename | log errors| log non-errors | allow repeat msgs | offset unixtime | offset millisecs");
	GetDlgItem(IDC_EDIT_PARAMS)->SetWindowText("mail.optonline.net|00:kenji@videodesignsoftware.com,kenji@sense-nexus.com;11:kenji-l@ix.netcom.com;22:kenji@sense-nexus.com;33:kenji@gravitydefiance.com|test@videodesignsoftware.com|from %h test mail|localhost|testmail.txt|1|1|0");		
	GetDlgItem(IDC_EDIT_NAME)->SetWindowText("email");
	GetDlgItem(IDC_EDIT_TYPE)->SetWindowText("5");
}

void CCoreUtil_MsgrDlg::OnButtonQuicklog() 
{
	GetDlgItem(IDC_STATIC_PARAMFMT)->SetWindowText("File base name | rotatespec | custom filename spec | human readable | allow repeat text | offset unixtime | offset millisecs");
	GetDlgItem(IDC_EDIT_PARAMS)->SetWindowText("testlog|WH||1");		
	GetDlgItem(IDC_EDIT_NAME)->SetWindowText("log");
	GetDlgItem(IDC_EDIT_TYPE)->SetWindowText("2");
}
