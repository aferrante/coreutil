#if !defined(AFX_COREUTIL_SERIALDLG_H__B6FB14C0_E3C4_4231_8C10_7453C2715E17__INCLUDED_)
#define AFX_COREUTIL_SERIALDLG_H__B6FB14C0_E3C4_4231_8C10_7453C2715E17__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// CoreUtil_SerialDlg.h : header file
//

#include "../Common/TTY/Serial.h"
/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_SerialDlg dialog

class CCoreUtil_SerialDlg : public CDialog
{
// Construction
public:
	CCoreUtil_SerialDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCoreUtil_SerialDlg)
	enum { IDD = IDD_COREUTIL_TTY_DIALOG };
	CString	m_szBaud;
	int		m_nCom;
	int		m_nParity;
	int		m_nStopBits;
	int		m_nByteSize;
	CString	m_szCmd;
	CString	m_szData;
	BOOL	m_bCRLF;
	BOOL	m_bListener;
	BOOL	m_bLogTransactions;
	//}}AFX_DATA

	CSerial m_serial;

	BOOL m_bSocketMonitorStarted;
	BOOL m_bMonitorSocket;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCoreUtil_SerialDlg)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCoreUtil_SerialDlg)
	afx_msg void OnButtonConnect();
	virtual void OnCancel();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonSend();
	afx_msg void OnCheckCrlf();
	afx_msg void OnCheckLog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COREUTIL_SERIALDLG_H__B6FB14C0_E3C4_4231_8C10_7453C2715E17__INCLUDED_)
