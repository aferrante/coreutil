#if !defined(AFX_COREUTIL_CORTEXUTILDLG_H__0485A26C_1983_4318_8EEB_CF7DC9848EA0__INCLUDED_)
#define AFX_COREUTIL_CORTEXUTILDLG_H__0485A26C_1983_4318_8EEB_CF7DC9848EA0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CoreUtil_CortexUtilDlg.h : header file
//
#include "..\Common\LAN\NetUtil.h"

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_CortexUtilDlg dialog

class CCoreUtil_CortexUtilDlg : public CDialog
{
// Construction
public:
	CCoreUtil_CortexUtilDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCoreUtil_CortexUtilDlg)
	enum { IDD = IDD_COREUTIL_CXUTIL_DIALOG };
	int		m_nSendPort;
	CString	m_szHost;
	CString	m_szCmd;
	CString	m_szOptions;
	BOOL	m_bLogExact;
	BOOL	m_bLogReadable;
	//}}AFX_DATA


	BOOL m_bInCommand;
	BOOL m_bMonitorSocket;
	BOOL m_bSocketMonitorStarted;
	int m_nClock;


	CNetUtil m_net;
	SOCKET m_s;
	CString	m_szAssembled;

	CString FormatXML(CString szXML);
	void OnMonitorButtonRecv();


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCoreUtil_CortexUtilDlg)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCoreUtil_CortexUtilDlg)
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnButtonCSend();
	afx_msg void OnButtonConnect();
	afx_msg void OnButtonAssemble();
	afx_msg void OnDestroy();
	afx_msg void OnButtonRecv();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COREUTIL_CORTEXUTILDLG_H__0485A26C_1983_4318_8EEB_CF7DC9848EA0__INCLUDED_)
