#if !defined(AFX_COREUTIL_AUDIODLG_H__E06ED4B6_642E_4D27_8EC4_72C795C8B945__INCLUDED_)
#define AFX_COREUTIL_AUDIODLG_H__E06ED4B6_642E_4D27_8EC4_72C795C8B945__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// CoreUtil_AudioDlg.h : header file
//
#include "..\Common\MFC\Audio\AudioUtil.h"

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_AudioDlg dialog

class CCoreUtil_AudioDlg : public CDialog
{
// Construction
public:
	CCoreUtil_AudioDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCoreUtil_AudioDlg)
	enum { IDD = IDD_COREUTIL_AUDIO_DIALOG };
	CString	m_szFile;
	int		m_nRate;
	//}}AFX_DATA


	CAudioUtil m_au;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCoreUtil_AudioDlg)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCoreUtil_AudioDlg)
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnButtonPlay();
	afx_msg void OnButtonRec();
	afx_msg void OnButtonStop();
	afx_msg void OnButtonTrim();
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonStoprec();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnButtonChkfmt();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COREUTIL_AUDIODLG_H__E06ED4B6_642E_4D27_8EC4_72C795C8B945__INCLUDED_)
