#if !defined(AFX_COREUTIL_SMTPDLG_H__A2D2D9FC_F65C_4BF8_85A5_F6F7D0B9E213__INCLUDED_)
#define AFX_COREUTIL_SMTPDLG_H__A2D2D9FC_F65C_4BF8_85A5_F6F7D0B9E213__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// CoreUtil_SmtpDlg.h : header file
//

#include "..\Common\SMTP\smtp.h"
/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_SmtpDlg dialog

class CCoreUtil_SmtpDlg : public CDialog
{
// Construction
public:
	CCoreUtil_SmtpDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCoreUtil_SmtpDlg)
	enum { IDD = IDD_COREUTIL_SMTP_DIALOG };
	CString	m_szAttach;
	CString	m_szFrom;
	CString	m_szHost;
	CString	m_szMsg;
	CString	m_szTo;
	BOOL	m_bAttach;
	CString	m_szReply;
	CString	m_szSubject;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCoreUtil_SmtpDlg)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	CSMTP m_mail;

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCoreUtil_SmtpDlg)
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnButtonSend();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COREUTIL_SMTPDLG_H__A2D2D9FC_F65C_4BF8_85A5_F6F7D0B9E213__INCLUDED_)
