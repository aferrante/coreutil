#if !defined(AFX_COREUTIL_SECURITYDLG_H__093E3187_F373_454B_847E_60AAE3E9D28D__INCLUDED_)
#define AFX_COREUTIL_SECURITYDLG_H__093E3187_F373_454B_847E_60AAE3E9D28D__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// CoreUtil_SecurityDlg.h : header file
//
#include "..\Common\TXT\Security.h"

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_SecurityDlg dialog

class CCoreUtil_SecurityDlg : public CDialog
{
// Construction
public:
	CCoreUtil_SecurityDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCoreUtil_SecurityDlg)
	enum { IDD = IDD_COREUTIL_SECURITY_DIALOG };
	CString	m_szFilename;
	CString	m_szLocusName;
	CString	m_szMagicPW;
	CString	m_szMagicUser;
	CString	m_szBuffer;
	CString	m_szUser;
	CString	m_szPw;
	CString	m_szAsset;
	CString	m_szGroup;
	//}}AFX_DATA

	CSecurity m_secure;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCoreUtil_SecurityDlg)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCoreUtil_SecurityDlg)
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnButtonAddlocus();
	afx_msg void OnButtonAdduser();
	afx_msg void OnButtonInit();
	afx_msg void OnButtonRemlocus();
	afx_msg void OnButtonRemuser();
	afx_msg void OnButtonSave();
	afx_msg void OnButtonCheck();
	afx_msg void OnButtonAddasset();
	afx_msg void OnButtonAddlocustouser();
	afx_msg void OnButtonAddgrp();
	afx_msg void OnButtonRemasset();
	afx_msg void OnButtonRemgrp();
	afx_msg void OnButtonRemlocusfromuser();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COREUTIL_SECURITYDLG_H__093E3187_F373_454B_847E_60AAE3E9D28D__INCLUDED_)
