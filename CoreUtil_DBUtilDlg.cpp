// CoreUtil_DBUtilDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CoreUtil.h"
#include "CoreUtil_DBUtilDlg.h"
#include <process.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_DBUtilDlg dialog


CCoreUtil_DBUtilDlg::CCoreUtil_DBUtilDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCoreUtil_DBUtilDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCoreUtil_DBUtilDlg)
	m_szDSN = _T("DirectPrimary");
	m_nTableIndex = 0;
	m_szPW = _T("");
	m_szTableName = _T("TableName");
	m_szUser = _T("sa");
	m_szColName = _T("More");
	m_nColSize = 29;
	m_nColType = DB_TYPE_VSTR;
	//}}AFX_DATA_INIT
	m_pdbconn = NULL;
}


void CCoreUtil_DBUtilDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCoreUtil_DBUtilDlg)
	DDX_Control(pDX, IDC_LIST_CONNS, m_lc);
	DDX_Text(pDX, IDC_EDIT_DSN, m_szDSN);
	DDX_Text(pDX, IDC_EDIT_INDEX, m_nTableIndex);
	DDX_Text(pDX, IDC_EDIT_PW, m_szPW);
	DDX_Text(pDX, IDC_EDIT_TABLE, m_szTableName);
	DDX_Text(pDX, IDC_EDIT_USER, m_szUser);
	DDX_Text(pDX, IDC_EDIT_COLNAME, m_szColName);
	DDX_Text(pDX, IDC_EDIT_COLSIZE, m_nColSize);
	DDX_Text(pDX, IDC_EDIT_COLTYPE, m_nColType);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCoreUtil_DBUtilDlg, CDialog)
	//{{AFX_MSG_MAP(CCoreUtil_DBUtilDlg)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_CONNS, OnItemchangedListConns)
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, OnButtonConnect)
	ON_BN_CLICKED(IDC_BUTTON_NEW, OnButtonNew)
	ON_BN_CLICKED(IDC_BUTTON_EXISTS, OnButtonExists)
	ON_BN_CLICKED(IDC_BUTTON_CREATETABLE, OnButtonCreatetable)
	ON_BN_CLICKED(IDC_BUTTON_GETTABLE, OnButtonGettable)
	ON_BN_CLICKED(IDC_BUTTON_DROPCOL, OnButtonDropcol)
	ON_BN_CLICKED(IDC_BUTTON_ADDCOL, OnButtonAddcol)
	ON_BN_CLICKED(IDC_BUTTON_RETBF, OnButtonRetbf)
	ON_BN_CLICKED(IDC_BUTTON_INDEX, OnButtonIndex)
	ON_BN_CLICKED(IDC_BUTTON_DELETE, OnButtonDelete)
	ON_BN_CLICKED(IDC_BUTTON_INSERT, OnButtonInsert)
	ON_BN_CLICKED(IDC_BUTTON_UPDATE, OnButtonUpdate)
	ON_BN_CLICKED(IDC_BUTTON_EXECSQL, OnButtonExecsql)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_DBUtilDlg message handlers

void CCoreUtil_DBUtilDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
//	CDialog::OnCancel();
}

void CCoreUtil_DBUtilDlg::OnOK() 
{
	// TODO: Add extra validation here
	
//	CDialog::OnOK();
}


BOOL CCoreUtil_DBUtilDlg::Create(CWnd* pParentWnd) 
{
	return CDialog::Create(CCoreUtil_DBUtilDlg::IDD, pParentWnd);
}

BOOL CCoreUtil_DBUtilDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
  CRect rcList;
  m_lc.GetWindowRect(&rcList);
	
  if(m_lc.InsertColumn(
    0, 
    "connections", 
    LVCFMT_LEFT, 
    rcList.Width()-20,
    -1
    ) <0) AfxMessageBox("could not add connections column");
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CCoreUtil_DBUtilDlg::OnItemchangedListConns(NMHDR* pNMHDR, LRESULT* pResult) 
{

	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	if(pNMListView->uChanged == LVIF_STATE)
	{
		if(!((pNMListView->uOldState&(LVNI_SELECTED)))
			&&(pNMListView->uNewState&(LVNI_SELECTED)))
		{
			CString szText = "Selected connection: ";
			szText+=m_lc.GetItemText( (pNMListView->iItem), 0);

			GetDlgItem(IDC_STATIC_SELCONN)->SetWindowText(szText);
			char dsn[1024];
			sprintf(dsn, "%s", m_lc.GetItemText( (pNMListView->iItem), 0));
//				AfxMessageBox(dsn);
			m_pdbconn = m_db.ConnectionExists(dsn);
			if(m_pdbconn==NULL) AfxMessageBox("could not find connection");
/*
			else
			{
				sprintf(dsn, "%s %s %s", m_pdbconn->m_pszDSNname, m_pdbconn->m_pszLogin, m_pdbconn->m_pszPw);
				AfxMessageBox(dsn);

			}
*/
		}
	}
	
	*pResult = 0;
}

void DirectMainThread(void* pvArgs);

void CCoreUtil_DBUtilDlg::OnButtonConnect() 
{

	if(m_pdbconn)
	{
		char error[DB_ERRORSTRING_LEN];
		if(m_db.ConnectDatabase(m_pdbconn, error)==DB_SUCCESS) 
		{
	GetDlgItem(IDC_EDIT_INFO)->SetWindowText("Connected.");
		}
		else
		{
			AfxMessageBox(error);
		}
	}

	/*
	if(_beginthread(DirectMainThread, 0, (void*) this)<0)
	{
		AfxMessageBox("Direct could not begin main thread!\nMust exit.");
	}
	*/
}

void DirectMainThread(void* pvArgs)
{
	CCoreUtil_DBUtilDlg* pdlg = (CCoreUtil_DBUtilDlg*)pvArgs;
	if(pdlg->m_pdbconn)
	{
		char error[DB_ERRORSTRING_LEN];
		if(pdlg->m_db.ConnectDatabase(pdlg->m_pdbconn, error)==DB_SUCCESS) 
		{
	AfxMessageBox("Connected.");
		}
		else
		{
			AfxMessageBox(error);
		}
	}

}


void CCoreUtil_DBUtilDlg::OnButtonNew() 
{
	UpdateData(TRUE);
	char dsn[1024];
	sprintf(dsn, "%s",m_szDSN);
	char user[1024];
	sprintf(user, "%s",m_szUser);
	char pw[1024];
	sprintf(pw, "%s",m_szPW);
//AfxMessageBox("X");
	CDBconn* pdbconn = m_db.CreateNewConnection(dsn, user, pw);
//AfxMessageBox("Y");
	if(pdbconn==NULL)
	{
		AfxMessageBox("Could not create connection");
	}
	else
	{
		m_lc.InsertItem( m_lc.GetItemCount(), dsn );
	}	
		Display();

}

void CCoreUtil_DBUtilDlg::OnButtonExists() 
{
	if(m_pdbconn)
	{
		UpdateData(TRUE);
		char table[1024];
		sprintf(table, "%s",m_szTableName);

		if(m_db.TableExists(m_pdbconn, table)==DB_EXISTS) 
			AfxMessageBox("Table exists.");
		else
			AfxMessageBox("Error, or table does not exist.");

	}
		Display();

}

void CCoreUtil_DBUtilDlg::OnButtonCreatetable() 
{
	if(m_pdbconn)
	{
		UpdateData(TRUE);
		char table[1024];
		sprintf(table, "%s",m_szTableName);

		if(m_db.TableExists(m_pdbconn, table)==DB_EXISTS) 
			AfxMessageBox("Table already exists.");
		else
		{
			
			int x = m_db.AddTable(m_pdbconn, table);
			if(x!=DB_SUCCESS)
			{
		char error[1024];
		sprintf(error, "table %s not added. %d", table, x);
				AfxMessageBox(error);	
				return;

			}
				

			CDBfield* pfld = new CDBfield[2];
			pfld[0].m_pszData = (char*)malloc(6);
			sprintf(pfld[0].m_pszData, "First");
			pfld[0].m_ucType = DB_TYPE_BYTE;
			pfld[0].m_usSize = 1;
			
			pfld[1].m_pszData = (char*)malloc(7);
			sprintf(pfld[1].m_pszData, "Second");
			pfld[1].m_ucType = DB_TYPE_VSTR;
			pfld[1].m_usSize = 25;


			x = m_db.SetTableInfo(m_pdbconn, m_db.GetTableIndex(m_pdbconn, table), table, pfld, 2);
			if(x!=DB_SUCCESS)
			{
		sprintf(table, "info not set. %d", x);
				AfxMessageBox(table);	
				return;
			}	



			char errorstring[DB_ERRORSTRING_LEN];

			x = m_db.CreateTable(m_pdbconn, m_db.GetTableIndex(m_pdbconn, table), errorstring);
			if(x==DB_SUCCESS)
			AfxMessageBox("Table created.");
			else
			{
		sprintf(table, "create returned %d\n%s", x, errorstring);
				AfxMessageBox(table);	
			}	
		}
	}
	else AfxMessageBox("no connection");

	Display();
	
}

void CCoreUtil_DBUtilDlg::OnButtonGettable() 
{
	if(m_pdbconn)
	{
		UpdateData(TRUE);
		char table[1024];
		sprintf(table, "%s",m_szTableName);
		char error[1024];

int x = m_db.GetTableInfo(m_pdbconn, table, DB_UPDATEINFO, error);
		if(x!=DB_SUCCESS) 
		{
			sprintf(table, "GetTableInfo returned %d\n%s",x, error);
			AfxMessageBox(table);
		}
		else
		{

			int i=0;
			int nindex = m_db.GetTableIndex(m_pdbconn, table);
			sprintf(error, "numfields %d\n\n", m_pdbconn->m_pdbTable[nindex].m_usNumFields);
			while(i<m_pdbconn->m_pdbTable[nindex].m_usNumFields)
			{
				sprintf(table, "%s, %d, %d\n", 
				m_pdbconn->m_pdbTable[nindex].m_pdbField[i].m_pszData, 
				m_pdbconn->m_pdbTable[nindex].m_pdbField[i].m_ucType, 
				m_pdbconn->m_pdbTable[nindex].m_pdbField[i].m_usSize); 

				strcat(error, table);
				i++;
			}


			AfxMessageBox(error);	
		}
	}
	else AfxMessageBox("no connection");
		Display();

	
}

void CCoreUtil_DBUtilDlg::OnButtonDropcol() 
{
	if(m_pdbconn)
	{
		UpdateData(TRUE);
		char col[1024];
		sprintf(col, "%s",m_szColName);
		char error[1024];
		char table[1024];
		sprintf(table, "%s",m_szTableName);

			int nindex = m_db.GetTableIndex(m_pdbconn, table);

 
int x = m_db.DropColumn(m_pdbconn, nindex, col, error);
		if(x!=DB_SUCCESS) 
		{
			sprintf(col, "DropColumn returned %d\n%s",x, error);
			AfxMessageBox(col);
		}
		else
		{
			 x = m_db.GetTableInfo(m_pdbconn, m_pdbconn->m_pdbTable[nindex].m_pszName, DB_UPDATEINFO, error);
			if(x==DB_SUCCESS) 
			{


				int i=0;
				
				sprintf(error, "numfields %d\n", m_pdbconn->m_pdbTable[nindex].m_usNumFields);
				while(i<m_pdbconn->m_pdbTable[nindex].m_usNumFields)
				{
					sprintf(col, "%s, %d, %d\n", 
					m_pdbconn->m_pdbTable[nindex].m_pdbField[i].m_pszData, 
					m_pdbconn->m_pdbTable[nindex].m_pdbField[i].m_ucType, 
					m_pdbconn->m_pdbTable[nindex].m_pdbField[i].m_usSize); 

					strcat(error, col);
					i++;
				}


				AfxMessageBox(error);	
			}
		}
	}
	else AfxMessageBox("no connection");
	
			Display();

}

void CCoreUtil_DBUtilDlg::OnButtonAddcol() 
{
	if(m_pdbconn)
	{
		UpdateData(TRUE);
		char col[1024];
		sprintf(col, "%s",m_szColName);
		char error[1024];
		char table[1024];
		sprintf(table, "%s",m_szTableName);

			int nindex = m_db.GetTableIndex(m_pdbconn, table);

CDBfield dbField;
dbField.m_pszData = col;
dbField.m_ucType = m_nColType;
dbField.m_usSize = m_nColSize;


 
int x = m_db.AddColumn(m_pdbconn, nindex, dbField, error);
		if(x!=DB_SUCCESS) 
		{
			sprintf(col, "AddColumn returned %d\n%s",x, error);
			AfxMessageBox(col);
		}
		else
		{
			 x = m_db.GetTableInfo(m_pdbconn, m_pdbconn->m_pdbTable[nindex].m_pszName, DB_UPDATEINFO, error);
			if(x==DB_SUCCESS) 
			{


				int i=0;
				
				sprintf(error, "numfields %d\n", m_pdbconn->m_pdbTable[nindex].m_usNumFields);
				while(i<m_pdbconn->m_pdbTable[nindex].m_usNumFields)
				{
					sprintf(col, "%s, %d, %d\n", 
					m_pdbconn->m_pdbTable[nindex].m_pdbField[i].m_pszData, 
					m_pdbconn->m_pdbTable[nindex].m_pdbField[i].m_ucType, 
					m_pdbconn->m_pdbTable[nindex].m_pdbField[i].m_usSize); 

					strcat(error, col);
					i++;
				}


				AfxMessageBox(error);	
			}
		}
	}
	else AfxMessageBox("no connection");
	
	Display();
	
}

void CCoreUtil_DBUtilDlg::OnButtonIndex() 
{
	if(m_pdbconn)
	{
		UpdateData(TRUE);
		char table[1024];
		char msg[1024];
		sprintf(table, "%s",m_szTableName);

		int x = m_db.GetTableIndex(m_pdbconn, table);
		sprintf(msg, "index of %s is %d", table, x);
		AfxMessageBox(msg);
	}
	Display();

}

void CCoreUtil_DBUtilDlg::Display()
{
	CString szText="";
	CString szTemp="";
	if(m_pdbconn)
	{
	// build the string
		for(int i=0; i<m_pdbconn->m_usNumTables; i++)
		{
			szText+= m_pdbconn->m_pdbTable[i].m_pszName;
			szText+="\r\n   ";

			for(int j=0; j<m_pdbconn->m_pdbTable[i].m_usNumFields; j++)
			{
				szTemp.Format("[%s]", m_pdbconn->m_pdbTable[i].m_pdbField[j].m_pszData);
				szText+=szTemp;
			}
			szText+="\r\n";
		}
	}
	else szText = "no connection";

	GetDlgItem(IDC_EDIT_INFO)->SetWindowText(szText);
}


void CCoreUtil_DBUtilDlg::OnButtonDelete() 
{
// use a "default record" where all strings are "x" and all numbers are 1
	if(m_pdbconn)
	{
		UpdateData(TRUE);
		char error[1024];
		char table[1024];
		sprintf(table, "%s",m_szTableName);

		int nindex = m_db.GetTableIndex(m_pdbconn, table);
		if(nindex>=0)
		{
			CDBCriterion* pdbcCriteria = new CDBCriterion(m_pdbconn, nindex);
			CDBCriterion* pdbcCriteria2 = new CDBCriterion(m_pdbconn, nindex);

			// load up the criteria.
			for(int i=0; i<m_pdbconn->m_pdbTable[nindex].m_usNumFields; i++)
			{
				pdbcCriteria->m_pdbField[i].m_pszData = (char*)malloc(2);
				pdbcCriteria2->m_pdbField[i].m_pszData = (char*)malloc(2);
				if( (m_pdbconn->m_pdbTable[nindex].m_pdbField[i].m_ucType==DB_TYPE_STR)
					||(m_pdbconn->m_pdbTable[nindex].m_pdbField[i].m_ucType==DB_TYPE_VSTR)
					||(m_pdbconn->m_pdbTable[nindex].m_pdbField[i].m_ucType==DB_TYPE_TEXT) )
				{
					if(pdbcCriteria->m_pdbField[i].m_pszData)
						strcpy(pdbcCriteria->m_pdbField[i].m_pszData, "x");
					pdbcCriteria->m_pdbField[i].m_ucType=DB_OP_EQUAL;
				}
				else
				if(m_pdbconn->m_pdbTable[nindex].m_pdbField[i].m_ucType==DB_TYPE_DATA)
				{
					if(pdbcCriteria->m_pdbField[i].m_pszData)
						strcpy(pdbcCriteria->m_pdbField[i].m_pszData, "");
						pdbcCriteria->m_pdbField[i].m_ucType=DB_OP_IGNORE;
				}
				else
				{
					if(pdbcCriteria->m_pdbField[i].m_pszData)
						strcpy(pdbcCriteria->m_pdbField[i].m_pszData, "3");
						pdbcCriteria->m_pdbField[i].m_ucType=DB_OP_GT;
					if(pdbcCriteria2->m_pdbField[i].m_pszData)
						strcpy(pdbcCriteria2->m_pdbField[i].m_pszData, "9");
						pdbcCriteria2->m_pdbField[i].m_ucType=DB_OP_LT;
				}
			}

			int x = m_db.DeleteByFields(pdbcCriteria, pdbcCriteria2, error);
			if(x!=DB_SUCCESS) 
			{
				sprintf(table, "DeleteByFields returned %d\n%s",x, error);
				AfxMessageBox(table);
			}
			else
			{
				sprintf(error, "Item deleted from %s", table);
				AfxMessageBox(error);
			}
		}
	}	
}

void CCoreUtil_DBUtilDlg::OnButtonInsert() 
{
// use a "default record" where all strings are "x" and all numbers are 1
	if(m_pdbconn)
	{
		UpdateData(TRUE);
		char error[1024];
		char table[1024];
		sprintf(table, "%s",m_szTableName);

		int nindex = m_db.GetTableIndex(m_pdbconn, table);
		if(nindex>=0)
		{
			CDBCriterion* pdbcCriteria = new CDBCriterion(m_pdbconn, nindex);

			// load up the criteria.
			for(int i=0; i<m_pdbconn->m_pdbTable[nindex].m_usNumFields; i++)
			{
				pdbcCriteria->m_pdbField[i].m_pszData = (char*)malloc(2);
				if( (m_pdbconn->m_pdbTable[nindex].m_pdbField[i].m_ucType==DB_TYPE_STR)
					||(m_pdbconn->m_pdbTable[nindex].m_pdbField[i].m_ucType==DB_TYPE_VSTR)
					||(m_pdbconn->m_pdbTable[nindex].m_pdbField[i].m_ucType==DB_TYPE_TEXT) )
				{
					if(pdbcCriteria->m_pdbField[i].m_pszData)
						strcpy(pdbcCriteria->m_pdbField[i].m_pszData, "x");
				pdbcCriteria->m_pdbField[i].m_ucType=DB_OP_EQUAL;
				}
				else
				if(m_pdbconn->m_pdbTable[nindex].m_pdbField[i].m_ucType==DB_TYPE_DATA)
				{
					if(pdbcCriteria->m_pdbField[i].m_pszData)
						strcpy(pdbcCriteria->m_pdbField[i].m_pszData, "");
						pdbcCriteria->m_pdbField[i].m_ucType=DB_OP_IGNORE;
				}
				else
				{
					if(pdbcCriteria->m_pdbField[i].m_pszData)
						strcpy(pdbcCriteria->m_pdbField[i].m_pszData, "1");
						pdbcCriteria->m_pdbField[i].m_ucType=DB_OP_EQUAL;
				}

			}

			int x = m_db.Insert(pdbcCriteria, error);
			if(x!=DB_SUCCESS) 
			{
				sprintf(table, "Insert returned %d\n%s",x, error);
				AfxMessageBox(table);
			}
			else
			{
				sprintf(error, "Item inserted into %s", table);
				AfxMessageBox(error);
			}
		}
	}	
	
}

void CCoreUtil_DBUtilDlg::OnButtonUpdate() 
{
// use a "default record" where all strings are "x" and all numbers are 1, change numbers to 2 and strings to "y"
	if(m_pdbconn)
	{
		UpdateData(TRUE);
		char error[1024];
		char table[1024];
		sprintf(table, "%s",m_szTableName);

		int nindex = m_db.GetTableIndex(m_pdbconn, table);
		if(nindex>=0)
		{
			CDBCriterion* pdbcValues = new CDBCriterion(m_pdbconn, nindex);
			CDBCriterion* pdbcCriteria = new CDBCriterion(m_pdbconn, nindex);
			CDBCriterion* pdbcCriteria2 = new CDBCriterion(m_pdbconn, nindex);

			// load up the criteria.
			for(int i=0; i<m_pdbconn->m_pdbTable[nindex].m_usNumFields; i++)
			{
				pdbcCriteria->m_pdbField[i].m_pszData = (char*)malloc(2);
				pdbcCriteria2->m_pdbField[i].m_pszData = (char*)malloc(2);
				pdbcValues->m_pdbField[i].m_pszData = (char*)malloc(2);
				if( (m_pdbconn->m_pdbTable[nindex].m_pdbField[i].m_ucType==DB_TYPE_STR)
					||(m_pdbconn->m_pdbTable[nindex].m_pdbField[i].m_ucType==DB_TYPE_VSTR)
					||(m_pdbconn->m_pdbTable[nindex].m_pdbField[i].m_ucType==DB_TYPE_TEXT) )
				{
					if(pdbcCriteria->m_pdbField[i].m_pszData)
						strcpy(pdbcCriteria->m_pdbField[i].m_pszData, "x");
					if(pdbcValues->m_pdbField[i].m_pszData)
						strcpy(pdbcValues->m_pdbField[i].m_pszData, "y");
				pdbcCriteria->m_pdbField[i].m_ucType=DB_OP_EQUAL;
				pdbcValues->m_pdbField[i].m_ucType=DB_OP_EQUAL;
				}
				else
				if(m_pdbconn->m_pdbTable[nindex].m_pdbField[i].m_ucType==DB_TYPE_DATA)
				{
					if(pdbcCriteria->m_pdbField[i].m_pszData)
						strcpy(pdbcCriteria->m_pdbField[i].m_pszData, "");
						pdbcCriteria->m_pdbField[i].m_ucType=DB_OP_IGNORE;
					if(pdbcValues->m_pdbField[i].m_pszData)
						strcpy(pdbcValues->m_pdbField[i].m_pszData, "");
						pdbcCriteria->m_pdbField[i].m_ucType=DB_OP_IGNORE;
				}
				else
				{
					if(pdbcCriteria->m_pdbField[i].m_pszData)
						strcpy(pdbcCriteria->m_pdbField[i].m_pszData, "1");
						pdbcCriteria->m_pdbField[i].m_ucType=DB_OP_EQUAL|DB_OP_GT;
					if(pdbcValues->m_pdbField[i].m_pszData)
						strcpy(pdbcValues->m_pdbField[i].m_pszData, "2");
						pdbcValues->m_pdbField[i].m_ucType=DB_OP_EQUAL;
					if(pdbcCriteria2->m_pdbField[i].m_pszData)
						strcpy(pdbcCriteria2->m_pdbField[i].m_pszData, "4");
						pdbcCriteria2->m_pdbField[i].m_ucType=DB_OP_LT;
				}
			}

			int x = m_db.Update(pdbcValues, pdbcCriteria, pdbcCriteria2, error);
			if(x!=DB_SUCCESS) 
			{
				sprintf(table, "Update returned %d\n%s",x, error);
				AfxMessageBox(table);
			}
			else
			{
				sprintf(error, "Item updated in %s", table);
				AfxMessageBox(error);
			}
		}
	}	

}

void CCoreUtil_DBUtilDlg::OnButtonRetbf() 
{
// use a "default record" where all strings are "x" and all numbers are 1
	if(m_pdbconn)
	{
		UpdateData(TRUE);
		char error[1024];
		char table[1024];
		sprintf(table, "%s",m_szTableName);

		int nindex = m_db.GetTableIndex(m_pdbconn, table);
		if(nindex>=0)
		{
			CDBCriterion* pdbcCriteria = new CDBCriterion(m_pdbconn, nindex);
			CDBCriterion* pdbcCriteria2 = new CDBCriterion(m_pdbconn, nindex);
			CDBCriterion* pdbcOrderCriteria = new CDBCriterion(m_pdbconn, nindex);

			// load up the criteria.
			for(int i=0; i<m_pdbconn->m_pdbTable[nindex].m_usNumFields; i++)
			{
				pdbcCriteria->m_pdbField[i].m_pszData = (char*)malloc(2);
				pdbcCriteria2->m_pdbField[i].m_pszData = (char*)malloc(2);
				if( (m_pdbconn->m_pdbTable[nindex].m_pdbField[i].m_ucType==DB_TYPE_STR)
					||(m_pdbconn->m_pdbTable[nindex].m_pdbField[i].m_ucType==DB_TYPE_VSTR)
					||(m_pdbconn->m_pdbTable[nindex].m_pdbField[i].m_ucType==DB_TYPE_TEXT) )
				{
					if(pdbcCriteria->m_pdbField[i].m_pszData)
						strcpy(pdbcCriteria->m_pdbField[i].m_pszData, "y");
					pdbcCriteria->m_pdbField[i].m_ucType=DB_OP_EQUAL;
				}
				else
				if(m_pdbconn->m_pdbTable[nindex].m_pdbField[i].m_ucType==DB_TYPE_DATA)
				{
					if(pdbcCriteria->m_pdbField[i].m_pszData)
						strcpy(pdbcCriteria->m_pdbField[i].m_pszData, "");
						pdbcCriteria->m_pdbField[i].m_ucType=DB_OP_IGNORE;
				}
				else
				{
					if(pdbcCriteria->m_pdbField[i].m_pszData)
						strcpy(pdbcCriteria->m_pdbField[i].m_pszData, "0");
						pdbcCriteria->m_pdbField[i].m_ucType=DB_OP_GT;
					if(pdbcCriteria2->m_pdbField[i].m_pszData)
						strcpy(pdbcCriteria2->m_pdbField[i].m_pszData, "9");
						pdbcCriteria2->m_pdbField[i].m_ucType=DB_OP_LT;
//						pdbcOrderCriteria->m_pdbField[i].m_ucType=DB_ORD_IGNORE;
				}
			}

			CRecordset* prs = m_db.RetrieveByFields(pdbcCriteria, pdbcCriteria2, pdbcOrderCriteria, error);
			if(prs == NULL) 
			{
				sprintf(table, "RetrieveByFields returned NULL\n%s", error);
				AfxMessageBox(table);
			}
			else
			{
				// load up display with the items.

				CDBRecord* pdbrData;
				unsigned long ulNumRecords=0;

//				AfxMessageBox("here");
			int x = m_db.Convert(prs, pdbcCriteria, &pdbrData, &ulNumRecords, error);
//				AfxMessageBox("here2");
				if(x != DB_SUCCESS) 
				{
					sprintf(table, "Convert returned %d\n%s", x, error);
					AfxMessageBox(table);
				}
				else
				{
					// load up display with the items.
					sprintf(error, "%d items retrieved from %s\r\n", ulNumRecords, table);
//				AfxMessageBox("error");
					for(x=0; x<(int)ulNumRecords; x++)
					{
						if(pdbrData[x].m_ppszData)
						{
							strcat(error, "  ");
							unsigned short i=0;
							while(pdbrData[x].m_ppszData[i]!=NULL)
							{
//				AfxMessageBox(pdbrData[x].m_ppszData[i]);

								if(i>0) strcat(error, ", ");
								strcat(error, pdbrData[x].m_ppszData[i++]);
							}
							pdbrData[x].Free();  // must free explicitly, per record
						}

						strcat(error, "\r\n");

					}
	GetDlgItem(IDC_EDIT_INFO)->SetWindowText(error);
//						AfxMessageBox(error);

				}

			}
		}
	}	
	
	
}


void CCoreUtil_DBUtilDlg::OnButtonExecsql() 
{
	CString szSQL;
	GetDlgItem(IDC_EDIT_SQL)->GetWindowText(szSQL);

	if(m_pdbconn)
	{
		char error[1024];
		char sql[1024];
		sprintf(sql, "%s",szSQL);

		int x = m_db.ExecuteSQL(m_pdbconn, sql, error);

		if(x != DB_SUCCESS) 
		{
			sprintf(sql, "ExecuteSQL returned %d\n%s", x, error);
			AfxMessageBox(sql);
		}
		else
		{
			AfxMessageBox("success");
		}
	}
}
