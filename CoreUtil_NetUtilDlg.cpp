// CoreUtil_NetUtilDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CoreUtil.h"
#include "CoreUtil_NetUtilDlg.h"
#include "..\Common\TXT\BufferUtil.h"
#include <process.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


void TestDatagramServerHandlerThread(void* pvArgs);
void TestServerHandlerThread(void* pvArgs);
CWnd* g_pwnd;

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_NetUtilDlg dialog


CCoreUtil_NetUtilDlg::CCoreUtil_NetUtilDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCoreUtil_NetUtilDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCoreUtil_NetUtilDlg)
	m_szData = _T("PLY:x1234567890");
	m_nPort = 10560;
	m_nCmd = 25;
	m_nSubCmd = 0;
	m_nSendPort = 10540;
	m_bPersist = TRUE;
	m_nTimerMS = 100;
	m_bSimple = TRUE;
	m_szHost = _T("192.168.0.108");
	m_bFromFile = FALSE;
	m_bForceScmd = FALSE;
	m_bUDP = FALSE;
	m_bSendNoData = FALSE;
	//}}AFX_DATA_INIT

	m_nPortSelected = 0;
	m_bTimerSet = false;
}


void CCoreUtil_NetUtilDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCoreUtil_NetUtilDlg)
	DDX_Control(pDX, IDC_LIST_SERVERS, m_lc);
	DDX_Text(pDX, IDC_EDIT_C_DATA, m_szData);
	DDX_Text(pDX, IDC_EDIT_S_PORT, m_nPort);
	DDX_Text(pDX, IDC_EDIT_C_CMD, m_nCmd);
	DDX_Text(pDX, IDC_EDIT_C_SCMD, m_nSubCmd);
	DDX_Text(pDX, IDC_EDIT_C_PORT, m_nSendPort);
	DDX_Check(pDX, IDC_CHECK_PERSIST, m_bPersist);
	DDX_Text(pDX, IDC_EDIT_TIMERMS, m_nTimerMS);
	DDX_Check(pDX, IDC_CHECK_SIMPLE, m_bSimple);
	DDX_CBString(pDX, IDC_COMBO_HOST, m_szHost);
	DDX_Check(pDX, IDC_CHECK_FROMFILE, m_bFromFile);
	DDX_Check(pDX, IDC_CHECK_FORCESCMD, m_bForceScmd);
	DDX_Check(pDX, IDC_CHECK_UDP, m_bUDP);
	DDX_Check(pDX, IDC_CHECK_NODATA, m_bSendNoData);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCoreUtil_NetUtilDlg, CDialog)
	//{{AFX_MSG_MAP(CCoreUtil_NetUtilDlg)
	ON_BN_CLICKED(IDC_BUTTON_S_LISTEN, OnButtonSListen)
	ON_BN_CLICKED(IDC_BUTTON_S_REMOVE, OnButtonSRemove)
	ON_BN_CLICKED(IDC_BUTTON_C_SEND, OnButtonCSend)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_SERVERS, OnItemchangedListServers)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_TIMER, OnButtonTimer)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_CHECK_NODATA, OnCheckNodata)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_NetUtilDlg message handlers

void CCoreUtil_NetUtilDlg::OnCancel() 
{

//	CDialog::OnCancel();
}

void CCoreUtil_NetUtilDlg::OnOK() 
{
//	CDialog::OnOK();
}

BOOL CCoreUtil_NetUtilDlg::Create(CWnd* pParentWnd) 
{
	return CDialog::Create(CCoreUtil_NetUtilDlg::IDD, pParentWnd);
}

void CCoreUtil_NetUtilDlg::OnButtonSListen() 
{
	UpdateData(TRUE);
	if(m_nPort>0)
	{
		CNetServer* pServer = new CNetServer;
		char buffer[NET_ERRORSTRING_LEN];
		pServer->m_usPort = m_nPort;
		pServer->m_lpfnHandler = TestServerHandlerThread;
		if(m_bPersist)
			pServer->m_ucType |= NET_TYPE_KEEPOPEN;

		if(m_bUDP)
		{
			pServer->m_lpfnHandler = TestDatagramServerHandlerThread;
			pServer->m_nType = SOCK_DGRAM; 
			pServer->m_nProtocol = 0;
		}

		if(m_net.StartServer(pServer, NULL, 10000, buffer )<NET_SUCCESS)
		{
			AfxMessageBox(buffer);
		}
		else
		{
			sprintf(buffer, "%05d listening (0)", m_nPort);
			m_lc.InsertItem( m_lc.GetItemCount(), buffer );
		}
	}
}

void CCoreUtil_NetUtilDlg::OnButtonSRemove() 
{
	if(m_nPortSelected>0)
	{
		char buffer[NET_ERRORSTRING_LEN];
		int nItem = m_lc.GetNextItem( -1,  LVNI_SELECTED );
		if(nItem>=0)
		{
			if(m_net.StopServer(m_nPortSelected, 5000, buffer)<NET_SUCCESS)
			{
				AfxMessageBox(buffer);
			}
			else
			{
				strcpy(m_chConns,"");
				m_lc.DeleteItem( nItem );
			}
		}
	}
}

void CCoreUtil_NetUtilDlg::OnButtonCSend() 
{
	CWaitCursor cw;
	UpdateData(TRUE);

	if(((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->FindStringExact( -1, m_szHost )==CB_ERR)
	{
		((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->AddString(m_szHost);
		((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->SelectString(-1, m_szHost);
	}


	CNetData data;

	data.m_ucType = NET_TYPE_PROTOCOL1;      // defined type - indicates which protocol to use, structure of data
	data.m_ucCmd = m_nCmd&0xff;       // the command byte
	data.m_ucSubCmd = m_nSubCmd&0xff;       // the subcommand byte

	if((m_nSubCmd>0)||(m_bForceScmd)) data.m_ucType |= NET_TYPE_HASSUBC; 

	
	data.m_pucData = NULL;
	data.m_ulDataLen = m_szData.GetLength();
	if(data.m_ulDataLen>0)
	{
		if(m_bFromFile)
		{
			CString foo; foo.Format("opening %s", m_szData);
			AfxMessageBox(foo);
			FILE* fp = fopen(m_szData, "rb");

			if(fp)
			{
				fseek(fp,0,SEEK_END);
				unsigned long ulLen = ftell(fp);

				if(ulLen>0)
				{	
					fseek(fp,0,SEEK_SET);
					// allocate buffer and read in file contents
					char* pch = (char*) malloc(ulLen+1); // for 0
					if(pch!=NULL)
					{
			foo.Format("len=%d", ulLen);
			AfxMessageBox(foo);
						fread(pch,sizeof(char),ulLen,fp);
						memset(pch+ulLen, 0, 1);
			AfxMessageBox(pch);
						data.m_pucData = (unsigned char*)pch;
						data.m_ulDataLen = ulLen;
					}
				}

				fclose(fp);
			}
			else
			{
			AfxMessageBox("no file");

			}

		}
		else
		{
			if(m_szData.GetAt(0)=='[')
			{
				// send a hex string

				unsigned long ulBufLen = m_szData.GetLength();
				char* pchBuffer = m_szData.GetBuffer(0);
				CBufferUtil bu;
				char* pch = bu.DecodeReadableHex(pchBuffer, &ulBufLen);
				if(pch)
				{
					data.m_pucData = (unsigned char*)pch;
					data.m_ulDataLen = ulBufLen;
				}

			}
			else
			{
				char* pch = (char*)malloc(data.m_ulDataLen+1);     // pointer to the payload data
				if(pch)
				{
						strcpy(pch, m_szData);
						data.m_pucData = (unsigned char*)pch;
				}
			}
			
		}
	}
	char buffer[NET_ERRORSTRING_LEN];


	char host[256];

	if(m_bSimple)
	{
		_snprintf(buffer, NET_ERRORSTRING_LEN-1, "%d: datalen:%d  data:%s", 
			clock(),
				data.m_ulDataLen,
				data.m_pucData==NULL?"":(char*)data.m_pucData				
			);
	}
	else
	{
		_snprintf(buffer, NET_ERRORSTRING_LEN-1, "%d: type:%d, cmd:%d, scmd:%d, datalen:%d  data:%s", 
			clock(),
				data.m_ucType,
				data.m_ucCmd,
				data.m_ucSubCmd,
				data.m_ulDataLen,
				data.m_pucData==NULL?"":(char*)data.m_pucData				
			);
	}
	GetDlgItem(IDC_EDIT_C_REPLY)->SetWindowText(buffer);

	if(m_bPersist)	
	{
		sprintf(host, "[%s:%d:", m_szHost, m_nSendPort);

		SOCKET s;
		int nreturn = NET_SUCCESS;
		char* pch = strstr(m_chConns, host);
		if(pch==NULL)  // first time.
		{
			strcpy(host, m_szHost);
			if(m_bUDP)
			{
				nreturn = m_net.OpenConnection(host, m_nSendPort, &s, 5000, 5000, buffer,	AF_INET, SOCK_DGRAM);
			}
			else
			{
				nreturn = m_net.OpenConnection(host, m_nSendPort, &s, 5000, 5000, buffer);
			}
			if(nreturn<NET_SUCCESS)
			{
				char snarf[2000];
				sprintf(snarf, "Persist open: %s", buffer);
				AfxMessageBox(snarf);
//				AfxMessageBox(m_chConns);
				if(m_bTimerSet) OnButtonTimer(); // dont keep on in error condition.

			}
			else
			{
				sprintf(host, "[%s:%d:", m_szHost, m_nSendPort);
				strcat(m_chConns, host);
				sprintf(host, "%ld]", s);
				strcat(m_chConns, host);

//				AfxMessageBox(m_chConns);
			}
		}
		else
		{
			s = atol(pch+strlen(host));
			sprintf(host, "socket: %ld", s);
//				AfxMessageBox(host);
		}




		if(nreturn>=NET_SUCCESS)
		{
			unsigned long ulTime = clock();

			if(m_bSimple)
			{
				if(m_net.SendLine(data.m_pucData, data.m_ulDataLen, s, EOLN_NONE, true, 5000, buffer)<NET_SUCCESS)
				{
					if(m_bTimerSet) OnButtonTimer(); // dont keep on in error condition.
				}
				else
				{
					unsigned long ulTime2 = clock();
					// receive something
					m_net.GetLine(&data.m_pucData, &data.m_ulDataLen, s, NET_RCV_ONCE, buffer);
					char* pch = NULL;
					if((data.m_ulDataLen)&&(data.m_pucData)) pch = (char*)malloc(data.m_ulDataLen+1);
					if(pch)
					{
						memcpy(pch, data.m_pucData, data.m_ulDataLen);
						*(pch+data.m_ulDataLen)=0;
					}

					_snprintf(buffer, NET_ERRORSTRING_LEN-1, "received %d: datalen:%d  data:%s\r\ntransaction time %d ms, ack time %d ms, total %d ms",
						clock(),
							data.m_ulDataLen,
							pch==NULL?"":pch,
							ulTime2 - ulTime,
							clock() - ulTime2,
							clock() - ulTime
						);
					if(pch) free(pch);
					GetDlgItem(IDC_EDIT_C_REPLY)->SetWindowText(buffer);

				}
			}
			else
			{
				if(m_net.SendData(&data, s, 5000, 0, NET_SND_CMDTOSVR|NET_SND_KEEPOPENRMT, buffer)<NET_SUCCESS)
				{
					if(m_bTimerSet) OnButtonTimer(); // dont keep on in error condition.
					if(data.m_ucCmd != NET_CMD_NAK)  // check the comm error if NAK didnt come thru ok
					{
						char snarf[2000];
						sprintf(snarf, "Persist send: %s", buffer);
						AfxMessageBox(snarf);
						// no need to nak back
						//m_net.SendData(&data, s, 5000, 0, NET_SND_NAK|NET_SND_NO_RX, buffer);
					}
				}
				else
				{
					unsigned long ulTime2 = clock();
					// ack back
					m_net.SendData(&data, s, 5000, 0, NET_SND_CLNTACK|NET_SND_KEEPOPENRMT, buffer);

					_snprintf(buffer, NET_ERRORSTRING_LEN-1, "%d: type:%d, cmd:%d, scmd:%d, datalen:%d  data:%s\r\ntransaction time %d ms, ack time %d ms, total %d ms\r\n0x%08x", 
						clock(),
							data.m_ucType,
							data.m_ucCmd,
							data.m_ucSubCmd,
							data.m_ulDataLen,
							data.m_pucData==NULL?"":(char*)data.m_pucData		,
							ulTime2 - ulTime,
							clock() - ulTime2,
							clock() - ulTime,
							NET_RCV_EOLN|EOLN_HTTP
						);
					GetDlgItem(IDC_EDIT_C_REPLY)->SetWindowText(buffer);

				}
			}
		}
	}
	else
	{

		// lets make sure theres no persistent connection now. if there is use it first to get rid of the conn,
		sprintf(host, "[%s:%d:", m_szHost, m_nSendPort);

		SOCKET s;
		int nreturn = NET_SUCCESS;
		bool bDisconn = false;
		char* pch = strstr(m_chConns, host);
		if(pch!=NULL)  // it exists.
		{
			s = atol(pch+strlen(host));
//			sprintf(host, "socket: %ld", s);
//				AfxMessageBox(host);
			bDisconn = true;

			//now have to remove it.

			int nLen = strlen(m_chConns);
			char* pch2 = strstr(pch, "]");
			memcpy(pch, pch2+1, (m_chConns+nLen) - pch2);

		}


		strcpy(host, m_szHost);

		if(m_bSendNoData)
		{
			_snprintf(buffer, NET_ERRORSTRING_LEN-1, "not sending data");
		}
		else


		if(m_bSimple)
		{
			_snprintf(buffer, NET_ERRORSTRING_LEN-1, "sending: %d: datalen:%d  data:%s", 
				clock(),
					data.m_ulDataLen,
					data.m_pucData==NULL?"":(char*)data.m_pucData				
				);

		}
		else
		{
			_snprintf(buffer, NET_ERRORSTRING_LEN-1, "sending: %d: checksum:%d, type:%d, cmd:%d, scmd:%d, datalen:%d  data:%s", 
				clock(),
				data.m_ulDataLen?(m_net.Checksum(data.m_pucData, data.m_ulDataLen)+data.m_ucType+data.m_ucCmd+data.m_ucSubCmd):(data.m_ucType+data.m_ucCmd+data.m_ucSubCmd),
					data.m_ucType,
					data.m_ucCmd,
					data.m_ucSubCmd,
					data.m_ulDataLen,
					data.m_pucData==NULL?"":(char*)data.m_pucData				
				);
		}
			GetDlgItem(IDC_EDIT_C_REPLY)->SetWindowText(buffer);

//			AfxMessageBox("sending");

			unsigned long ulTime = clock();
		if(m_bSimple)
		{
			if(bDisconn)
				m_net.CloseConnection(s);

			
			if(m_bUDP)
			{
//				AfxMessageBox("UDP");
				nreturn = m_net.OpenConnection(host, m_nSendPort, &s, 5000, 5000, buffer, AF_INET, SOCK_DGRAM, 0);
			}
			else
			{
//				AfxMessageBox("not UDP");
				nreturn = m_net.OpenConnection(host, m_nSendPort, &s, 5000, 5000, buffer);
			}

			if(m_bSendNoData)
			{
				// nothing!
			}
			else


			if(nreturn<NET_SUCCESS)
			{
				char snarf[2000];
				sprintf(snarf, "Non-persist open: %s", buffer);
				AfxMessageBox(snarf);
//				AfxMessageBox(m_chConns);
				if(m_bTimerSet) OnButtonTimer(); // dont keep on in error condition.

			}
			else
			{
				nreturn = m_net.SendLine(data.m_pucData, data.m_ulDataLen, s, EOLN_NONE, true, 5000, buffer);
				if(nreturn<NET_SUCCESS)
				{
					AfxMessageBox(buffer);
					if(m_bTimerSet) OnButtonTimer(); // dont keep on in error condition.
					
				}
				else
				{
					unsigned long ulTime2 = clock();
					// receive something
					m_net.GetLine(&data.m_pucData, &data.m_ulDataLen, s, NET_RCV_ONCE, buffer);
					char* pch = NULL;
					if((data.m_ulDataLen)&&(data.m_pucData)) pch = (char*)malloc(data.m_ulDataLen+1);
					if(pch)
					{
						memcpy(pch, data.m_pucData, data.m_ulDataLen);
						*(pch+data.m_ulDataLen)=0;
					}

					_snprintf(buffer, NET_ERRORSTRING_LEN-1, "received %d: datalen:%d  data:%s\r\ntransaction time %d ms, ack time %d ms, total %d ms",
						clock(),
							data.m_ulDataLen,
							pch==NULL?"":pch,
							ulTime2 - ulTime,
							clock() - ulTime2,
							clock() - ulTime
						);
					if(pch) free(pch);

					GetDlgItem(IDC_EDIT_C_REPLY)->SetWindowText(buffer);
				}
			}
		}
		else
		{

			if(m_bSendNoData)
			{
				_snprintf(buffer, NET_ERRORSTRING_LEN-1, "not sending data");
				GetDlgItem(IDC_EDIT_C_REPLY)->SetWindowText(buffer);
			}
			else
			{


				if(bDisconn)
					nreturn = m_net.SendData(&data, s, 5000, 0, NET_SND_CMDTOSVR, buffer);
				else
					nreturn = m_net.SendData(&data, host, m_nSendPort, 5000, 0, NET_SND_CMDTOSVR, &s, buffer);
			
				if(nreturn<NET_SUCCESS)
				{
		//			AfxMessageBox("failure");
						if(m_bTimerSet) OnButtonTimer(); // dont keep on in error condition.
					
					if(data.m_ucCmd != NET_CMD_NAK) // means we received a neg reply, so comm ok, but it doesnt expect a reply.
					{
						char snarf[2000];
						sprintf(snarf, "Transient send: %s", buffer);
						AfxMessageBox(snarf);
					}
				}
				else
				{
		//			AfxMessageBox("success");

						unsigned long ulTime2 = clock();
					// send the ack
					m_net.SendData(NULL, s, 5000, 0, NET_SND_CLNTACK, buffer);

						_snprintf(buffer, NET_ERRORSTRING_LEN-1, "success %d: type:%d, cmd:%d, scmd:%d, datalen:%d  data:%s\r\ntransaction time %d ms, ack time %d ms, total %d ms", 
							clock(),
								data.m_ucType,
								data.m_ucCmd,
								data.m_ucSubCmd,
								data.m_ulDataLen,
								data.m_pucData==NULL?"":(char*)data.m_pucData		,
								ulTime2 - ulTime,
								clock() - ulTime2,
								clock() - ulTime
							);
					GetDlgItem(IDC_EDIT_C_REPLY)->SetWindowText(buffer);
				}
			}
		}

		m_net.CloseConnection(s);
	}
}

BOOL CCoreUtil_NetUtilDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	FILE* fp = fopen("netlast.cfg", "rb");

	if(fp)
	{
		fseek(fp, 0, SEEK_END);
		unsigned long ulFileLen = ftell(fp);
		char* pchFile = (char*) malloc(ulFileLen+1); // term zero
		if(pchFile)
		{
			fseek(fp, 0, SEEK_SET);

			fread(pchFile, sizeof(char), ulFileLen, fp);
			*(pchFile+ulFileLen) = 0; // term zero

			int i=0; int nSel=-1;
			char* pch = strtok(pchFile, "\r\n");
			while(pch != NULL)
			{
				if(*pch=='*')
				{
					nSel = i;
					pch++;
				}
				((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->InsertString(i, pch);

				pch = strtok(NULL, "\r\n");
				i++;
			}
			if ((i>0)&&(nSel>=0)&&(nSel<i))
			{
				((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->SetCurSel(nSel);
				UpdateData(TRUE);
			}
			free(pchFile);

		}
		UpdateData(FALSE);
		fclose(fp);
	}

  CRect rcList;
  m_lc.GetWindowRect(&rcList);

	
  if(m_lc.InsertColumn(
    0, 
    "Listeners", 
    LVCFMT_LEFT, 
    rcList.Width()-20,
    -1
    ) <0) AfxMessageBox("could not add Listeners column");

	SetTimer(1, 1000, NULL); 
		g_pwnd = GetDlgItem(IDC_STATIC_LISTEN);



	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCoreUtil_NetUtilDlg::OnItemchangedListServers(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	if(pNMListView->uChanged == LVIF_STATE)
	{
		if(!((pNMListView->uOldState&(LVNI_SELECTED)))
			&&(pNMListView->uNewState&(LVNI_SELECTED)))
		{
			int nItem = m_lc.GetNextItem( -1,  LVNI_SELECTED );
			if(nItem>=0)
				m_nPortSelected = atol(m_lc.GetItemText( nItem, 0));
			else
				m_nPortSelected = 0;
		}
	}
	
	*pResult = 0;
}


void CCoreUtil_NetUtilDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default

	if(nIDEvent == 1)
	{ 
		int nCount = m_lc.GetItemCount();
		for (int i=0; i<nCount; i++)
		{
			int nIndex=0;
			int nConns=0;
			char buffer[NET_ERRORSTRING_LEN];
			nIndex = m_net.GetServerIndex(atoi(m_lc.GetItemText( i, 0)));
			if(nIndex>=0)
			{
				if(m_net.m_ppNetServers[nIndex]->m_ulServerStatus&NET_STATUS_SERVERSTARTED)
				{
					sprintf(buffer, "%05d listening [0x%08x][0x%02x] (%d)",
						m_net.m_ppNetServers[nIndex]->m_usPort,
						m_net.m_ppNetServers[nIndex]->m_ulServerStatus,
						m_net.m_ppNetServers[nIndex]->m_ucType,
						m_net.m_ppNetServers[nIndex]->m_ulConnections);
				}
				else if(m_net.m_ppNetServers[nIndex]->m_ulServerStatus&NET_STATUS_ERROR)
				{
					sprintf(buffer, "%05d error [0x%08x][0x%02x] (%d)",
						m_net.m_ppNetServers[nIndex]->m_usPort,
						m_net.m_ppNetServers[nIndex]->m_ulServerStatus,
						m_net.m_ppNetServers[nIndex]->m_ucType,
						m_net.m_ppNetServers[nIndex]->m_ulConnections);
				}
				else if(m_net.m_ppNetServers[nIndex]->m_ulServerStatus&NET_STATUS_THREADSTARTED)
				{
					sprintf(buffer, "%05d in thread [0x%08x][0x%02x] (%d)",
						m_net.m_ppNetServers[nIndex]->m_usPort,
						m_net.m_ppNetServers[nIndex]->m_ulServerStatus,
						m_net.m_ppNetServers[nIndex]->m_ucType,
						m_net.m_ppNetServers[nIndex]->m_ulConnections);
				}
				else
				{
					sprintf(buffer, "%05d unknown [0x%08x][0x%02x] (%d)",
						m_net.m_ppNetServers[nIndex]->m_usPort,
						m_net.m_ppNetServers[nIndex]->m_ulServerStatus,
						m_net.m_ppNetServers[nIndex]->m_ucType,
						m_net.m_ppNetServers[nIndex]->m_ulConnections);
				}

				m_lc.SetItemText( i, 0, buffer );
			}

		}
	}
	else
	if(nIDEvent == 6)
	{ 
		OnButtonCSend();
	}
	else
	CDialog::OnTimer(nIDEvent);
}

void CCoreUtil_NetUtilDlg::OnButtonTimer() 
{
	if(m_bTimerSet)
	{
		KillTimer(6);
		m_bTimerSet = false;

		GetDlgItem(IDC_BUTTON_TIMER)->SetWindowText("Set Timer");
	}
	else
	{
		UpdateData(TRUE);

		SetTimer(6, m_nTimerMS, NULL);
		GetDlgItem(IDC_BUTTON_TIMER)->SetWindowText("Kill Timer");
		m_bTimerSet = true;
	}

}

void TestServerHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		(*(pClient->m_pulConnections))++;

		CNetUtil net(false); // local object for utility functions.

		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it

		CNetData data;


		bool bCloseCommand = false;

		do
		{
			// get the raw buffer and process it yourself
			unsigned char* pch; 
			unsigned long ulBufLen;

// AfxMessageBox("now");

			nReturn = net.GetLine(&pch, &ulBufLen, pClient->m_socket, NET_RCV_ONCE, pszStatus);

			char buffer9[125]; sprintf(buffer9,"%d",ulBufLen);
// AfxMessageBox(buffer9);

 			data.m_ulDataLen = ulBufLen;
			CBufferUtil bu; 
			
			char* pch2 = bu.ReadableHex((char*)pch, &ulBufLen, 0);
// AfxMessageBox(pch2);

			if(g_pwnd) g_pwnd->SetWindowText(pch2);

			free(pch2);
			// following line fills out the data structure
//			nReturn = net.ReceiveData(pClient->m_socket, &data, pszStatus);
			nReturn = net.InterpretData(pch, &data, true, pszStatus);
// AfxMessageBox((char*)pch);

			if(nReturn<NET_SUCCESS)  // for use with ReceiveData()
			{
	sprintf(buffer9,"%d",nReturn);
// AfxMessageBox(buffer9);
				//error.
				if(pClient->m_lpMsgObj)
				{
					_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error receiving data.  %s", pszStatus);
					((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
				}

				if((data.m_ucCmd == NET_CMD_NAK)&&(nReturn == NET_ERROR_NAK))  // this is unlikely, this is the originating cmd
				{
					// just close the conn if we are commanded to.
					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) bCloseCommand = true;

				} // else not really a NAK, 
				else
				{
					// close the conn if we are commanded to, the client will have to try again..
					unsigned long ulFlags = NET_SND_SVRNAK;  //NET_SND_NAK|NET_SND_NO_RX
					if((data.m_ucType)&NET_TYPE_KEEPOPEN)
					{
//						ulFlags|=NET_SND_KEEPOPENLCL;  // not nec, by default it keeps it open
					}
					else
					{
						bCloseCommand = true;
					}
					// we also need to send the instruction to close down the client side if we arent a persistent server.
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)
					{
						ulFlags|=NET_SND_KEEPOPENRMT;
					}

					// send nak if possible  - have to tell the client that the request was not received in good order.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect that there is no reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending NAK reply.  %s", pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
						}
					}
				}
			}
			else  // successful reception of data.
			{
				// process the data here...
				// in this sample we are using protocol 1...
				if(((data.m_ucType)&NET_TYPE_PROTOCOLMASK) == NET_TYPE_PROTOCOL1)
				{

					//  in this sample, take the datalength, send back the data length in the reply data.

					data.m_ucCmd = NET_CMD_ACK;

					if(!((data.m_ucType)&NET_TYPE_KEEPOPEN)) 
					{
						bCloseCommand = true;  // the incoming request asks to shut down the connection after the exchange.
					}

					char repbuf[64];
					if(data.m_pucData!=NULL)
					{
						_snprintf(repbuf, 63, "len %d of [", data.m_ulDataLen);

						for(unsigned long q=0; q<data.m_ulDataLen; q++)
						{
							if(strlen(repbuf)<63)
								strncat(repbuf, (char*)(data.m_pucData+q), 1);
						}
						if(strlen(repbuf)<63)
							strcat(repbuf, "]");
						data.m_ulDataLen = strlen(repbuf);
						repbuf[data.m_ulDataLen] = 0;  // just in case

						free(data.m_pucData);  //destroy the buffer;
					}
					else
					{
						_snprintf(repbuf, 63, "data was NULL");
						data.m_ulDataLen = strlen(repbuf); 
					}
					data.m_pucData = (unsigned char*) malloc(data.m_ulDataLen);
					if(data.m_pucData) memcpy(data.m_pucData, repbuf, data.m_ulDataLen);
					else 
					{
						data.m_ucCmd = NET_CMD_ACK+1;  // just to differentiate
						data.m_ulDataLen = 0;
					}
					
					data.m_ucType = NET_TYPE_PROTOCOL1|NET_TYPE_HASDATA; // has data but no subcommand.

					// we also need to send the instruction to close down the client side if we arent a persistent server.
					//	if I am a persistent server, but get a connection that wants me to terminate it, NP.
					//	however, if I am not a persistent server and i get a request to keep open, no can do.

					unsigned long ulFlags = NET_SND_SVRREPLY; //NET_SND_KEEPOPENLCL|NET_SND_NO_RXACK
					if((pClient->m_ucType)&NET_TYPE_KEEPOPEN)  // i am persistent
					{
						if(!bCloseCommand)
							ulFlags|=NET_SND_KEEPOPENRMT;  
						// else close it by not adding the param!
					}
					else  // i am not persistent
					{
						ulFlags &= ~NET_SND_KEEPOPENRMT;  // close it!
					}

					// send a reply - actually this is the server's answer to the request.  we expect an ack back to say the client got a good response.
					nReturn = net.SendData(&data, pClient->m_socket, 5000, 3, ulFlags, pszStatus);  // expect an ack. for a reply
					if(nReturn<NET_SUCCESS)
					{
						//error.
						if(pClient->m_lpMsgObj)
						{
							_snprintf(pszStatus, NET_ERRORSTRING_LEN-1, "ServerHandlerThread: error sending reply.  %s", pszStatus);
							((CMessagingObject*)pClient->m_lpMsgObj)->Message(MSG_PRI_MEDIUM|MSG_ICONERROR, pszStatus, "CNetUtil:ServerHandlerThread");
						}
					}
				}
			}

			// here we want to make sure that the pipe has been cleared of any data that isnt part of things we want.


		} while ( 
							(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
						&&((pClient->m_ucType)&NET_TYPE_KEEPOPEN) 
						&&(!bCloseCommand)
						);
		// the while is in case this is many exchanges over a single connection

		shutdown(pClient->m_socket, SD_BOTH);
		closesocket(pClient->m_socket);
		(*(pClient->m_pulConnections))--;

	}

	delete pClient; // was created with new in the thread that spawned this one.
}



void TestDatagramServerHandlerThread(void* pvArgs)
{
	CNetClient* pClient = (CNetClient*) pvArgs;
	if(pClient==NULL) { _endthread(); return; }

	if(pClient->m_socket!=INVALID_SOCKET)
	{
		CNetUtil net(false); // local object for utility functions.

//		int nReturn;
		char pszStatus[NET_ERRORSTRING_LEN];
//		char pszInfo[NET_ERRORSTRING_LEN];
		strcpy(pszStatus, "");  // clear it


		CNetDatagramData** ppData=NULL;
		int nNumDataObj=0;

		struct timeval tv;
		tv.tv_sec = 0; tv.tv_usec = 30;  // timeout value
		fd_set fds;
		int nNumSockets;
		FD_ZERO(&fds);

		FD_SET(pClient->m_socket, &fds);

		nNumSockets = select(0, &fds, NULL, NULL, &tv);

		SOCKADDR_IN si;
		int nSize = sizeof(si);


		while (
						(!((*(pClient->m_pulThreadControl))&NET_CTRL_KILL)) 
					&&(nNumSockets>0)
					)
		{
			char* pchRecv = (char*)malloc(NET_COMMBUFFERSIZE);
			if(pchRecv)
			{
				int nNumBytes 
					=  recvfrom(
											pClient->m_socket,
											pchRecv,
											NET_COMMBUFFERSIZE,
											0,
											(struct sockaddr*)&si,
											&nSize
										);

				if(nNumBytes>0)
				{

			char buffer9[125]; sprintf(buffer9,"%d",nNumBytes);
 AfxMessageBox(buffer9);

 			unsigned long ulBufLen = nNumBytes;
			CBufferUtil bu; 
			
			char* pch2 = bu.ReadableHex((char*)pchRecv, &ulBufLen, 0);
 AfxMessageBox(pch2);

			if(g_pwnd) g_pwnd->SetWindowText(pch2);

			free(pch2);



					BOOL bNew = FALSE;
					// first check the host list
					if(ppData)
					{
						int i=0;
						BOOL bFound = FALSE;
						while(i<nNumDataObj)
						{
							if(
									(ppData[i]->m_si.sin_port == si.sin_port) 
								&&(ppData[i]->m_si.sin_addr.S_un.S_addr == si.sin_addr.S_un.S_addr)
								) // check both port and address.
							{
								// add here
								unsigned char* pucNew = (unsigned char*)malloc(nNumBytes + ppData[i]->m_ulRecv);
								if(pucNew)
								{
									if(ppData[i]->m_pucRecv)
									{
										memcpy(pucNew, ppData[i]->m_pucRecv, ppData[i]->m_ulRecv);			
										free(ppData[i]->m_pucRecv);
									}

									memcpy(pucNew + ppData[i]->m_ulRecv, pchRecv, nNumBytes);

									ppData[i]->m_ulRecv += nNumBytes;
									ppData[i]->m_pucRecv = pucNew;
								}
								bFound = TRUE;
								break;
							}
						
							i++;
						}
						if(!bFound) bNew = TRUE;
					}
					else
					{
						bNew = TRUE;
						// add new.
					}

					if(bNew)
					{
						CNetDatagramData* pData=new CNetDatagramData;

						if(pData)
						{
							CNetDatagramData** ppTempData = new CNetDatagramData*[nNumDataObj+1];
							if(ppTempData)
							{
								if(ppData)
								{
									memcpy(ppTempData, ppData, nNumDataObj*sizeof(CNetDatagramData*));
									delete [] ppData;
								}

								ppTempData[nNumDataObj++] = pData;
								ppData = ppTempData;

							}
							unsigned char* pucNew = (unsigned char*)malloc(nNumBytes);
							if(pucNew)
							{
								memcpy(pucNew, pchRecv, nNumBytes);

								pData->m_ulRecv = nNumBytes;
								pData->m_pucRecv = pucNew;
								memcpy(&pData->m_si, &si, sizeof(si));
								//pData->m_si.sin_addr.S_un.S_addr = si.sin_addr.S_un.S_addr;
							}

						}
					}
				}
			}

			tv.tv_sec = 0; tv.tv_usec = 1000;  // longer timeout value for data continuation
			FD_ZERO(&fds);
			FD_SET(pClient->m_socket, &fds);
			nNumSockets = select(0, &fds, NULL, NULL, &tv);

		}

		// no more data on pipe - respond!

		int q=0;
		while(q<nNumDataObj)
		{
			if(ppData[q])
			{

// for this sample, just make this an echo server.
				if(ppData[q]->m_pucRecv)
				{
					sendto(
						pClient->m_socket,
						(char*)ppData[q]->m_pucRecv, 
						ppData[q]->m_ulRecv,
						0,
						(struct sockaddr *) &(ppData[q]->m_si),
						nSize
					);
				}



				delete ppData[q];
			}
			q++;
		}

		delete [] ppData;

	}

	(*(pClient->m_pulConnections))--;

	delete pClient; // was created with new in the thread that spawned this one.
}






void CCoreUtil_NetUtilDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	FILE* fp = fopen("netlast.cfg", "wb");

	if(fp)
	{
		UpdateData(TRUE);
		CString szText;
		int nCount=((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->GetCount();
		int nSel=((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->GetCurSel();  //CB_ERR
		if(nSel==CB_ERR)
		{
			if(m_szHost.GetLength()>0)
				fprintf(fp, "*%s\n", m_szHost);
		}
		if(nCount>0)
		{
			int i=0;
			while(i<nCount)
			{
				((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->GetLBText( i, szText );
				if(nSel==i)
				{
					fprintf(fp, "*");
				}
				fprintf(fp, "%s\n", szText);
				i++;
			}
		}
		fclose(fp);
	}
	
}


void CCoreUtil_NetUtilDlg::OnCheckNodata() 
{
	// TODO: Add your control notification handler code here
	m_bSendNoData = ((CButton*)GetDlgItem(IDC_CHECK_NODATA))->GetCheck();
	
}
