#if !defined(AFX_COREUTIL_USBDLG_H__01F75157_99A2_4D34_BC33_8E880C5909AE__INCLUDED_)
#define AFX_COREUTIL_USBDLG_H__01F75157_99A2_4D34_BC33_8E880C5909AE__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// CoreUtil_USBDlg.h : header file
//

#include "..\Common\MFC\USB\USBUtil.h"

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_USBDlg dialog

class CCoreUtil_USBDlg : public CDialog
{
// Construction
public:
	CCoreUtil_USBDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCoreUtil_USBDlg)
	enum { IDD = IDD_COREUTIL_USB_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	CUSBUtil m_uu;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCoreUtil_USBDlg)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCoreUtil_USBDlg)
	virtual void OnCancel();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COREUTIL_USBDLG_H__01F75157_99A2_4D34_BC33_8E880C5909AE__INCLUDED_)
