// CoreUtilDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CoreUtil.h"
#include "CoreUtilDlg.h"
#include "CoreUtil_FileUtilDlg.h"
#include "CoreUtil_LogUtilDlg.h"
#include "CoreUtil_DBUtilDlg.h"
#include "CoreUtil_NetUtilDlg.h"
#include "CoreUtil_Http10Dlg.h"
#include "CoreUtil_BufferUtilDlg.h"
#include "CoreUtil_SecurityDlg.h"
#include "CoreUtil_SmtpDlg.h"
#include "CoreUtil_MsgrDlg.h"
#include "CoreUtil_InetDlg.h"
#include "CoreUtil_AudioDlg.h"
#include "CoreUtil_USBDlg.h"
#include "CoreUtil_DirUtilDlg.h"
#include "CoreUtil_SerialDlg.h"
#include "CoreUtil_CortexUtilDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCoreUtilDlg dialog

CCoreUtilDlg::CCoreUtilDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCoreUtilDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCoreUtilDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CCoreUtilDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCoreUtilDlg)
	DDX_Control(pDX, IDC_TAB1, m_Tab1);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CCoreUtilDlg, CDialog)
	//{{AFX_MSG_MAP(CCoreUtilDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB1, OnSelchangeTab1)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCoreUtilDlg message handlers

BOOL CCoreUtilDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	int i=0;

	m_tabItem.mask = TCIF_TEXT;
	m_tabItem.cchTextMax = 15;

	m_tabItem.pszText="CFileUtil";
	m_Tab1.InsertItem( i++, &m_tabItem);

	m_tabItem.pszText="CLogUtil";
	m_Tab1.InsertItem( i++, &m_tabItem);

	m_tabItem.pszText="CDBUtil";
	m_Tab1.InsertItem( i++, &m_tabItem);

	m_tabItem.pszText="CNetUtil";
	m_Tab1.InsertItem( i++, &m_tabItem);

	m_tabItem.pszText="CHTTP10";
	m_Tab1.InsertItem( i++, &m_tabItem);

	m_tabItem.pszText="CBufferUtil";
	m_Tab1.InsertItem( i++, &m_tabItem);

	m_tabItem.pszText="CSecurity";
	m_Tab1.InsertItem( i++, &m_tabItem);

	m_tabItem.pszText="CSMTP";
	m_Tab1.InsertItem( i++, &m_tabItem);

	m_tabItem.pszText="CMessager";
	m_Tab1.InsertItem( i++, &m_tabItem);

	m_tabItem.pszText="CInet";
	m_Tab1.InsertItem( i++, &m_tabItem);

	m_tabItem.pszText="CAudioUtil";
	m_Tab1.InsertItem( i++, &m_tabItem);

	m_tabItem.pszText="CUSBUtil";
	m_Tab1.InsertItem( i++, &m_tabItem);

	m_tabItem.pszText="CDirUtil";
	m_Tab1.InsertItem( i++, &m_tabItem);

	m_tabItem.pszText="CSerial";
	m_Tab1.InsertItem( i++, &m_tabItem);

	m_tabItem.pszText="CortexUtil";
	m_Tab1.InsertItem( i++, &m_tabItem);

	i=0;
	m_pDlg[i++] = new CCoreUtil_FileUtilDlg;
	m_pDlg[i++] = new CCoreUtil_LogUtilDlg;
	m_pDlg[i++] = new CCoreUtil_DBUtilDlg;
	m_pDlg[i++] = new CCoreUtil_NetUtilDlg;
	m_pDlg[i++] = new CCoreUtil_Http10Dlg;
	m_pDlg[i++] = new CCoreUtil_BufferUtilDlg;
	m_pDlg[i++] = new CCoreUtil_SecurityDlg;
	m_pDlg[i++] = new CCoreUtil_SmtpDlg;
	m_pDlg[i++] = new CCoreUtil_MsgrDlg;
	m_pDlg[i++] = new CCoreUtil_InetDlg;
	m_pDlg[i++] = new CCoreUtil_AudioDlg;
	m_pDlg[i++] = new CCoreUtil_USBDlg;
	m_pDlg[i++] = new CCoreUtil_DirUtilDlg;
	m_pDlg[i++] = new CCoreUtil_SerialDlg;
	m_pDlg[i++] = new CCoreUtil_CortexUtilDlg;
//AfxMessageBox("y");

	int x=0;
	for(x=0; x<i; x++)
	{
		if(m_pDlg[x]==NULL) return FALSE;
	}

	i=0;
	if(!((CCoreUtil_FileUtilDlg*)m_pDlg[i++])->Create(GetDlgItem(IDC_TAB1))) return FALSE;
	if(!((CCoreUtil_LogUtilDlg*)m_pDlg[i++])->Create(GetDlgItem(IDC_TAB1))) return FALSE;
	if(!((CCoreUtil_DBUtilDlg*)m_pDlg[i++])->Create(GetDlgItem(IDC_TAB1))) return FALSE;
	if(!((CCoreUtil_NetUtilDlg*)m_pDlg[i++])->Create(GetDlgItem(IDC_TAB1))) return FALSE;
	if(!((CCoreUtil_Http10Dlg*)m_pDlg[i++])->Create(GetDlgItem(IDC_TAB1))) return FALSE;
	if(!((CCoreUtil_BufferUtilDlg*)m_pDlg[i++])->Create(GetDlgItem(IDC_TAB1))) return FALSE;
	if(!((CCoreUtil_SecurityDlg*)m_pDlg[i++])->Create(GetDlgItem(IDC_TAB1))) return FALSE;
	if(!((CCoreUtil_SmtpDlg*)m_pDlg[i++])->Create(GetDlgItem(IDC_TAB1))) return FALSE;
	if(!((CCoreUtil_MsgrDlg*)m_pDlg[i++])->Create(GetDlgItem(IDC_TAB1))) return FALSE;
	if(!((CCoreUtil_InetDlg*)m_pDlg[i++])->Create(GetDlgItem(IDC_TAB1))) return FALSE;
	if(!((CCoreUtil_AudioDlg*)m_pDlg[i++])->Create(GetDlgItem(IDC_TAB1))) return FALSE;
	if(!((CCoreUtil_USBDlg*)m_pDlg[i++])->Create(GetDlgItem(IDC_TAB1))) return FALSE;
	if(!((CCoreUtil_DirUtilDlg*)m_pDlg[i++])->Create(GetDlgItem(IDC_TAB1))) return FALSE;
	if(!((CCoreUtil_SerialDlg*)m_pDlg[i++])->Create(GetDlgItem(IDC_TAB1))) return FALSE;
	if(!((CCoreUtil_CortexUtilDlg*)m_pDlg[i++])->Create(GetDlgItem(IDC_TAB1))) return FALSE;

	for(x=0; x<i; x++)
	{
		m_pDlg[x]->ShowWindow(SW_SHOW);
	}

	CRect rcDlg;
	for(int d=0; d<NUM_TEST_TABS; d++)
	{
		if(m_pDlg[d]) 
		{
			m_Tab1.GetWindowRect(&rcDlg);
			m_Tab1.AdjustRect( FALSE, &rcDlg);
			m_Tab1.ScreenToClient(&rcDlg);

			m_pDlg[d]->SetWindowPos( 
				&wndTop,
				rcDlg.left,
				rcDlg.top,
				rcDlg.Width(),  // goes with right edge
				rcDlg.Height(),  // goes with bottom edge
				SWP_NOZORDER
				);
		}
	}

/*
	m_Tab1.SetCurSel(i-1);

	for(x=0; x<i-1; x++)
	{
		m_pDlg[x]->ShowWindow(SW_HIDE);
	}
*/

	int ncursel=3;

	FILE* fp = fopen("coreutil.cfg", "rb");
	if(fp)
	{
		fscanf(fp,"%d",&ncursel);
		fclose(fp);
	}

	m_Tab1.SetCurSel(ncursel);
	for(x=0; x<i; x++)
	{
		m_pDlg[x]->ShowWindow(SW_HIDE);
	}
	m_pDlg[ncursel]->ShowWindow(SW_SHOW);


	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CCoreUtilDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCoreUtilDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCoreUtilDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CCoreUtilDlg::OnSelchangeTab1(NMHDR* pNMHDR, LRESULT* pResult) 
{

	int n=m_Tab1.GetCurSel();
	m_pDlg[n]->ShowWindow(SW_SHOW); n++; 
	
	for(int i=1; i<NUM_TEST_TABS; i++)
	{
		if (n>(NUM_TEST_TABS-1)) n=0;
		m_pDlg[n]->ShowWindow(SW_HIDE); n++;
	}
	
	
	*pResult = 0;
}

void CCoreUtilDlg::OnOK() 
{
//	CDialog::OnOK();
}

void CCoreUtilDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	FILE* fp = fopen("coreutil.cfg", "wb");
	if(fp)
	{
		int n=m_Tab1.GetCurSel();
		fprintf(fp,"%d",n);
		fclose(fp);
	}
}
