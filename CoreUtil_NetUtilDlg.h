#if !defined(AFX_COREUTIL_NETUTILDLG_H__AE250044_51C9_40D6_AF5A_E187AC5AC128__INCLUDED_)
#define AFX_COREUTIL_NETUTILDLG_H__AE250044_51C9_40D6_AF5A_E187AC5AC128__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// CoreUtil_NetUtilDlg.h : header file
//

#include "..\Common\LAN\NetUtil.h"

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_NetUtilDlg dialog

class CCoreUtil_NetUtilDlg : public CDialog
{
// Construction
public:
	CCoreUtil_NetUtilDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCoreUtil_NetUtilDlg)
	enum { IDD = IDD_COREUTIL_NETUTIL_DIALOG };
	CListCtrl	m_lc;
	CString	m_szData;
	int		m_nPort;
	int		m_nCmd;
	int		m_nSubCmd;
	int		m_nSendPort;
	BOOL	m_bPersist;
	int		m_nTimerMS;
	BOOL	m_bSimple;
	CString	m_szHost;
	BOOL	m_bFromFile;
	BOOL	m_bForceScmd;
	BOOL	m_bUDP;
	BOOL	m_bSendNoData;
	//}}AFX_DATA

	CNetUtil m_net;
	int m_nPortSelected;
	char m_chConns[2056];

	bool m_bTimerSet;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCoreUtil_NetUtilDlg)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCoreUtil_NetUtilDlg)
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnButtonSListen();
	afx_msg void OnButtonSRemove();
	afx_msg void OnButtonCSend();
	virtual BOOL OnInitDialog();
	afx_msg void OnItemchangedListServers(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnButtonTimer();
	afx_msg void OnDestroy();
	afx_msg void OnCheckNodata();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COREUTIL_NETUTILDLG_H__AE250044_51C9_40D6_AF5A_E187AC5AC128__INCLUDED_)
