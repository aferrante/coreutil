// CoreUtil_SecurityDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CoreUtil.h"
#include "CoreUtil_SecurityDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_SecurityDlg dialog


CCoreUtil_SecurityDlg::CCoreUtil_SecurityDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCoreUtil_SecurityDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCoreUtil_SecurityDlg)
	m_szFilename = _T("security.pwf");
	m_szLocusName = _T("Locus1");
	m_szMagicPW = _T("wizard");
	m_szMagicUser = _T("magicke");
	m_szBuffer = _T("");
	m_szUser = _T("user1");
	m_szPw = _T("password");
	m_szAsset = _T("asset1");
	m_szGroup = _T("group1");
	//}}AFX_DATA_INIT
}


void CCoreUtil_SecurityDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCoreUtil_SecurityDlg)
	DDX_Text(pDX, IDC_EDIT_FILENAME, m_szFilename);
	DDX_Text(pDX, IDC_EDIT_LOCUSNAME, m_szLocusName);
	DDX_Text(pDX, IDC_EDIT_MAGICPW, m_szMagicPW);
	DDX_Text(pDX, IDC_EDIT_MAGICUSER, m_szMagicUser);
	DDX_Text(pDX, IDC_EDIT_OUTPUT, m_szBuffer);
	DDX_Text(pDX, IDC_EDIT_USERNAME, m_szUser);
	DDX_Text(pDX, IDC_EDIT_USERPW, m_szPw);
	DDX_Text(pDX, IDC_EDIT_ASSETNAME, m_szAsset);
	DDX_Text(pDX, IDC_EDIT_GROUP, m_szGroup);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCoreUtil_SecurityDlg, CDialog)
	//{{AFX_MSG_MAP(CCoreUtil_SecurityDlg)
	ON_BN_CLICKED(IDC_BUTTON_ADDLOCUS, OnButtonAddlocus)
	ON_BN_CLICKED(IDC_BUTTON_ADDUSER, OnButtonAdduser)
	ON_BN_CLICKED(IDC_BUTTON_INIT, OnButtonInit)
	ON_BN_CLICKED(IDC_BUTTON_REMLOCUS, OnButtonRemlocus)
	ON_BN_CLICKED(IDC_BUTTON_REMUSER, OnButtonRemuser)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, OnButtonSave)
	ON_BN_CLICKED(IDC_BUTTON_CHECK, OnButtonCheck)
	ON_BN_CLICKED(IDC_BUTTON_ADDASSET, OnButtonAddasset)
	ON_BN_CLICKED(IDC_BUTTON_ADDLOCUSTOUSER, OnButtonAddlocustouser)
	ON_BN_CLICKED(IDC_BUTTON_ADDGRP, OnButtonAddgrp)
	ON_BN_CLICKED(IDC_BUTTON_REMASSET, OnButtonRemasset)
	ON_BN_CLICKED(IDC_BUTTON_REMGRP, OnButtonRemgrp)
	ON_BN_CLICKED(IDC_BUTTON_REMLOCUSFROMUSER, OnButtonRemlocusfromuser)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_SecurityDlg message handlers

void CCoreUtil_SecurityDlg::OnCancel() 
{
// CDialog::OnCancel();
}

void CCoreUtil_SecurityDlg::OnOK() 
{
// CDialog::OnOK();
}

BOOL CCoreUtil_SecurityDlg::Create(CWnd* pParentWnd) 
{
	return CDialog::Create(CCoreUtil_SecurityDlg::IDD, pParentWnd);
}

void CCoreUtil_SecurityDlg::OnButtonAddlocus() 
{
	UpdateData(TRUE);
	char locus[1024];
	sprintf(locus, "%s",m_szLocusName);

	CSecureLocus* pLocus = new CSecureLocus(locus, SECURE_LOCUS_VIRT);

	if(m_secure.AddLocus(pLocus, SECURE_LOCUS_VIRT)<SECURE_SUCCESS) AfxMessageBox("could not add locus");
	else OnButtonSave();
	
}

void CCoreUtil_SecurityDlg::OnButtonAdduser() 
{
	UpdateData(TRUE);
	char user[1024];
	sprintf(user, "%s",m_szUser);
	char pw[1024];
	sprintf(pw, "%s",m_szPw);

	CSecureUser* pUser = new CSecureUser(user, pw);

	if(m_secure.AddUser(pUser, 0)<SECURE_SUCCESS) AfxMessageBox("could not add user");
	else

		OnButtonSave();
	
	
}

void CCoreUtil_SecurityDlg::OnButtonInit() 
{
	UpdateData(TRUE);
	char filename[1024];
	sprintf(filename, "%s",m_szFilename);

	m_secure.InitSecurity(filename, NULL, NULL, NULL, 0); // can ov

	m_szBuffer.Format("%s",m_secure.m_file.m_pchBuffer);

	UpdateData(FALSE);
	
}

void CCoreUtil_SecurityDlg::OnButtonRemlocus() 
{
	UpdateData(TRUE);
	char locus[1024];
	sprintf(locus, "%s",m_szLocusName);

	if(m_secure.DeleteLocus(locus, 0)<SECURE_SUCCESS) AfxMessageBox("could not remove locus");
	else
//AfxMessageBox("x");

		OnButtonSave();
	
}

void CCoreUtil_SecurityDlg::OnButtonRemuser() 
{
	UpdateData(TRUE);
	char user[1024];
	sprintf(user, "%s",m_szUser);

	if(m_secure.DeleteUser(user, 0)<SECURE_SUCCESS) AfxMessageBox("could not remove user");
	else

		OnButtonSave();
	
}

void CCoreUtil_SecurityDlg::OnButtonSave() 
{
	UpdateData(TRUE);
	char filename[1024];
	sprintf(filename, "%s",m_szFilename);

	m_secure.SaveSecurity(0,filename ); // can ov

	m_szBuffer.Format("%s",m_secure.m_file.m_pchBuffer);

	UpdateData(FALSE);
	
}

void CCoreUtil_SecurityDlg::OnButtonCheck() 
{
	UpdateData(TRUE);
	char locus[1024];
	sprintf(locus, "%s",m_szLocusName);
	char user[1024];
	sprintf(user, "%s",m_szUser);
	char pw[1024];
	sprintf(pw, "%s",m_szPw);
	char asset[1024];
	sprintf(asset, "%s",m_szAsset);

	if(strlen(locus))
	{
		if(strlen(asset))
		{
			if(m_secure.CheckSecure(user, pw, locus, asset ) <SECURE_SUCCESS) AfxMessageBox("failed security check");
			else AfxMessageBox("passed security check");
		}
		else
		{
			if(m_secure.CheckSecure(user, pw, locus, NULL ) <SECURE_SUCCESS) AfxMessageBox("failed security check");
			else AfxMessageBox("passed security check");
		}
	}
	else
	{
		if(m_secure.CheckSecure(user, pw, NULL, NULL ) <SECURE_SUCCESS) AfxMessageBox("failed security check");
		else AfxMessageBox("passed security check");
	}
	
}

void CCoreUtil_SecurityDlg::OnButtonAddasset() 
{
	UpdateData(TRUE);
	char locus[1024];
	sprintf(locus, "%s",m_szLocusName);
	char asset[1024];
	sprintf(asset, "%s",m_szAsset);

	int nindex = m_secure.GetLocusIndex(locus);
	if(nindex>=0)
	{
		CSecureAsset* pAsset = new CSecureAsset(asset, SECURE_LOCUS_VIRT);

		if(m_secure.m_ppLoci[nindex]->AddAsset(pAsset)<SECURE_SUCCESS) AfxMessageBox("could not add asset");
		else OnButtonSave();
	}else AfxMessageBox("could not add asset");
}

void CCoreUtil_SecurityDlg::OnButtonAddlocustouser() 
{
	UpdateData(TRUE);
	char locus[1024];
	sprintf(locus, "%s",m_szLocusName);
	char user[1024];
	sprintf(user, "%s",m_szUser);

	int nindex = m_secure.GetLocusIndex(locus);
	int nuindex = m_secure.GetUserIndex(user);
	if((nindex>=0)&&(nuindex>=0))
	{
		if(m_secure.m_ppUsers[nuindex]->AddLocus(m_secure.m_ppLoci[nindex])<SECURE_SUCCESS) AfxMessageBox("could not add locus");
		else OnButtonSave();
	}else AfxMessageBox("could not add locus");
	
}

void CCoreUtil_SecurityDlg::OnButtonAddgrp() 
{
	UpdateData(TRUE);
	char group[1024];
	sprintf(group, "%s",m_szGroup);
	char user[1024];
	sprintf(user, "%s",m_szUser);

	int nuindex = m_secure.GetUserIndex(user);
	if((nuindex>=0))
	{
		if(m_secure.m_ppUsers[nuindex]->AddGroup(group)<SECURE_SUCCESS) AfxMessageBox("could not add group");
		else OnButtonSave();
	}else AfxMessageBox("could not add group");
	
}

void CCoreUtil_SecurityDlg::OnButtonRemasset() 
{
	UpdateData(TRUE);
	char locus[1024];
	sprintf(locus, "%s",m_szLocusName);
	char asset[1024];
	sprintf(asset, "%s",m_szAsset);

	int nindex = m_secure.GetLocusIndex(locus);
	if(nindex>=0)
	{
		if(m_secure.m_ppLoci[nindex]->RemoveAsset(asset)<SECURE_SUCCESS) AfxMessageBox("could not remove asset");
		else OnButtonSave();
	}else AfxMessageBox("could not remove asset");
	
}

void CCoreUtil_SecurityDlg::OnButtonRemgrp() 
{
	UpdateData(TRUE);
	char group[1024];
	sprintf(group, "%s",m_szGroup);
	char user[1024];
	sprintf(user, "%s",m_szUser);

	int nuindex = m_secure.GetUserIndex(user);
	if((nuindex>=0))
	{
		if(m_secure.m_ppUsers[nuindex]->RemoveGroup(group)<SECURE_SUCCESS) AfxMessageBox("could not remove group");
		else OnButtonSave();
	}else AfxMessageBox("could not remove group");
	
	
}

void CCoreUtil_SecurityDlg::OnButtonRemlocusfromuser() 
{
	UpdateData(TRUE);
	char locus[1024];
	sprintf(locus, "%s",m_szLocusName);
	char user[1024];
	sprintf(user, "%s",m_szUser);

	int nuindex = m_secure.GetUserIndex(user);
	if((nuindex>=0))
	{
		if(m_secure.m_ppUsers[nuindex]->RemoveLocus(locus)<SECURE_SUCCESS) AfxMessageBox("could not remove locus");
		else OnButtonSave();
	}else AfxMessageBox("could not remove locus");
	
}
