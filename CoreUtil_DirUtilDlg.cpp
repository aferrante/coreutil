// CoreUtil_DirUtilDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CoreUtil.h"
#include "CoreUtil_DirUtilDlg.h"
#include "SelectDirDlg.h"
#include <sys/timeb.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define MAX_MESSAGE_LENGTH 512

bool bBusy = false;
int nChanges=0;
/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_DirUtilDlg dialog


CCoreUtil_DirUtilDlg::CCoreUtil_DirUtilDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCoreUtil_DirUtilDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCoreUtil_DirUtilDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_pszFile = NULL;
}


void CCoreUtil_DirUtilDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCoreUtil_DirUtilDlg)
	DDX_Control(pDX, IDC_LIST1, m_list);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCoreUtil_DirUtilDlg, CDialog)
	//{{AFX_MSG_MAP(CCoreUtil_DirUtilDlg)
	ON_BN_CLICKED(IDC_BUTTON_SETDIR, OnButtonSetdir)
	ON_BN_CLICKED(IDC_BUTTON_WATCH, OnButtonWatch)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_DirUtilDlg message handlers

void CCoreUtil_DirUtilDlg::OnCancel() 
{
//	CDialog::OnCancel();
}

void CCoreUtil_DirUtilDlg::OnOK() 
{
//	CDialog::OnOK();
}

void CCoreUtil_DirUtilDlg::OnButtonSetdir() 
{
//	CFileDialog dlg(TRUE, NULL, NULL, OFN_NOCHANGEDIR|OFN_PATHMUSTEXIST);

	CSelectDirDlg dlg;
//	dlg.m_bDirAsRoot = true;  // dont go back to the drive level
//	dlg.m_bAllDrives = true;  // list all drives, or list only the single drive if false.  no effect if m_bDirAsRoot is true
//	dlg.SetInitDir("C:");

	
	if(dlg.DoModal() == IDOK)
	{
		GetDlgItem(IDC_EDIT1)->SetWindowText(dlg.m_pszCurrentDir);
	}
}

BOOL CCoreUtil_DirUtilDlg::Create(CWnd* pParentWnd) 
{
	return CDialog::Create(CCoreUtil_DirUtilDlg::IDD, pParentWnd);
}

void CCoreUtil_DirUtilDlg::OnButtonWatch() 
{
//			AfxMessageBox("on button watch");

	if(m_du.m_bWatchFolder)
	{
		m_du.EndWatch();
		Message("Ending watch");
		KillTimer(1);
	}
	else
	{
		CString szPath;
		GetDlgItem(IDC_EDIT1)->GetWindowText(szPath);
		
		char pszFullPath[MAX_PATH+1];

		sprintf(pszFullPath, "%s",szPath);

		if(szPath.GetLength())
		{
			if(m_du.SetWatchFolder(pszFullPath)>=0)
			{
				if(m_du.BeginWatch(60000, true)< 0)
				{
					AfxMessageBox("could not begin watch");
				}
				else
				{
					_unlink("_removedlog.log"); nChanges=0;

					while(!m_du.m_bWatchFolder) Sleep(50);
					Message("Watch folder set to: %s", m_du.m_pszWatchPath);

//			AfxMessageBox("started");
					SetTimer(1,100,NULL);
				}
			}
			else AfxMessageBox("could not set watch folder");
		}
		else
		{	
			AfxMessageBox("Not a valid path");
		}
	}

	if(m_du.m_bWatchFolder)
	{
		GetDlgItem(IDC_BUTTON_WATCH)->SetWindowText("End Watch");
//			AfxMessageBox("end");
	}
	else
	{
		GetDlgItem(IDC_BUTTON_WATCH)->SetWindowText("Start Watch");
//			AfxMessageBox("start");
	}


}
void CCoreUtil_DirUtilDlg::Message(char* pszFormat, ...)
{

	if((pszFormat)&&(strlen(pszFormat)))
	{
		_timeb timestamp;
		_ftime( &timestamp );

		char buffer[MAX_MESSAGE_LENGTH];
		tm* theTime = localtime( &timestamp.time	);
		strftime(buffer, 30, "%H:%M:%S.", theTime );
		int nOffset = strlen(buffer);

		sprintf(buffer+nOffset,"%03d   ",timestamp.millitm);
		nOffset = strlen(buffer);
		va_list marker;
		// create the formatted output string
		va_start(marker, pszFormat); // Initialize variable arguments.
		_vsnprintf((char *) (buffer+nOffset), MAX_MESSAGE_LENGTH-1-nOffset, pszFormat, (va_list) marker);
		va_end( marker );             // Reset variable arguments.

		int n = m_list.GetItemCount();

		if(n > 1000)  // max num of records in list box
		{
			m_list.DeleteItem(0);
			n--;
		}
		if(m_list.InsertItem(n, buffer) >= 0)
		{
			n = m_list.GetStringWidth(buffer);
			if(m_list.GetColumnWidth(0)<n) m_list.SetColumnWidth(0, n+10);

			m_list.EnsureVisible(m_list.GetItemCount()-1, FALSE);
		}
//		else 	AfxMessageBox(buffer);

		if((m_pszFile)&&(strlen(m_pszFile)))
		{
			FILE* fp;
			fp = fopen(m_pszFile, "at");
			if(fp)
			{
				fprintf(fp, "%s\r\n", buffer);
				fflush(fp);
				fclose(fp);
			}
		}
	}

	MSG msg;
	while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
		AfxGetApp()->PumpMessage();

}


BOOL CCoreUtil_DirUtilDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	
	CRect rc;
	m_list.GetWindowRect(rc);
	ScreenToClient(rc);

	m_list.InsertColumn(0, "", LVCFMT_LEFT, rc.Width()-5, 0);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCoreUtil_DirUtilDlg::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent==1)
	{
		if((bBusy)||(!m_du.m_bInitialized)) return;
		bBusy = true;
		// Check for stuff:
		int n = m_du.QueryChanges();
		int q = 0;
		if(n>0)
		{
			CDirEntry* pDirChange;
			Message("%d initial changes", n);

			while(m_du.GetDirChange(&pDirChange)>0)
			{

				q++;
				CString foo;
				if(pDirChange)
				{
					switch(pDirChange->m_ulFlags&(~WATCH_CHK)) // ignore the check flag
					{
					case WATCH_INIT://		0x00000000  // was there at beginning	
						foo = _T("init"); break;
					case WATCH_ADD: //		0x00000001  // was added	
						foo = _T(" add"); break;
					case WATCH_DEL: //		0x00000002  // was del	
						foo = _T(" del"); break;
					case WATCH_CHG: //		0x00000003  // was chg	
						foo = _T("chng"); break;
					default: 
						foo = _T("unkn"); break;  // unknown
					}
				}



	// debug file
			//	_unlink("_removedlog.log");
/*
				FILE* fp;
				fp = fopen("_removedlog.log", "at");
				if(fp)
				{
					if(pDirChange)
					{
						fprintf(fp, "%s %s\\%s %d %d%d %d%db 0x%08x\r\n", 	
							foo,
							pDirChange->m_szFullPath?pDirChange->m_szFullPath:"(null)",
							pDirChange->m_szFilename?pDirChange->m_szFilename:"(null)",
							pDirChange->m_ulEntryTimestamp,			// unixtime, seconds resolution, last modified
							pDirChange->m_ftFileModified.dwHighDateTime,			// unixtime, seconds resolution, last modified
							pDirChange->m_ftFileModified.dwLowDateTime,			// unixtime, seconds resolution, last modified
							pDirChange->m_ulFileSizeHigh,				// filesize		DWORD    nFileSizeHigh; 
							pDirChange->m_ulFileSizeLow,				// filesize		DWORD    nFileSizeLow;
							pDirChange->m_ulFileAttribs    // attribs returned by GetFileAttributes
							);
					}
					else	fprintf(fp, "NULL change\r\n");	


					fflush(fp);
					fclose(fp);
				}


/*
				Message("%s %s\\%s %d %d%d %d%db 0x%08x", 	
					foo,
					pDirChange->m_szFullPath?pDirChange->m_szFullPath:"(null)",
					pDirChange->m_szFilename?pDirChange->m_szFilename:"(null)",
					pDirChange->m_ulEntryTimestamp,			// unixtime, seconds resolution, last modified
					pDirChange->m_ftFileModified.dwHighDateTime,			// unixtime, seconds resolution, last modified
					pDirChange->m_ftFileModified.dwLowDateTime,			// unixtime, seconds resolution, last modified
					pDirChange->m_ulFileSizeHigh,				// filesize		DWORD    nFileSizeHigh; 
					pDirChange->m_ulFileSizeLow,				// filesize		DWORD    nFileSizeLow;
					pDirChange->m_ulFileAttribs    // attribs returned by GetFileAttributes
					);
*/
				if(pDirChange) delete(pDirChange); 
				pDirChange=NULL;
				//Sleep(1);
			}  //while

			nChanges += q;
			Message("processed %d changes of %d",q, nChanges);

		}
		bBusy = false;
	}
	else
	CDialog::OnTimer(nIDEvent);
}
