// CoreUtil_CortexUtilDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CoreUtil.h"
#include "CoreUtil_CortexUtilDlg.h"
#include "..\Common\TXT\BufferUtil.h"
#include <process.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

void SocketMonitorThread(void* pvArgs);


/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_CortexUtilDlg dialog


CCoreUtil_CortexUtilDlg::CCoreUtil_CortexUtilDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCoreUtil_CortexUtilDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCoreUtil_CortexUtilDlg)
	m_nSendPort = 10671;
	m_szHost = _T("10.150.0.32");
	m_szCmd = _T("get_event");
	m_szOptions = _T("<id>8</id>");
	m_bLogExact = FALSE;
	m_bLogReadable = FALSE;
	//}}AFX_DATA_INIT
	m_s=NULL;
	m_szAssembled = "";

	m_bMonitorSocket=FALSE;
	m_bSocketMonitorStarted=FALSE;
	m_bInCommand=FALSE;

}

BOOL CCoreUtil_CortexUtilDlg::Create(CWnd* pParentWnd) 
{
	return CDialog::Create(CCoreUtil_CortexUtilDlg::IDD, pParentWnd);
}


void CCoreUtil_CortexUtilDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCoreUtil_CortexUtilDlg)
	DDX_Text(pDX, IDC_EDIT_C_PORT, m_nSendPort);
	DDX_CBString(pDX, IDC_COMBO_HOST, m_szHost);
	DDX_Text(pDX, IDC_EDIT_CMD, m_szCmd);
	DDX_Text(pDX, IDC_EDIT_OPTIONS, m_szOptions);
	DDX_Check(pDX, IDC_CHECK_LOGEXACT, m_bLogExact);
	DDX_Check(pDX, IDC_CHECK_LOGREADABLE, m_bLogReadable);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCoreUtil_CortexUtilDlg, CDialog)
	//{{AFX_MSG_MAP(CCoreUtil_CortexUtilDlg)
	ON_BN_CLICKED(IDC_BUTTON_C_SEND, OnButtonCSend)
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, OnButtonConnect)
	ON_BN_CLICKED(IDC_BUTTON_ASSEMBLE, OnButtonAssemble)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_RECV, OnButtonRecv)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_CortexUtilDlg message handlers

BOOL CCoreUtil_CortexUtilDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	FILE* fp = fopen("netlast.cfg", "rb");

	if(fp)
	{
		fseek(fp, 0, SEEK_END);
		unsigned long ulFileLen = ftell(fp);
		char* pchFile = (char*) malloc(ulFileLen+1); // term zero
		if(pchFile)
		{
			fseek(fp, 0, SEEK_SET);

			fread(pchFile, sizeof(char), ulFileLen, fp);
			*(pchFile+ulFileLen) = 0; // term zero

			int i=0; int nSel=-1;
			char* pch = strtok(pchFile, "\r\n");
			while(pch != NULL)
			{
				if(*pch=='*')
				{
					nSel = i;
					pch++;
				}
				((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->InsertString(i, pch);

				pch = strtok(NULL, "\r\n");
				i++;
			}
			if ((i>0)&&(nSel>=0)&&(nSel<i))
			{
				((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->SetCurSel(nSel);
				UpdateData(TRUE);
			}
			free(pchFile);

		}
		UpdateData(FALSE);
		fclose(fp);
	}

/*
  CRect rcList;
  m_lc.GetWindowRect(&rcList);

	
  if(m_lc.InsertColumn(
    0, 
    "Listeners", 
    LVCFMT_LEFT, 
    rcList.Width()-20,
    -1
    ) <0) AfxMessageBox("could not add Listeners column");

	SetTimer(1, 1000, NULL); 
		g_pwnd = GetDlgItem(IDC_STATIC_LISTEN);
*/

	
	GetDlgItem(IDC_BUTTON_C_SEND)->EnableWindow(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCoreUtil_CortexUtilDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
//	CDialog::OnCancel();
}

void CCoreUtil_CortexUtilDlg::OnOK() 
{
	// TODO: Add extra validation here
	
//	CDialog::OnOK();
}

void CCoreUtil_CortexUtilDlg::OnButtonCSend() 
{
	CWaitCursor cw;
	UpdateData(TRUE);
	if(m_s==NULL) OnButtonConnect();
	if(m_s==NULL) return;


	if(((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->FindStringExact( -1, m_szHost )==CB_ERR)
	{
		((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->AddString(m_szHost);
		((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->SelectString(-1, m_szHost);
	}
/*

	unsigned char buffer[8192];
	unsigned long ulLen=0;
	
	// assemble buffer

	UUID uuid;
	UuidCreate(&uuid); // could check response code to be RPC_S_OK ?

//			CoCreateGuid(&uuid);

	unsigned char* pucUUID = NULL;
	UuidToString(&uuid, &pucUUID);


	_snprintf((char*)buffer, 8191, "<cortex xmlns=\"Cortex\"><rx rxid=\"%s\"/><cmd type=\"%s\"><options>%s</options></cmd></cortex>", 
		pucUUID?(char*)pucUUID:"27B",
		m_szCmd,
		m_szOptions
		);

	if(pucUUID)
	{
		try { RpcStringFree(&pucUUID); } catch(...){}
	}
	pucUUID = NULL;

*/

	OnButtonAssemble();

	unsigned char* buffer[8192];
	unsigned long ulLen=m_szAssembled.GetLength();
	unsigned char* pbuf = (unsigned char*)m_szAssembled.GetBuffer(ulLen);


	FILE* fp=NULL;
	if((m_bLogExact)||(m_bLogReadable))
	{
		fp = fopen("XML.txt", "ab");
		if(fp)
		{
			fprintf(fp,"sending:\r\n");
			if(m_bLogReadable)
			{
				fprintf(fp,"%s\r\n\r\n", FormatXML((char*)pbuf));
			}
			if(m_bLogExact)
			{
				fprintf(fp,"sending [%s]\r\n\r\n", pbuf);
			}
			fflush(fp);
		}
	}

	unsigned long ulTime = clock();
	m_bInCommand = TRUE;
	int r = m_net.SendLine(pbuf, ulLen, m_s, EOLN_NONE, true, 5000);
	m_bInCommand = FALSE;
	if(r<NET_SUCCESS)
	{
//		if(m_bTimerSet) OnButtonTimer(); // dont keep on in error condition.
		AfxMessageBox("Send error");
		OnButtonConnect();

		if(fp)
		{
			fprintf(fp,"send error, code %d:\r\n\r\n", r);
			fflush(fp);
		}

	}
	else
	{
		unsigned char* pucbuffer = NULL;
		unsigned long ulLenRec= 0;
		unsigned long ulTime2 = clock();

		if(fp)
		{
			fprintf(fp,"receiving:\r\n");
			fflush(fp);
		}

		// receive something
//		Sleep(100); // after a short pause
	m_bInCommand = TRUE;
		r = m_net.GetLine(&pucbuffer, &ulLenRec, m_s, NET_RCV_EOLN|EOLN_TOKEN, "</cortex>");
	m_bInCommand = FALSE;
//		r = m_net.GetLine(&pucbuffer, &ulLenRec, m_s, EOLN_TOKEN, "</cortex>");

		int sendtime = ulTime2 - ulTime;
		int recvtime = clock() - ulTime2;
		int totaltime = clock() - ulTime;

		CString szX = FormatXML((char*)pucbuffer);
		if(fp)
		{
			if(m_bLogReadable)
			{
				fprintf(fp,"received code %d, %d bytes, send time %d ms, recv time %d ms, total %d ms (%d bps)\r\n%s\r\n\r\n", r, ulLenRec, 
					sendtime,
					recvtime,
					totaltime, int((double)ulLenRec/((double)totaltime/1000.0)),
					szX);
			}
			if(m_bLogExact)
			{
				fprintf(fp,"received code %d, %d bytes, send time %d ms, recv time %d ms, total %d ms (%d bps)\r\n[%s]\r\n\r\n", r, ulLenRec, 
					sendtime,
					recvtime,
					totaltime, int((double)ulLenRec/((double)totaltime/1000.0)),
					(char*)pucbuffer);
			}
			fflush(fp);
		}

		_snprintf((char*)buffer, 8191, "received %d:\r\ndatalen:%d send time %d ms, recv time %d ms, total %d ms (%d bps)\r\n\r\ndata:\r\n%s",
			clock(),
				ulLenRec,
				sendtime,
				recvtime,
				totaltime, int((double)ulLenRec/((double)totaltime/1000.0)),
				szX
			);

		if(pucbuffer) free(pucbuffer);

		GetDlgItem(IDC_EDIT_C_REPLY)->SetWindowText((char*)buffer);

	}
	if(fp) fclose(fp);
}

void CCoreUtil_CortexUtilDlg::OnButtonConnect() 
{
	// TODO: Add your control notification handler code here
	int nreturn = NET_SUCCESS;

	UpdateData(TRUE);
	if(m_s==NULL)  // first time.
	{
		GetDlgItem(IDC_BUTTON_CONNECT)->EnableWindow(FALSE);
		char host[256];
		strcpy(host, m_szHost);
		nreturn = m_net.OpenConnection(host, m_nSendPort, &m_s, 5000, 5000);
		if(nreturn>=NET_SUCCESS)
		{
			GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Disconnect");
			GetDlgItem(IDC_COMBO_HOST)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_C_PORT)->EnableWindow(FALSE);
			GetDlgItem(IDC_BUTTON_C_SEND)->EnableWindow(TRUE);
/*
			// send the persist command.
			unsigned char* buffer[8192];
			CString m_szAssembled;
			m_szAssembled.Format("<cortex xmlns=\"Cortex\"><rx rxid=\"1\"/><cmd type=\"persist\"/></cortex>");
			unsigned long ulLen=m_szAssembled.GetLength();
			unsigned char* pbuf = (unsigned char*)m_szAssembled.GetBuffer(ulLen);


			FILE* fp=NULL;
			if((m_bLogExact)||(m_bLogReadable))
			{
				fp = fopen("XML.txt", "ab");
				if(fp)
				{
					fprintf(fp,"sending:\r\n");
					if(m_bLogReadable)
					{
						fprintf(fp,"%s\r\n\r\n", FormatXML((char*)pbuf));
					}
					if(m_bLogExact)
					{
						fprintf(fp,"sending [%s]\r\n\r\n", pbuf);
					}
					fflush(fp);
				}
			}

			unsigned long ulTime = clock();
			int r = m_net.SendLine(pbuf, ulLen, m_s, EOLN_NONE, true, 5000);

			if(r<NET_SUCCESS)
			{
		//		if(m_bTimerSet) OnButtonTimer(); // dont keep on in error condition.
				AfxMessageBox("Send error");


				if(fp)
				{
					fprintf(fp,"send error, code %d:\r\n\r\n", r);
					fflush(fp);
				}

				m_net.CloseConnection(m_s);
				GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
				GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
				GetDlgItem(IDC_EDIT_C_PORT)->EnableWindow(TRUE);
				GetDlgItem(IDC_BUTTON_C_SEND)->EnableWindow(FALSE);
				m_s = NULL;


			}
			else
			{
				unsigned char* pucbuffer = NULL;
				unsigned long ulLenRec= 0;
				unsigned long ulTime2 = clock();

				if(fp)
				{
					fprintf(fp,"receiving:\r\n");
					fflush(fp);
				}

				// receive something
				r = m_net.GetLine(&pucbuffer, &ulLenRec, m_s, EOLN_TOKEN, "</cortex>");

				int sendtime = ulTime2 - ulTime;
				int recvtime = clock() - ulTime2;
				int totaltime = clock() - ulTime;

				CString szX = FormatXML((char*)pucbuffer);
				if(fp)
				{
					if(m_bLogReadable)
					{
						fprintf(fp,"recevied code %d, %d bytes, send time %d ms, recv time %d ms, total %d ms\r\n%s\r\n\r\n", r, ulLenRec, 
							sendtime,
							recvtime,
							totaltime,
							szX);
					}
					if(m_bLogExact)
					{
						fprintf(fp,"received code %d, %d bytes, send time %d ms, recv time %d ms, total %d ms\r\n[%s]\r\n\r\n", r, ulLenRec, 
							sendtime,
							recvtime,
							totaltime,
							(char*)pucbuffer);
					}
					fflush(fp);
				}

				_snprintf((char*)buffer, 8191, "received %d:\r\ndatalen:%d send time %d ms, recv time %d ms, total %d ms\r\n\r\ndata:\r\n%s",
					clock(),
						ulLenRec,
						sendtime,
						recvtime,
						totaltime,
						szX
					);

				GetDlgItem(IDC_EDIT_C_REPLY)->SetWindowText((char*)buffer);
			}

	*/

			m_bMonitorSocket=FALSE;
			while(m_bSocketMonitorStarted) Sleep(10);
			m_bMonitorSocket=TRUE;
			_beginthread(SocketMonitorThread, 0, (void*)this);


		}
		else
		{
			AfxMessageBox("No connection!");
		}
		GetDlgItem(IDC_BUTTON_CONNECT)->EnableWindow(TRUE);
	}
	else
	{
		m_bMonitorSocket=FALSE;
		while(m_bSocketMonitorStarted) Sleep(10);
		m_net.CloseConnection(m_s);
		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
		GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_C_PORT)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUTTON_C_SEND)->EnableWindow(FALSE);
		m_s = NULL;
	}
}

void CCoreUtil_CortexUtilDlg::OnButtonAssemble() 
{
	UpdateData(TRUE);

	unsigned char buffer[8192];
	unsigned long ulLen=0;
	
	// assemble buffer

	UUID uuid;
	UuidCreate(&uuid); // could check response code to be RPC_S_OK ?

//			CoCreateGuid(&uuid);

	unsigned char* pucUUID = NULL;
	UuidToString(&uuid, &pucUUID);


	_snprintf((char*)buffer, 8191, "<cortex xmlns=\"Cortex\"><rx rxid=\"%s\"/><cmd type=\"%s\"><options>%s</options></cmd></cortex>", 
		pucUUID?(char*)pucUUID:"27B",
		m_szCmd,
		m_szOptions
		);

	if(pucUUID)
	{
		try { RpcStringFree(&pucUUID); } catch(...){}
	}
	pucUUID = NULL;

	m_szAssembled = (char*)buffer;
	
	GetDlgItem(IDC_EDIT_C_REPLY)->SetWindowText(FormatXML(m_szAssembled));

}

CString CCoreUtil_CortexUtilDlg::FormatXML(CString szXML)
{
	CString szFormatted="";
	CBufferUtil bu;
	char* p = bu.FormatXML(szXML.GetBuffer(0));
	if(p)
	{
		szFormatted = p;
		free(p);
	}
	return szFormatted;
}

/*
CString CCoreUtil_CortexUtilDlg::FormatXML(CString szXML)
{
	CString szFormatted;

	CBufferUtil bu;
	int nLen =szXML.GetLength();
	char* inbuf = szXML.GetBuffer(nLen);
	int count = bu.CountChar(inbuf, nLen, '>');

	nLen += count;
	nLen += count;
	
	nLen *= 3;
//	nLen *= 200000;

	char* outbuf = szFormatted.GetBuffer(nLen+1);
	char* outbuf0 = outbuf;
	char buffer[256]; strcpy(buffer,"");
	char buffer2[256];
	count=0;
	int indent = -1;
	int nCondition=0; //0 = init, 1 = in begin tag including <>, 2 in content (X in <tag>X</tag>), 
	// 3 in end tag including </>, 4 in between tags including any whitespace

	char** pptags=NULL;
	int nNumtags=0;

	BOOL content = FALSE;
//	BOOL contentreturn = FALSE;
//	BOOL tagonline = FALSE;
	BOOL indented = FALSE;
	BOOL suppressed = FALSE;
//	BOOL returned = FALSE;

	int nLastCondition=0;
	int nLastTag=-1;

//	FILE* fp;

	while(count<nLen)
	{
		if(*inbuf == 0)
		{
			break;
		}

		if(*inbuf =='<')
		{
			if(*(inbuf+1) =='/')
			{
				//end tag, can't be anything else
				nCondition = 3;
				content = FALSE;
				//if we are in an end tag and this is good XML, we have to remove the last tag from the list
				nNumtags--;
				if((nNumtags>=0)&&(pptags)&&(pptags[nNumtags])) free(pptags[nNumtags]);
			}
			else
			{
				// in a tag
				nCondition = 1;
				content = FALSE;

				// get the tag name.
				int q=0;
				while((*(inbuf+q+1) != ' ')&&(*(inbuf+q+1) != '\t')&&(*(inbuf+q+1) != '>')&&(*(inbuf+q+1) != '/')&&(*(inbuf+q+1) != 0))
				{
					buffer2[q] = *(inbuf+q+1);
					q++;
				}
				buffer2[q] = 0;

				char** ppnew = new char*[nNumtags+1];
				if(ppnew)
				{
					char* pch = (char*)malloc(strlen(buffer2)+1);
					if(pch)
					{
						strcpy(pch, buffer2);
					}
					if(pptags)
					{
						q=0;
						while(q<nNumtags)
						{
							ppnew[q]=pptags[q];
							q++;
						}
						delete [] pptags;
					}
					ppnew[nNumtags] = pch;
					nNumtags++;
					pptags = ppnew;

				}
				else AfxMessageBox("memory error");

			}

		}
		else if(*inbuf =='>')
		{
			if(nCondition == 1)
			{
				if(*(inbuf-1)=='/')
				{
					nNumtags--;
					if((nNumtags>=0)&&(pptags)&&(pptags[nNumtags])) free(pptags[nNumtags]);
					nCondition = 5;
					content = FALSE;
					if(*(inbuf+1)!='<') { content = TRUE;}
				}
				else
				{
					if(*(inbuf+1)!='<') { content = TRUE;}
					else{ content = FALSE; nCondition=0;}
				}
			}
			else
			if(nCondition == 3)
			{
				if(*(inbuf+1)!='<') { content = TRUE;}
				else{ content = FALSE; nCondition=4;}
			}
		}
		else
		{
			if(content)
			{
				if((nCondition==5)||(nCondition==3)) nCondition=6;
				else if(nCondition == 1)	nCondition=2; 
			}
		}

		if(nLastCondition!=nCondition)
		{
			// something has changed so do something with indents or newlines
			switch(nCondition)
			{
			case 0:  // init, or after a begin tag has just ended.
				{
					// put the > there
					*outbuf = *inbuf;
					outbuf++;

					// then decide...

					// only do this if the next thing is not an end tag for the same begin tag
					sprintf(buffer, "</>");
					if((pptags)&&(nNumtags>0)&&(pptags[nNumtags-1]))
					{
						sprintf(buffer, "</%s>", pptags[nNumtags-1]);
					}
/*
		fp=fopen("XML.txt", "ab");
		if(fp)
		{
			*outbuf = 0;
			fprintf(fp,"checking %s vs %s\r\n", buffer, inbuf+1);
			fclose(fp);
		}
* /
					if(strncmp(inbuf+1, buffer, strlen(buffer)))
					{
/*
		fp=fopen("XML.txt", "ab");
		if(fp)
		{
			*outbuf = 0;
			fprintf(fp,"tags diff return\r\n");
			fclose(fp);
		}
* /
						*outbuf = 13;
						outbuf++;
						*outbuf = 10;
						outbuf++;
//						tagonline = FALSE;
						indented = FALSE;
						suppressed = FALSE;
//						returned = TRUE;
					}
					else suppressed = TRUE;

				} break;
			case 1:  // in a tag
				{
					nLastTag = nNumtags-1;
					indent++;

/*


		fp=fopen("XML.txt", "ab");
		if(fp)
		{
			*outbuf = 0;
			fprintf(fp,"INDENT1+: %d\r\n", indent);
			fclose(fp);
		}
* /
					if((nLastCondition==2)||(nLastCondition==6)||(nLastCondition==4)||(nLastCondition==5))
					{

						*outbuf = 13;
						outbuf++;
						*outbuf = 10;
						outbuf++;
//						tagonline = FALSE;
						indented = FALSE;
						suppressed = FALSE;

					}

					if(!indented)
					{
						int q=0;
						while(q<indent)
						{
							*outbuf = ' ';
							outbuf++;
							*outbuf = ' ';
							outbuf++;
							q++;
						}
						indented = TRUE;
					}

//					tagonline = TRUE;

					// and the begin tag marker
					*outbuf = *inbuf;
					outbuf++;

				} break;
			case 2:  // in content
				{
					//if the content has a simple bracketed set of tags, leave it, otherwise return and indent 
					BOOL DoIt=TRUE;
					int q=1;
					while(*(inbuf+q)!=0)
					{
						if(*(inbuf+q)=='<')
						{
							sprintf(buffer, "</>");
							if((pptags)&&(nNumtags>0)&&(pptags[nNumtags-1]))
							{
								sprintf(buffer, "</%s>", pptags[nNumtags-1]);
							}
/*
				fp=fopen("XML.txt", "ab");
				if(fp)
				{
					*outbuf = 0;
					fprintf(fp,"checking %s vs %s\r\n", buffer, inbuf+1);
					fclose(fp);
				}

* /
							if(strncmp(inbuf+q, buffer, strlen(buffer))==0)
							{
								DoIt=FALSE;
							}


							break;
						}
						q++;
					}
					if(DoIt)
					{
						*outbuf = 13;
						outbuf++;
						*outbuf = 10;
						outbuf++;
//						tagonline = FALSE;
						indented = FALSE;

						if(!indented)
						{
							int q=0;
							while(q<indent+1)
							{
								*outbuf = ' ';
								outbuf++;
								*outbuf = ' ';
								outbuf++;
								q++;
							}
							indented = TRUE;
						}
					}

					*outbuf = *inbuf;
					outbuf++;
				} break;
			case 3:  // in an end tag
				{

					if((!suppressed)&&(nLastCondition!=2))
					{
						*outbuf = 13;
						outbuf++;
						*outbuf = 10;
						outbuf++;
//						tagonline = FALSE;
						indented = FALSE;

						if(!indented)
						{
							int q=0;
							while(q<indent)
							{
								*outbuf = ' ';
								outbuf++;
								*outbuf = ' ';
								outbuf++;
								q++;
							}
							indented = TRUE;
						}
					}
					suppressed = FALSE;

					*outbuf = *inbuf;
					outbuf++;
					indent--; //back off one
/*
		fp=fopen("XML.txt", "ab");
		if(fp)
		{
			*outbuf = 0;
			fprintf(fp,"INDENT2-: %d\r\n", indent);
			fclose(fp);
		}
* /

				} break;
			case 4:  // in between tags
				{
					// put the > there
					*outbuf = *inbuf;
					outbuf++;

				} break;
			case 5:  // in between tags but came out of a <single/> tag
				{
					// put the > there
					*outbuf = *inbuf;
					outbuf++;

					indent--; //back off one
/*
		fp=fopen("XML.txt", "ab");
		if(fp)
		{
			*outbuf = 0;
			fprintf(fp,"INDENT3-: %d\r\n", indent);
			fclose(fp);
		}
* /

				} break;
			case 6:  // in betwewen tags content
				{


					if((nLastCondition==3)||(nLastCondition==5))
					{

						*outbuf = 13;
						outbuf++;
						*outbuf = 10;
						outbuf++;
//						tagonline = FALSE;
						indented = FALSE;
						suppressed = FALSE;


						if(!indented)
						{
							int q=0;
							while(q<indent+1)
							{
								*outbuf = ' ';
								outbuf++;
								*outbuf = ' ';
								outbuf++;
								q++;
							}
							indented = TRUE;
						}
					}



					*outbuf = *inbuf;
					outbuf++;
				} break;
			}

		}
		else
		{
			*outbuf = *inbuf;
			outbuf++;
		}
		nLastCondition = nCondition;

///					*outbuf = 0;
//					AfxMessageBox(outbuf0);
/*
		fp=fopen("XML.txt", "ab");
		if(fp)
		{
			*outbuf = 0;
			if((pptags)&&(nNumtags>0)&&(pptags[nNumtags-1]))
				fprintf(fp,"now condition %d, current tag [%s]\r\n%s\r\n\r\n", nCondition, pptags[nNumtags-1], outbuf0);
			else
				fprintf(fp,"now condition %d, no current tag\r\n%s\r\n\r\n", nCondition, outbuf0);
			fclose(fp);
		}
* /
		inbuf++;
		count++;
	}
	*outbuf=0;
	szFormatted.ReleaseBuffer();

	if(pptags)
	{
		if(nNumtags>0)
		{
			int q=0; 
			while(q<nNumtags)
			{
				free (pptags[q]);
				q++;
			}
		}
		delete [] pptags;
	}
	return szFormatted;
}



*/

void CCoreUtil_CortexUtilDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	FILE* fp = fopen("netlast.cfg", "wb");

	if(fp)
	{
		UpdateData(TRUE);
		CString szText;
		int nCount=((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->GetCount();
		int nSel=((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->GetCurSel();  //CB_ERR
		if(nSel==CB_ERR)
		{
			if(m_szHost.GetLength()>0)
				fprintf(fp, "*%s\n", m_szHost);
		}
		if(nCount>0)
		{
			int i=0;
			while(i<nCount)
			{
				((CComboBox*)GetDlgItem(IDC_COMBO_HOST))->GetLBText( i, szText );
				if(nSel==i)
				{
					fprintf(fp, "*");
				}
				fprintf(fp, "%s\n", szText);
				i++;
			}
		}
		fclose(fp);
	}
	
}


void SocketMonitorThread(void* pvArgs)
{
	CCoreUtil_CortexUtilDlg* p = (CCoreUtil_CortexUtilDlg*)pvArgs;
	if(p)
	{
		p->m_bSocketMonitorStarted = TRUE;
		while(p->m_bMonitorSocket)
		{
			timeval tv;
			tv.tv_sec = 0; 
			tv.tv_usec = 500;  // timeout value
			int nNumSockets;
			fd_set fds;

			FD_ZERO(&fds);  // Zero this out each time
			FD_SET(p->m_s, &fds);

			nNumSockets = select(0, &fds, NULL, NULL, &tv);
			if ( nNumSockets == SOCKET_ERROR )  // had INVALID_SOCKET for some reason before
			{
				p->GetDlgItem(IDC_BUTTON_RECV)->EnableWindow(FALSE);
			}
			else
			if(
					(nNumSockets==0) // 0 = timed out, -1 = error
				||(!(FD_ISSET(p->m_s, &fds)))
				) 
			{ 
				p->GetDlgItem(IDC_BUTTON_RECV)->EnableWindow(FALSE);
			}
			else // there is recv data.
			{ // wait some delay.
				if((clock() > p->m_nClock+1000)&&(!p->m_bInCommand))
				{
					p->GetDlgItem(IDC_BUTTON_RECV)->EnableWindow(TRUE);
					if(clock() > p->m_nClock+1000) // five seconds is a huge timeout.
						p->OnMonitorButtonRecv();
				}
			}
			if(p->m_bMonitorSocket) Sleep(100);
		}
		p->m_bSocketMonitorStarted = FALSE;
	}
}
void CCoreUtil_CortexUtilDlg::OnMonitorButtonRecv()
{
	if(m_bInCommand) return;
	timeval tv;
	tv.tv_sec = 0; 
	tv.tv_usec = 500;  // timeout value
	int nNumSockets;
	fd_set fds;

	FD_ZERO(&fds);  // Zero this out each time
	FD_SET(m_s, &fds);

	nNumSockets = select(0, &fds, NULL, NULL, &tv);
	if ( nNumSockets == SOCKET_ERROR )  // had INVALID_SOCKET for some reason before
	{
		CString attaturk;  attaturk.Format("Socket error in select: %d", WSAGetLastError());
		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
		GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_C_PORT)->EnableWindow(TRUE);
		m_net.CloseConnection(m_s);
		m_s = NULL;
		AfxMessageBox(attaturk);
	}
	else
	if(
			(nNumSockets==0) // 0 = timed out, -1 = error
		||(!(FD_ISSET(m_s, &fds)))
		) 
	{ 
//		AfxMessageBox("No incoming data to receive");
	}
	else // there is recv data.
	{
		//recv the response.
//		unsigned char* puc2 = NULL;
//		unsigned long ulLen2=0
		unsigned char* puc = NULL;
		unsigned long ulLen=0;
		char szError[8096];
		int nReturn=m_net.GetLine(&puc, &ulLen, m_s, NET_RCV_ONCE, szError);
		if(nReturn<NET_SUCCESS)
		{
			CString attaturk;  attaturk.Format("Error %d sending command, return code %d\r\n%s: %d", WSAGetLastError(), nReturn,szError);
			GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
			GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
			GetDlgItem(IDC_EDIT_C_PORT)->EnableWindow(TRUE);
			m_net.CloseConnection(m_s);
			m_s = NULL;
			AfxMessageBox(attaturk);
		}
		else
		{

			if(ulLen==0) //disconnected
			{
				GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
				GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
				GetDlgItem(IDC_EDIT_C_PORT)->EnableWindow(TRUE);
				m_net.CloseConnection(m_s);
				m_s = NULL;
				AfxMessageBox("disconnected");
			}
			else
			{
				if(puc)
				{
/*
if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );
				fprintf(logfp, "%s%03d Recd: ", logtmbuf, timestamp.millitm);
				fwrite((char*)puc, sizeof(char), ulLen, logfp);
				fprintf(logfp, "%c%c%c%c", 13, 10, 13, 10);
				fclose(logfp);
			}
}
*/
	GetDlgItem(IDC_EDIT_C_REPLY)->SetWindowText((char*)puc);

					m_nClock = clock();

					free(puc);
				}
			}
		}

	}
}



void CCoreUtil_CortexUtilDlg::OnButtonRecv() 
{
	timeval tv;
	tv.tv_sec = 0; 
	tv.tv_usec = 500;  // timeout value
	int nNumSockets;
	fd_set fds;

	FD_ZERO(&fds);  // Zero this out each time
	FD_SET(m_s, &fds);

	nNumSockets = select(0, &fds, NULL, NULL, &tv);
	if ( nNumSockets == SOCKET_ERROR )  // had INVALID_SOCKET for some reason before
	{
		GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
		GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_C_PORT)->EnableWindow(TRUE);
		m_net.CloseConnection(m_s);
		m_s = NULL;
		CString attaturk;  attaturk.Format("Socket error in select: %d", WSAGetLastError());
		AfxMessageBox(attaturk);
	}
	else
	if(
			(nNumSockets==0) // 0 = timed out, -1 = error
		||(!(FD_ISSET(m_s, &fds)))
		) 
	{ 
		AfxMessageBox("No incoming data to receive");
	}
	else // there is recv data.
	{
		//recv the response.
//		unsigned char* puc2 = NULL;
//		unsigned long ulLen2=0
		unsigned char* puc = NULL;
		unsigned long ulLen=0;
		char szError[8096];
		int nReturn=m_net.GetLine(&puc, &ulLen, m_s, NET_RCV_ONCE, szError);
		if(nReturn<NET_SUCCESS)
		{
			CString attaturk;  attaturk.Format("Error %d sending command, return code %d\r\n%s: %d", WSAGetLastError(), nReturn,szError);
			GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
			GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
			GetDlgItem(IDC_EDIT_C_PORT)->EnableWindow(TRUE);
			m_net.CloseConnection(m_s);
			m_s = NULL;
			AfxMessageBox(attaturk);
		}
		else
		{

			if(ulLen==0) //disconnected
			{
				GetDlgItem(IDC_BUTTON_CONNECT)->SetWindowText("Connect");
				GetDlgItem(IDC_COMBO_HOST)->EnableWindow(TRUE);
				GetDlgItem(IDC_EDIT_C_PORT)->EnableWindow(TRUE);
				m_net.CloseConnection(m_s);
				m_s = NULL;
				AfxMessageBox("disconnected");
			}
			else
			{
				if(puc)
				{
/*
if(m_bLogTransactions)
{
			FILE* logfp = fopen(LOGFILENAME, "ab");
			if(logfp)
			{
				_timeb timestamp;
				_ftime(&timestamp);
				char logtmbuf[48]; // need 33;
				tm* theTime = localtime( &timestamp.time	);
				strftime( logtmbuf, 40, "%Y-%b-%d %H:%M:%S.", theTime );
				fprintf(logfp, "%s%03d Recd: ", logtmbuf, timestamp.millitm);
				fwrite((char*)puc, sizeof(char), ulLen, logfp);
				fprintf(logfp, "%c%c%c%c", 13, 10, 13, 10);
				fclose(logfp);
			}
}
*/

	GetDlgItem(IDC_EDIT_C_REPLY)->SetWindowText((char*)puc);
					m_nClock = clock();
					free(puc);
				}
			}
		}

	}	
}
