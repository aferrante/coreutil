#if !defined(AFX_COREUTIL_FILEUTILDLG_H__2B9F32CE_C0D6_4466_9B84_11854EB3C8FE__INCLUDED_)
#define AFX_COREUTIL_FILEUTILDLG_H__2B9F32CE_C0D6_4466_9B84_11854EB3C8FE__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// CoreUtil_FileUtilDlg.h : header file
//
#include "..\Common\TXT\FileUtil.h"

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_FileUtilDlg dialog

class CCoreUtil_FileUtilDlg : public CDialog
{
// Construction
public:
	CCoreUtil_FileUtilDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCoreUtil_FileUtilDlg)
	enum { IDD = IDD_COREUTIL_FILEUTIL_DIALOG };
	CString	m_szBuffer;
	CString	m_szComment;
	CString	m_szEntry;
	CString	m_szFilename;
	CString	m_szSection;
	CString	m_szValue;
	BOOL	m_bEncrypt;
	//}}AFX_DATA

	CFileUtil m_fu;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCoreUtil_FileUtilDlg)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCoreUtil_FileUtilDlg)
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnButtonGetsettings();
	afx_msg void OnButtonGpi();
	afx_msg void OnButtonGps();
	afx_msg void OnButtonSetsettings();
	afx_msg void OnButtonWpi();
	afx_msg void OnButtonWps();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COREUTIL_FILEUTILDLG_H__2B9F32CE_C0D6_4466_9B84_11854EB3C8FE__INCLUDED_)
