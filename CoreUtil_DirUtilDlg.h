#if !defined(AFX_COREUTIL_DIRUTILDLG_H__89EA69F6_A94E_4A9B_861A_B02D2EEAD192__INCLUDED_)
#define AFX_COREUTIL_DIRUTILDLG_H__89EA69F6_A94E_4A9B_861A_B02D2EEAD192__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// CoreUtil_DirUtilDlg.h : header file
//

#include "..\Common\FILE\DirUtil.h"

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_DirUtilDlg dialog

class CCoreUtil_DirUtilDlg : public CDialog
{
// Construction
public:
	CCoreUtil_DirUtilDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCoreUtil_DirUtilDlg)
	enum { IDD = IDD_COREUTIL_DIRUTIL_DIALOG };
	CListCtrl	m_list;
	//}}AFX_DATA


	char* m_pszFile; // in case log messages to file
	CDirUtil m_du;
	void Message(char* pszFormat, ...);


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCoreUtil_DirUtilDlg)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCoreUtil_DirUtilDlg)
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnButtonSetdir();
	afx_msg void OnButtonWatch();
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COREUTIL_DIRUTILDLG_H__89EA69F6_A94E_4A9B_861A_B02D2EEAD192__INCLUDED_)
