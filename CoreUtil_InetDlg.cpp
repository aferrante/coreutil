// CoreUtil_InetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CoreUtil.h"
#include "CoreUtil_InetDlg.h"
#include <sys\timeb.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_InetDlg dialog


CCoreUtil_InetDlg::CCoreUtil_InetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCoreUtil_InetDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCoreUtil_InetDlg)
	m_bFile = FALSE;
	m_szURL = _T("");
	m_nTimerMS = 100;
	m_bTimes = FALSE;
	//}}AFX_DATA_INIT
	m_bTimerSet = false;
	m_bDownloading = false;
}


void CCoreUtil_InetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCoreUtil_InetDlg)
	DDX_Check(pDX, IDC_CHECK_FILE, m_bFile);
	DDX_Text(pDX, IDC_EDIT1, m_szURL);
	DDX_Text(pDX, IDC_EDIT_TIMERMS, m_nTimerMS);
	DDV_MinMaxInt(pDX, m_nTimerMS, 0, 86400000);
	DDX_Check(pDX, IDC_CHECK_TIME, m_bTimes);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCoreUtil_InetDlg, CDialog)
	//{{AFX_MSG_MAP(CCoreUtil_InetDlg)
	ON_BN_CLICKED(IDC_BUTTON_START, OnButtonStart)
	ON_BN_CLICKED(IDC_BUTTON_TIMER, OnButtonTimer)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_InetDlg message handlers

BOOL CCoreUtil_InetDlg::Create(CWnd* pParentWnd) 
{
	return CDialog::Create(CCoreUtil_InetDlg::IDD, pParentWnd);
}

void CCoreUtil_InetDlg::OnButtonStart() 
{
	if (	m_bDownloading ) return;
	UpdateData(TRUE);	
	if(m_szURL.GetLength()<=0) return;
	m_bDownloading = true;
	UpdateData(TRUE);	
	CString szTextData;
	CString szInfo;
	CString szTime;
	GetDlgItem(IDC_BUTTON_START)->EnableWindow(FALSE);

	_timeb now;

	unsigned long ulTotalTime;
	unsigned long ulTime = clock();
	_ftime(&now);
	if(m_bFile)
	{
		if(m_inet.RetrieveURLToFile(m_szURL, "inetfile.txt", FALSE, FALSE, "", &szInfo)>=INET_SUCCESS)
		{
			ulTotalTime = clock() - ulTime;
			szTime.Format("time to download: %d ms", ulTotalTime);
			GetDlgItem(IDC_STATIC_TIME)->SetWindowText(szTime);

			FILE* fp; 
			if(m_bTimes)
			{
				fp = fopen("inettimes.txt", "ab");
				if(fp)
				{
					fprintf(fp, "%d,\"%s\",1,%d\r\n", now.time, m_szURL, ulTotalTime);
					fclose(fp);
				}
			}

			fp = fopen("inetfile.txt", "rt");
			if(fp)
			{

				fseek(fp, 0, SEEK_END);
				unsigned long ulFileLen = ftell(fp);

				char* pch = (char*) malloc(ulFileLen+1); // term zero
				if(pch)
				{
					fseek(fp, 0, SEEK_SET);
					fread(pch, sizeof(char), ulFileLen, fp);
					*(pch+ulFileLen) = 0; // term zero


					GetDlgItem(IDC_EDIT_STATUS)->SetWindowText(pch);

					free(pch);
				}
				fclose(fp);
			}
			else
			{
				GetDlgItem(IDC_EDIT_STATUS)->SetWindowText("could not open file");
			}

		}
		else
		{
			ulTotalTime = clock() - ulTime;
			szTime.Format("failed to download after: %d ms", ulTotalTime);
			GetDlgItem(IDC_STATIC_TIME)->SetWindowText(szTime);
			if(m_bTimes)
			{
				FILE* fp; 
				fp = fopen("inettimes.txt", "ab");
				if(fp)
				{
					fprintf(fp, "%d,\"%s\",0,%d\r\n", now.time, m_szURL, ulTotalTime);
					fclose(fp);
				}
			}

			GetDlgItem(IDC_EDIT_STATUS)->SetWindowText(szInfo);
		}
	}
	else
	{
		if(m_inet.RetrieveURLToText(m_szURL, &szTextData, FALSE, FALSE, "", &szInfo)>=INET_SUCCESS)
		{
			ulTotalTime = clock() - ulTime;
			szTime.Format("time to download: %d ms", ulTotalTime);
			if(m_bTimes)
			{
				FILE* fp; 
				fp = fopen("inettimes.txt", "ab");
				if(fp)
				{
					fprintf(fp, "%d,\"%s\",1,%d\r\n", now.time, m_szURL, ulTotalTime);
					fclose(fp);
				}
			}
			GetDlgItem(IDC_STATIC_TIME)->SetWindowText(szTime);
			GetDlgItem(IDC_EDIT_STATUS)->SetWindowText(szTextData);
		}
		else
		{
			ulTotalTime = clock() - ulTime;
			szTime.Format("failed to download after: %d ms", ulTotalTime);
			if(m_bTimes)
			{
				FILE* fp; 
				fp = fopen("inettimes.txt", "ab");
				if(fp)
				{
					fprintf(fp, "%d,\"%s\",0,%d\r\n", now.time, m_szURL, ulTotalTime);
					fclose(fp);
				}
			}
			GetDlgItem(IDC_STATIC_TIME)->SetWindowText(szTime);
			GetDlgItem(IDC_EDIT_STATUS)->SetWindowText(szInfo);
		}
	}
	GetDlgItem(IDC_BUTTON_START)->EnableWindow(TRUE);
	m_bDownloading = false;

}

void CCoreUtil_InetDlg::OnCancel() 
{
// CDialog::OnCancel();
}

void CCoreUtil_InetDlg::OnOK() 
{
//	CDialog::OnOK();
}

void CCoreUtil_InetDlg::OnButtonTimer() 
{
	if(m_bTimerSet)
	{
		KillTimer(6);
		m_bTimerSet = false;

		GetDlgItem(IDC_BUTTON_TIMER)->SetWindowText("Set Timer");
	}
	else
	{
		UpdateData(TRUE);

		SetTimer(6, m_nTimerMS, NULL);
		GetDlgItem(IDC_BUTTON_TIMER)->SetWindowText("Kill Timer");
		m_bTimerSet = true;
	}

}

void CCoreUtil_InetDlg::OnTimer(UINT nIDEvent) 
{
	if(nIDEvent == 6)
	{ 
		OnButtonStart();
	}
	else
	
	CDialog::OnTimer(nIDEvent);
}
