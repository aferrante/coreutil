// CoreUtilDlg.h : header file
//

#if !defined(AFX_COREUTILDLG_H__0FA12D49_0551_4758_9E5E_5A85CB9F0915__INCLUDED_)
#define AFX_COREUTILDLG_H__0FA12D49_0551_4758_9E5E_5A85CB9F0915__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000



#define NUM_TEST_TABS 15
/////////////////////////////////////////////////////////////////////////////
// CCoreUtilDlg dialog

class CCoreUtilDlg : public CDialog
{
// Construction
public:
	CCoreUtilDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CCoreUtilDlg)
	enum { IDD = IDD_COREUTIL_DIALOG };
	CTabCtrl	m_Tab1;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCoreUtilDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

	TC_ITEM	m_tabItem;
	CDialog* m_pDlg[NUM_TEST_TABS];

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CCoreUtilDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnSelchangeTab1(NMHDR* pNMHDR, LRESULT* pResult);
	virtual void OnOK();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COREUTILDLG_H__0FA12D49_0551_4758_9E5E_5A85CB9F0915__INCLUDED_)
