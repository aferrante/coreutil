#if !defined(AFX_COREUTIL_HTTP10DLG_H__FB6994C0_F0E1_4843_B68F_AED48E1992E8__INCLUDED_)
#define AFX_COREUTIL_HTTP10DLG_H__FB6994C0_F0E1_4843_B68F_AED48E1992E8__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// CoreUtil_Http10Dlg.h : header file
//

#include "..\Common\HTTP\HTTP10.h"

/////////////////////////////////////////////////////////////////////////////
// CCoreUtil_Http10Dlg dialog

class CCoreUtil_Http10Dlg : public CDialog
{
// Construction
public:
	CCoreUtil_Http10Dlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCoreUtil_Http10Dlg)
	enum { IDD = IDD_COREUTIL_HTTP10_DIALOG };
	CString	m_szRoot;
	int		m_nPort;
	//}}AFX_DATA

	CHTTP10 m_http;
	bool m_bServerStarted;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCoreUtil_Http10Dlg)
	public:
	virtual BOOL Create(CWnd* pParentWnd);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCoreUtil_Http10Dlg)
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnButtonStart();
	afx_msg void OnButtonStop();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COREUTIL_HTTP10DLG_H__FB6994C0_F0E1_4843_B68F_AED48E1992E8__INCLUDED_)
